package com.spheresuite.erp.util;
import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.DepartmentService;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


public class JwtTokenGenerator {
	
	public String createJWT(List<UserDO> userInfo) throws AppException {
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		 
		long nowMillis = System.currentTimeMillis();
		Date dateNow = new Date(nowMillis);
		 
		//We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("ths is secret");
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
		 //Let's set the JWT Claims
		List<Long> deptId = new ArrayList<Long>();
		if(userInfo.get(0).getEmpId() != null){
			deptId = new DepartmentService().retrieveisManager(userInfo.get(0).getEmpId());
		}
		JwtBuilder builder = Jwts.builder().setId(deptId.toString())
	                            .setIssuedAt(dateNow)
	                            .setSubject(userInfo.get(0).getEmpId().toString())
	                            .setIssuer(userInfo.get(0).getEmail().toString())
	                            .signWith(signatureAlgorithm, signingKey);
		int ttlMillis = 9000000;
		if (ttlMillis >= 0) {
		    long expMillis = nowMillis + ttlMillis;
		    Date exp = new Date(expMillis);
		    builder.setExpiration(exp);
		}
		String token = builder.compact();
		return token;
	}
}
