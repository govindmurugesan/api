package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ScheduleDO;
import com.spheresuite.erp.service.ScheduleService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ScheduleUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/schedule")
public class ScheduleRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ScheduleRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ScheduleDO scheduleDO = new ScheduleDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.LEAD_ID).toString().isEmpty()){
			 			scheduleDO.setLeadId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEAD_ID).toString() : ""));
			 		}
			 		if(!inputJSON.get(CommonConstants.CONTACTLIST).toString().isEmpty()){
			 			JSONArray resultJSONArray = new JSONArray();
				 		resultJSONArray.put(inputJSON.get(CommonConstants.CONTACTLIST));
				 		String contactList="";
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArrayContact = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArrayContact.size(); i++) {
				 			contactList = contactList+resultJSONArrayContact.get(i)+",";
				 		}
				 		
				 		if(contactList != null && contactList.length() > 0){
				 			scheduleDO.setContactList(contactList);
				 		}
			 		}
			 		
			 		scheduleDO.setDescription(!inputJSON.get(CommonConstants.BODY).toString().isEmpty() ? inputJSON.get(CommonConstants.BODY).toString() : "");
			 		scheduleDO.setSubject(!inputJSON.get(CommonConstants.SUBJECT).toString().isEmpty() ? inputJSON.get(CommonConstants.SUBJECT).toString() : "");
			 		scheduleDO.setLogTime(!inputJSON.get(CommonConstants.TIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TIME).toString() : "");
			 		scheduleDO.setLogDate(!inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON.get(CommonConstants.DATE).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			scheduleDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			scheduleDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		scheduleDO.setUpdatedon(new Date());
			 	}
				new ScheduleService().persist(scheduleDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Schedule Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadid(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<ScheduleDO> logList = new ScheduleService().retrieveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
						respJSON = ScheduleUtil.getScheduleList(logList).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<ScheduleDO> logList = new ScheduleService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
						respJSON = ScheduleUtil.getScheduleList(logList).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ScheduleDO scheduleDO = new ScheduleDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<ScheduleDO> logActivityList = new ScheduleService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(logActivityList != null && logActivityList.size() > 0){
			 				scheduleDO = logActivityList.get(0);
					 		JSONArray resultJSONArray = new JSONArray();
					 		resultJSONArray.put(inputJSON.get(CommonConstants.CONTACTLIST));
					 		String contactList="";
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArrayContact = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArrayContact.size(); i++) {
					 			if(resultJSONArrayContact.get(i).toString().length() > 3){
					 				contactList = contactList+resultJSONArrayContact.get(i)+",";
					 			}
					 		}
					 		
					 		if(contactList != null && contactList.length() > 0){
					 			scheduleDO.setContactList(contactList);
					 		}
					 		scheduleDO.setDescription(!inputJSON.get(CommonConstants.BODY).toString().isEmpty() ? inputJSON.get(CommonConstants.BODY).toString() : "");
					 		scheduleDO.setSubject(!inputJSON.get(CommonConstants.SUBJECT).toString().isEmpty() ? inputJSON.get(CommonConstants.SUBJECT).toString() : "");
					 		scheduleDO.setLogTime(!inputJSON.get(CommonConstants.TIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TIME).toString() : "");
					 		scheduleDO.setLogDate(!inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON.get(CommonConstants.DATE).toString() : "");
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			scheduleDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		scheduleDO.setUpdatedon(new Date());
					 		new ScheduleService().update(scheduleDO);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Schedule Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveUpcomingCalender/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveUpcomingCalender(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ScheduleDO> logList = new ScheduleService().retrieveUpcomingCalender(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ScheduleUtil.getScheduleList(logList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveUpcomingCalenderOfEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveUpcomingCalenderOfEmp(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ScheduleDO> logList = new ScheduleService().retrieveUpcomingCalenderOfEmp(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ScheduleUtil.getScheduleList(logList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
}
