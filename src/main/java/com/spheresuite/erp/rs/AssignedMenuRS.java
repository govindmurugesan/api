package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.service.AssignedMenuService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/assignedmenu")
public class AssignedMenuRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AssignedMenuRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				List<AssignedMenuDO> assignedMenuList = new ArrayList<AssignedMenuDO>();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.MENU) != null && !inputJSON.get(CommonConstants.MENU).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
			 		resultJSONArray.put(inputJSON.get(CommonConstants.MENU));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			AssignedMenuDO assignedMenuDO = new AssignedMenuDO(); 
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			if(!inputJSON1.get(CommonConstants.SUB_MENU).toString().isEmpty()){
			 				assignedMenuDO.setMenuId(Long.parseLong(inputJSON1.get(CommonConstants.SUB_MENU).toString()));
				 			assignedMenuDO.setRoleId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 			assignedMenuDO.setUpdatedon(new Date());
			 			}
			 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 				assignedMenuDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 				assignedMenuDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
				 		}
			 			assignedMenuList.add(assignedMenuDO);
			 		}
			 		
			 	}
				new AssignedMenuService().persist(assignedMenuList);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
