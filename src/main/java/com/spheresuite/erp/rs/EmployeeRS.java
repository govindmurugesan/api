package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadEmailService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		String empId = null;
			 		String companyEmail = null;
			 		if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			employeeDO.setEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			empId = inputJSON.get(CommonConstants.EMPID).toString();
			 		}
			 		if(!inputJSON.get(CommonConstants.COMPANYEMAIL).toString().isEmpty()){
			 			employeeDO.setCompanyemail(inputJSON.get(CommonConstants.COMPANYEMAIL).toString());
			 			companyEmail = inputJSON.get(CommonConstants.COMPANYEMAIL).toString();
			 		}
			 		String flag = "";
			 		if(companyEmail != null){
			 			flag = checkDuplicate(empId, companyEmail);
			 			if(!flag.isEmpty()){
			 				if(flag.equalsIgnoreCase(CommonConstants.EMPID)){
			 					return CommonWebUtil.buildErrorResponse(CommonConstants.EMPID_FOUND).toString();
			 				}else if(flag.equalsIgnoreCase(CommonConstants.COMPANYEMAIL)){
			 					return CommonWebUtil.buildErrorResponse(CommonConstants.COMPANYEMAIL_FOUND).toString();
			 				}else if(flag.equalsIgnoreCase(CommonConstants.NO_DUPLICATE)){
			 					employeeDO.setWorkLocationId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.WORKLOCATION).toString() : ""));
						 		employeeDO.setFirstname(!inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : null);
						 		employeeDO.setLastname(!inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : null);
						 		employeeDO.setMiddlename(!inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : null);
						 		employeeDO.setPersonalcontact(!inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()? inputJSON.get(CommonConstants.PERSONALCONTACT).toString() : null);
						 		employeeDO.setPrimarycontact(!inputJSON.get(CommonConstants.PRIMARYCONTACT).toString().isEmpty() ? inputJSON.get(CommonConstants.PRIMARYCONTACT).toString() : null);
						 		
						 		employeeDO.setPersonalemail(!inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONALEMAIL).toString() : null);
						 		employeeDO.setAadhar(!inputJSON.get(CommonConstants.AADHAR).toString().isEmpty() ? inputJSON.get(CommonConstants.AADHAR).toString() : null);
						 		employeeDO.setPanno(!inputJSON.get(CommonConstants.PANNO).toString().isEmpty() ? inputJSON.get(CommonConstants.PANNO).toString() : null);
						 		employeeDO.setDateofbirth(!inputJSON.get(CommonConstants.DATEOFBIRTH).toString().isEmpty() ? inputJSON.get(CommonConstants.DATEOFBIRTH).toString() : null);
						 		if(!inputJSON.get(CommonConstants.EMPTYPE).toString().isEmpty()){
						 			employeeDO.setEmptype(Long.parseLong(!inputJSON.get(CommonConstants.EMPTYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.EMPTYPE).toString() : null));
						 		}
						 		if(!inputJSON.get(CommonConstants.REPORTTO).toString().isEmpty()){
						 			employeeDO.setReportto(Long.parseLong(!inputJSON.get(CommonConstants.REPORTTO).toString().isEmpty() ? inputJSON.get(CommonConstants.REPORTTO).toString() : null));
						 		}
						 		
						 		if(!inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
						 			employeeDO.setEmpdept(Long.parseLong(!inputJSON.get(CommonConstants.DEPT).toString().isEmpty() ? inputJSON.get(CommonConstants.DEPT).toString() : null));
						 		}
						 		employeeDO.setTitle(!inputJSON.get(CommonConstants.TITLE).toString().isEmpty() ? inputJSON.get(CommonConstants.TITLE).toString() : null);
						 		employeeDO.setJobdesc(!inputJSON.get(CommonConstants.JOBDESC).toString().isEmpty() ? inputJSON.get(CommonConstants.JOBDESC).toString() : null);
						 		employeeDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
						 		employeeDO.setGender((char) (inputJSON != null ? inputJSON.get(CommonConstants.GENDER).toString().charAt(0) : ""));
						 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			employeeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			employeeDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
						 		}
						 		employeeDO.setUpdatedon(new Date());
						 		employeeDO.setStatus('a');
						 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Employee Created");
			 				}
			 			}else{
			 				return CommonWebUtil.buildErrorResponse("").toString();
			 			}
			 		}else{
			 			return CommonWebUtil.buildErrorResponse(CommonConstants.COMPANYEMAIL_FOUND).toString();
			 		}
			 	}
				new EmployeeService().persist(employeeDO);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeUtil.getEmployeeList(employeeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeDO> employeeList = new EmployeeService().retrieve();
				respJSON = EmployeeUtil.getEmployeeListWithOutPhoto(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeDO> employeeList = new EmployeeService().retrieve();
				respJSON = EmployeeUtil.getEmployeeListWithOutPhoto(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveId", method = RequestMethod.GET)
	public @ResponseBody String retrieveId(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		Long empid = 0L;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeDO> employeeList = new EmployeeService().retrieve();
				if(employeeList != null && employeeList.size() > 0){
					Collections.reverse(employeeList);
					if(employeeList.get(0).getEmpId() != null){
						empid = Long.parseLong(employeeList.get(0).getEmpId());
						empid = empid+1;
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch(NumberFormatException ex){  
			empid = 0L;
			return CommonWebUtil.buildSuccessResponseId(empid).toString();
		}catch(Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(empid).toString();
	}

@RequestMapping(value = "/retriveByEmail/{inputParams:.+}", method = RequestMethod.GET)
public @ResponseBody String retriveByEmail(@PathVariable String inputParams, Model model, HttpServletRequest request) {
	String respJSON = null;
	try {
		if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				List<EmployeeDO> employeeList = new EmployeeService().retrieveByEmail(inputJSON.get(CommonConstants.EMAIL).toString());
				respJSON = EmployeeUtil.getEmployeeListWithOutPhoto(employeeList).toString();
			}
		}else{
			return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		}
	}catch (Exception e) {
		return CommonWebUtil.buildErrorResponse("").toString();
	}
	return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		String empId = null;
			 		String companyEmail = null;
			 		if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			employeeDO.setEmpId(inputJSON.get(CommonConstants.EMPID).toString());
			 			empId = inputJSON.get(CommonConstants.EMPID).toString();
			 		}
			 		if(!inputJSON.get(CommonConstants.COMPANYEMAIL).toString().isEmpty()){
			 			employeeDO.setCompanyemail(inputJSON.get(CommonConstants.COMPANYEMAIL).toString());
			 			companyEmail = inputJSON.get(CommonConstants.COMPANYEMAIL).toString();
			 		}
			 		String flag = "";
			 		if(companyEmail != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			flag = checkDuplicateUpdate(empId, companyEmail, Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(flag.equalsIgnoreCase(CommonConstants.EMPID)){
		 					return CommonWebUtil.buildErrorResponse(CommonConstants.EMPID_FOUND).toString();
		 				}else if(flag.equalsIgnoreCase(CommonConstants.COMPANYEMAIL)){
		 					return CommonWebUtil.buildErrorResponse(CommonConstants.COMPANYEMAIL_FOUND).toString();
		 				}else if(flag.equalsIgnoreCase(CommonConstants.NO_DUPLICATE)){
			 				List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					 		if(employeeList != null && employeeList.size() > 0){
					 			employeeDO = employeeList.get(0);
						 		employeeDO.setWorkLocationId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.WORKLOCATION).toString() : ""));
						 		if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						 			employeeDO.setEmpId(inputJSON.get(CommonConstants.EMPID).toString());
						 		}
						 		employeeDO.setCompanyemail(inputJSON.get(CommonConstants.COMPANYEMAIL).toString());
						 		employeeDO.setFirstname(!inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : null);
						 		employeeDO.setLastname(!inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : null);
						 		employeeDO.setMiddlename(!inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : null);
						 		employeeDO.setPersonalcontact(!inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()? inputJSON.get(CommonConstants.PERSONALCONTACT).toString() : null);
						 		employeeDO.setPrimarycontact(!inputJSON.get(CommonConstants.PRIMARYCONTACT).toString().isEmpty() ? inputJSON.get(CommonConstants.PRIMARYCONTACT).toString() : null);
						 		employeeDO.setPersonalemail(!inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONALEMAIL).toString() : null);
						 		employeeDO.setAadhar(!inputJSON.get(CommonConstants.AADHAR).toString().isEmpty() ? inputJSON.get(CommonConstants.AADHAR).toString() : null);
						 		employeeDO.setPanno(!inputJSON.get(CommonConstants.PANNO).toString().isEmpty() ? inputJSON.get(CommonConstants.PANNO).toString() : null);
						 		employeeDO.setDateofbirth(!inputJSON.get(CommonConstants.DATEOFBIRTH).toString().isEmpty() ? inputJSON.get(CommonConstants.DATEOFBIRTH).toString() : null);
						 		if(!inputJSON.get(CommonConstants.EMPTYPE).toString().isEmpty()){
						 			employeeDO.setEmptype(Long.parseLong(!inputJSON.get(CommonConstants.EMPTYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.EMPTYPE).toString() : null));
						 		}
						 		if(!inputJSON.get(CommonConstants.REPORTTO).toString().isEmpty()){
						 			employeeDO.setReportto(Long.parseLong(!inputJSON.get(CommonConstants.REPORTTO).toString().isEmpty() ? inputJSON.get(CommonConstants.REPORTTO).toString() : null));
						 		}
						 		
						 		if(!inputJSON.get(CommonConstants.DEPT).toString().isEmpty()){
						 			employeeDO.setEmpdept(Long.parseLong(!inputJSON.get(CommonConstants.DEPT).toString().isEmpty() ? inputJSON.get(CommonConstants.DEPT).toString() : null));
						 		}
						 		employeeDO.setTitle(!inputJSON.get(CommonConstants.TITLE).toString().isEmpty() ? inputJSON.get(CommonConstants.TITLE).toString() : null);
						 		employeeDO.setJobdesc(!inputJSON.get(CommonConstants.JOBDESC).toString().isEmpty() ? inputJSON.get(CommonConstants.JOBDESC).toString() : null);
						 		employeeDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
						 		employeeDO.setGender((char) (inputJSON != null ? inputJSON.get(CommonConstants.GENDER).toString().charAt(0) : ""));
						 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			employeeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
						 		employeeDO.setUpdatedon(new Date());
						 		new EmployeeService().update(employeeDO);
						 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Updated");
					 		}
			 			}
			 		}else{
			 			return CommonWebUtil.buildErrorResponse(CommonConstants.COMPANYEMAIL_FOUND).toString();
			 		}
			 	}

			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("id") != null){
			 		List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(request.getParameter("id")));
			 		if(request.getParameter("file") != null && !request.getParameter("file").equals("")){
			 			if(employeeList != null && employeeList.size() > 0){
				 			employeeDO = employeeList.get(0);
					 		employeeDO.setPhoto(request.getParameter("file"));
					 		employeeDO.setUpdatedon(new Date());
					 		new EmployeeService().update(employeeDO);
				 		}
			 		}else{
			 			if(employeeList != null && employeeList.size() > 0){
				 			employeeDO = employeeList.get(0);
					 		employeeDO.setPhoto(null);
					 		employeeDO.setUpdatedon(new Date());
					 		new EmployeeService().update(employeeDO);
				 		}
			 		}
			 		
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateEmail", method = RequestMethod.POST)
	public @ResponseBody String updateEmail(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(employeeList != null && employeeList.size() > 0){
			 			employeeDO = employeeList.get(0);
			 			if(inputJSON.get(CommonConstants.EMAIL).toString() != null && inputJSON.get(CommonConstants.PASSWORD).toString() != null
			 					){//&& inputJSON.get(CommonConstants.TYPE).toString() != null
			 				employeeDO.setEmailId(inputJSON.get(CommonConstants.EMAIL).toString()); 
			 				String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
			 				employeeDO.setEmailPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
			 				employeeDO.setUpdatedon(new Date());
			 				employeeDO.setEmailType(inputJSON.get(CommonConstants.TYPE).toString());
					 		new EmployeeService().update(employeeDO);
					 		//MailBoxUtil.addInbox(employeeDO.getEmailId(), employeeDO.getEmailPassword(), employeeDO.getId());
			 			}else{
			 				return CommonWebUtil.buildErrorResponse("").toString();
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
	@RequestMapping(value = "/resetEmail", method = RequestMethod.POST)
	public @ResponseBody String resetEmail(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeDO employeeDO = new EmployeeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(employeeList != null && employeeList.size() > 0){
			 			employeeDO = employeeList.get(0);
		 				employeeDO.setEmailId(null); 
		 				employeeDO.setEmailPassword(null);
		 				employeeDO.setUpdatedon(new Date());
		 				employeeDO.setEmailType(null);
				 		new EmployeeService().update(employeeDO);
				 		new LeadEmailService().delete(employeeDO.getId());
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	public String checkDuplicate(String empId, String companyEmail) throws AppException{
		return new EmployeeService().checkDuplicate(empId, companyEmail);
	}
	
	public String checkDuplicateUpdate(String empId, String companyEmail, Long Id) throws AppException{
		return new EmployeeService().checkDuplicateUpdate(empId, companyEmail, Id);
	}
	
	@RequestMapping(value = "/importEmp/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
			
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<EmployeeDO> empDOlist = new ArrayList<EmployeeDO>();
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				for (int i=0; i < fileData.size(); i++){
					EmployeeDO employeeDO = new EmployeeDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(colName.get(CommonConstants.EMPID) != null){
						employeeDO.setEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
					}
					if(colName.get(CommonConstants.COMPANYEMAIL) != null){
						employeeDO.setCompanyemail(rowJSON.get(colName.get(CommonConstants.COMPANYEMAIL)).toString());
					}
					if(colName.get(CommonConstants.WORKLOCATION).toString() != null){
						if(!rowJSON.get(colName.get(CommonConstants.WORKLOCATION)).toString().isEmpty()){
							employeeDO.setWorkLocationId(Long.parseLong(rowJSON.get(colName.get(CommonConstants.WORKLOCATION)).toString()));
						}
					}
					if(colName.get(CommonConstants.FIRSTNAME) != null){
						employeeDO.setFirstname(!rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString() : null);
					}
					if(colName.get(CommonConstants.LASTNAME) != null){
						employeeDO.setLastname(!rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString() : null);
					}
					if(colName.get(CommonConstants.MIDDLENAME) != null){
						employeeDO.setMiddlename(!rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString() : null);
					}
					if(colName.get(CommonConstants.PERSONALCONTACT) != null){
						employeeDO.setPersonalcontact(!rowJSON.get(colName.get(CommonConstants.PERSONALCONTACT)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.PERSONALCONTACT)).toString() : null);
					}
					if(colName.get(CommonConstants.PRIMARYCONTACT) != null){
						employeeDO.setPrimarycontact(!rowJSON.get(colName.get(CommonConstants.PRIMARYCONTACT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PRIMARYCONTACT)).toString() : null);
					}
					if(colName.get(CommonConstants.PERSONALEMAIL) != null){
						employeeDO.setPersonalemail(!rowJSON.get(colName.get(CommonConstants.PERSONALEMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PERSONALEMAIL)).toString() : null);
					}
					if(colName.get(CommonConstants.AADHAR) != null){
						employeeDO.setAadhar(!rowJSON.get(colName.get(CommonConstants.AADHAR)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.AADHAR)).toString() : null);
					}
					if(colName.get(CommonConstants.PANNO) != null){
						employeeDO.setPanno(!rowJSON.get(colName.get(CommonConstants.PANNO)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PANNO)).toString() : null);
					}
					if(colName.get(CommonConstants.DATEOFBIRTH) != null){
						employeeDO.setDateofbirth(!rowJSON.get(colName.get(CommonConstants.DATEOFBIRTH)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.DATEOFBIRTH)).toString() : null);
					}
					if(colName.get(CommonConstants.EMPTYPE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.EMPTYPE)).toString().isEmpty()){
				 			employeeDO.setEmptype(Long.parseLong(!rowJSON.get(colName.get(CommonConstants.EMPTYPE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.EMPTYPE)).toString() : null));
				 		}
					}
					if(colName.get(CommonConstants.REPORTTO) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.REPORTTO)).toString().isEmpty()){
				 			employeeDO.setReportto(Long.parseLong(!rowJSON.get(colName.get(CommonConstants.REPORTTO)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.REPORTTO)).toString() : null));
				 		}
					}
					if(colName.get(CommonConstants.DEPT) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.DEPT)).toString().isEmpty()){
				 			employeeDO.setEmpdept(Long.parseLong(!rowJSON.get(colName.get(CommonConstants.DEPT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.DEPT)).toString() : null));
				 		}
					}
					if(colName.get(CommonConstants.TITLE) != null){
						employeeDO.setTitle(!rowJSON.get(colName.get(CommonConstants.TITLE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.TITLE)).toString() : null);
					}
					if(colName.get(CommonConstants.JOBDESC) != null){
						employeeDO.setJobdesc(!rowJSON.get(colName.get(CommonConstants.JOBDESC)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.JOBDESC)).toString() : null);
					}
					if(colName.get(CommonConstants.STARTDATE) != null){
						employeeDO.setStartdate(!rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString() : null);
					}
					if(colName.get(CommonConstants.GENDER) != null){
						employeeDO.setGender((char) (rowJSON != null ? rowJSON.get(colName.get(CommonConstants.GENDER)).toString().charAt(0) : ""));
					}
			 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
			 			employeeDO.setUpdatedby(updatedBy.toString());
			 			employeeDO.setCreatedBy(Long.parseLong(updatedBy.toString()));
			 		}
			 		employeeDO.setUpdatedon(new Date());
			 		employeeDO.setStatus('a');
					
					
					/*empDO.setEmpId(rowJSON.get(colName.get(CommonConstants.EMPID)).toString());
					empDO.setFirstname(rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString());
					//empDO.setUpdatedby(rowJSON.get(colName.get(CommonConstants.UPDATED_BY)).toString());
					empDO.setGender(rowJSON.get(colName.get(CommonConstants.GENDER)).toString().charAt(0));
					empDO.setPanno(rowJSON.get(colName.get(CommonConstants.PANNO)).toString());
				
				//	empDO.setUpdatedon((Date) rowJSON.get(colName.get(CommonConstants.UPDATED_ON)));
					empDO.setTitle(rowJSON.get(colName.get(CommonConstants.TITLE)).toString());
					empDO.setStartdate(rowJSON.get(colName.get(CommonConstants.STARTDATE)).toString());
					empDO.setPrimarycontact(rowJSON.get(colName.get(CommonConstants.PRIMARYCONTACT)).toString());
					empDO.setAadhar(rowJSON.get(colName.get(CommonConstants.AADHAR)).toString());
					empDO.setJobdesc(rowJSON.get(colName.get(CommonConstants.JOBDESC)).toString());
					empDO.setPersonalcontact(rowJSON.get(colName.get(CommonConstants.PERSONALCONTACT)).toString());
					empDO.setDateofbirth(rowJSON.get(colName.get(CommonConstants.DATEOFBIRTH)).toString());
					empDO.setCompanyemail(rowJSON.get(colName.get(CommonConstants.DEPT)).toString());
					empDO.setReportto(Long.parseLong(rowJSON.get(colName.get(CommonConstants.REPORTTO)).toString()));
					empDO.setMiddlename(rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString());
					empDO.setLastname(rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString());
					empDO.setCompanyemail(rowJSON.get(colName.get(CommonConstants.COMPANYEMAIL)).toString());
					empDO.setWorkLocationId(Long.parseLong(rowJSON.get(colName.get(CommonConstants.WORKLOCATION)).toString()));
					empDO.setEmptype(Long.parseLong(rowJSON.get(colName.get(CommonConstants.EMPTYPE)).toString()));
					System.out.println("i =="+i);*/
					
					empDOlist.add(employeeDO);
				}
				
				new EmployeeService().persistList(empDOlist);
				
			 	
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveEmpReporting/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveReportingDetails(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeUtil.getEmployeeReportingList(employeeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
}
