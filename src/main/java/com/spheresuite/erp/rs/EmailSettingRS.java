package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.service.EmailSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/emailSettings")
public class EmailSettingRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmailSettingRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmailSettingsDO emailSettingsDO = new EmailSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		emailSettingsDO.setMailtype(!inputJSON.get(CommonConstants.MAIL_TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.MAIL_TYPE).toString() : null);
			 		emailSettingsDO.setImaphost(!inputJSON.get(CommonConstants.IMAP_HOST).toString().isEmpty() ? inputJSON.get(CommonConstants.IMAP_HOST).toString() : null);
			 		emailSettingsDO.setImapport(!inputJSON.get(CommonConstants.PORT).toString().isEmpty() ? inputJSON.get(CommonConstants.PORT).toString() : null);
			 		emailSettingsDO.setSmtpport(!inputJSON.get(CommonConstants.SMTP_PORT).toString().isEmpty() ? inputJSON.get(CommonConstants.SMTP_PORT).toString() : null);
			 		emailSettingsDO.setSmtphost(!inputJSON.get(CommonConstants.SMTP_HOST).toString().isEmpty() ? inputJSON.get(CommonConstants.SMTP_HOST).toString() : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			emailSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			emailSettingsDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		emailSettingsDO.setUpdatedon(new Date());
			 		emailSettingsDO.setStatus('a');
			 	}
				new EmailSettingsService().persist(emailSettingsDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Email Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmailSettingsDO> emailSettingsList = new EmailSettingsService().retrieve();
				respJSON = EmailSettingsUtil.getEmailSettingsList(emailSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmailSettingsDO emailSettingsDO = new EmailSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmailSettingsDO> emailSettingsList = new EmailSettingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		emailSettingsDO = emailSettingsList.get(0);
			 		emailSettingsDO.setMailtype(!inputJSON.get(CommonConstants.MAIL_TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.MAIL_TYPE).toString() : null);
			 		emailSettingsDO.setImaphost(!inputJSON.get(CommonConstants.IMAP_HOST).toString().isEmpty() ? inputJSON.get(CommonConstants.IMAP_HOST).toString() : null);
			 		emailSettingsDO.setImapport(!inputJSON.get(CommonConstants.PORT).toString().isEmpty() ? inputJSON.get(CommonConstants.PORT).toString() : null);
			 		emailSettingsDO.setSmtpport(!inputJSON.get(CommonConstants.SMTP_PORT).toString().isEmpty() ? inputJSON.get(CommonConstants.SMTP_PORT).toString() : null);
			 		emailSettingsDO.setSmtphost(!inputJSON.get(CommonConstants.SMTP_HOST).toString().isEmpty() ? inputJSON.get(CommonConstants.SMTP_HOST).toString() : null);
			 		emailSettingsDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			emailSettingsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		emailSettingsDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new EmailSettingsService().update(emailSettingsDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Email Settings Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
