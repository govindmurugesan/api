package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.RolesUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/roles")
public class RolesRS {
	
	String validation = null;
	
	static Logger logger = Logger.getLogger(RolesRS.class.getName());

	

	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				RolesDO rolesDO = new RolesDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
					rolesDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					rolesDO.setDescription(!inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESCRIPTION).toString() : null);
					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			rolesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			rolesDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
					rolesDO.setUpdatedon(new Date());
					rolesDO.setStatus('a');
					new RolesService().persist(rolesDO);
					CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Role Created");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<RolesDO> rolesList = new RolesService().retrieve();
				respJSON = RolesUtil.getRolesList(rolesList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<RolesDO> rolesList = new RolesService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					if(rolesList != null){
						respJSON = RolesUtil.getRolesList(rolesList).toString();
					}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<RolesDO> rolesList = new RolesService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					if(rolesList != null){
						RolesDO rolesDO = rolesList.get(0);
						if(rolesDO != null){
							rolesDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
							rolesDO.setDescription(!inputJSON.get(CommonConstants.DESCRIPTION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESCRIPTION).toString() :  "");
							rolesDO.setUpdatedon(new Date());
							if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			rolesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
							new RolesService().update(rolesDO);
							CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Role Updated");
						}
					}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/delete/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String delete(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		boolean status = false;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					RolesDO rolesDO = new RolesDO();
					rolesDO.setId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					status = new RolesService().delete(rolesDO);
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return status == true ? CommonWebUtil.buildSuccessResponse().toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}