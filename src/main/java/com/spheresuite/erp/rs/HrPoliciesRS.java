package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.HrPoliciesDO;
import com.spheresuite.erp.domainobject.HrPoliciesDocDO;
import com.spheresuite.erp.service.HrPoliciesDocService;
import com.spheresuite.erp.service.HrPoliciesService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.HrPoliciesUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/hrpolicy")
public class HrPoliciesRS {

	String validation = null;
	static Logger logger = Logger.getLogger(HrPoliciesRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		HrPoliciesDO hrPoliciesDO = new HrPoliciesDO(); 
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.VALUE) != null && !inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
		 			
	 				hrPoliciesDO.setName(inputJSON.get(CommonConstants.VALUE).toString());
	 				if(!inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
	 					hrPoliciesDO.setRoleId(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
	 				}else{
	 					hrPoliciesDO.setRoleId(null);
	 				}
	 				
	 				hrPoliciesDO.setUpdatedon(new Date());
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				hrPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 				hrPoliciesDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
		 			hrPoliciesDO.setStatus('a');
		 			new HrPoliciesService().persist(hrPoliciesDO);
		 			CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Hr Policy Created");
		 			CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrpolicies","hrpolicies");
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(hrPoliciesDO.getId()).toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<HrPoliciesDO> employeeList = new HrPoliciesService().retrieve();
				respJSON = HrPoliciesUtil.getHrPoliciesList(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByRoleId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByRoleId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<HrPoliciesDO> employeeList = new HrPoliciesService().retrieveByRoleId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = HrPoliciesUtil.getHrPoliciesList(employeeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			if(!inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
			 				HrPoliciesDO hrPoliciesDO = new HrPoliciesDO(); 
			 				List<HrPoliciesDO> hrPolicyList = new HrPoliciesService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 				if(hrPolicyList != null && hrPolicyList.size() > 0){
			 					hrPoliciesDO = hrPolicyList.get(0);
			 					hrPoliciesDO.setName(inputJSON.get(CommonConstants.VALUE).toString());
				 				hrPoliciesDO.setUpdatedon(new Date());
				 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 				hrPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
				 				hrPoliciesDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
				 				if(!inputJSON.get(CommonConstants.ROLE_ID).toString().isEmpty()){
				 					hrPoliciesDO.setRoleId(Long.parseLong(inputJSON.get(CommonConstants.ROLE_ID).toString()));
				 				}else{
				 					hrPoliciesDO.setRoleId(null);
				 				}
				 				new HrPoliciesService().update(hrPoliciesDO);
				 				new HrPoliciesDocService().delete(hrPoliciesDO.getId());
				 				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Hr Policy Updated");
			 				}
			 			}
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				HrPoliciesDocDO hrpoliciesdoc = new HrPoliciesDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		hrpoliciesdoc.setHrpoliciesId(Long.parseLong(request.getParameter("id")));
			 		hrpoliciesdoc.setPhoto(request.getParameter("file"));
			 		hrpoliciesdoc.setFileName(request.getParameter("name"));
			 		new HrPoliciesDocService().persist(hrpoliciesdoc);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
