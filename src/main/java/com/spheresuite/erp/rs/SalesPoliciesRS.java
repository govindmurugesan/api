package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SalesPoliciesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.SalesPoliciesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.SalesPoliciesUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/salespolicy")
public class SalesPoliciesRS {

	String validation = null;
	static Logger logger = Logger.getLogger(SalesPoliciesRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.VALUE) != null && !inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
		 			SalesPoliciesDO salesPoliciesDO = new SalesPoliciesDO(); 
	 				salesPoliciesDO.setName(inputJSON.get(CommonConstants.VALUE).toString());
	 				salesPoliciesDO.setUpdatedon(new Date());
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				salesPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 				salesPoliciesDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
		 			salesPoliciesDO.setStatus('a');
		 			new SalesPoliciesService().persist(salesPoliciesDO);
		 			CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Sales Policy Updated");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<Long> empIds = new ArrayList<Long>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = new EmployeeService().retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<Long>();
					Long empid = Long.parseLong(request.getSession().getAttribute("empIds").toString());
					empIds.add(empid);
					List<UserDO> userList = new UserService().retriveByEmpId(empid);
					if(userList.get(0).getRoleId() != null){
						List<RolesDO> roleList = new RolesService().retriveById(userList.get(0).getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<Long>();
						}
					}else{
						empIds = new ArrayList<Long>();
					}
				}
				
				if(i==1){
					List<SalesPoliciesDO> salesPoliciesList = new SalesPoliciesService().retrieveAll();
					respJSON = SalesPoliciesUtil.getSalesPoliciesList(salesPoliciesList).toString();
				}else{
					List<SalesPoliciesDO> salesPoliciesList = new SalesPoliciesService().retrieve(empIds);
					respJSON = SalesPoliciesUtil.getSalesPoliciesList(salesPoliciesList).toString();
					//List<SalesPoliciesDO> salesPoliciesList = new SalesPoliciesService().retrieveAll();
					//respJSON = SalesPoliciesUtil.getSalesPoliciesList(salesPoliciesList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			if(!inputJSON.get(CommonConstants.VALUE).toString().isEmpty()){
			 				SalesPoliciesDO salesPoliciesDO = new SalesPoliciesDO(); 
			 				List<SalesPoliciesDO> salesPolicyList = new SalesPoliciesService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 				if(salesPolicyList != null && salesPolicyList.size() > 0){
			 					salesPoliciesDO = salesPolicyList.get(0);
			 				}
			 				salesPoliciesDO.setName(inputJSON.get(CommonConstants.VALUE).toString());
			 				salesPoliciesDO.setUpdatedon(new Date());
			 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 				salesPoliciesDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
			 				salesPoliciesDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 				new SalesPoliciesService().update(salesPoliciesDO);
			 				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sales Policy Updated");
			 			}
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
