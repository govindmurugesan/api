package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.TimesheetTypeDO;
import com.spheresuite.erp.service.TimesheetTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.TimesheetTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/timesheetType")
public class TimesheetTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TimesheetTypeRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				TimesheetTypeDO timesheetTypeDO = new TimesheetTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		timesheetTypeDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			timesheetTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			timesheetTypeDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		timesheetTypeDO.setUpdatedon(new Date());
			 		timesheetTypeDO.setStatus(CommonConstants.ACTIVE);
			 	}
				new TimesheetTypeService().persist(timesheetTypeDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New TimeSheet Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TimesheetTypeDO> timesheetTypeList = new TimesheetTypeService().retrieve();
				respJSON = TimesheetTypeUtil.getTimesheetTypeList(timesheetTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				TimesheetTypeDO timesheetTypeDO = new TimesheetTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<TimesheetTypeDO> timesheetTypeList = new TimesheetTypeService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		timesheetTypeDO = timesheetTypeList.get(0);
			 		timesheetTypeDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		timesheetTypeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			timesheetTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		timesheetTypeDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new TimesheetTypeService().update(timesheetTypeDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "TimeSheet Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
