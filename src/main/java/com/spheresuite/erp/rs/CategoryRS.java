package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CategoryDO;
import com.spheresuite.erp.service.CategoryService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CategoryUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/category")
public class CategoryRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CategoryRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				CategoryDO categoryDO = new CategoryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		categoryDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		categoryDO.setDescripetion(!inputJSON.get(CommonConstants.DESC).toString().isEmpty() ? inputJSON.get(CommonConstants.DESC).toString() : null);
			 		categoryDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			categoryDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			categoryDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		categoryDO.setStatus('a');
			 	}
				new CategoryService().persist(categoryDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Caregory Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CategoryDO> cagtegoryList = new CategoryService().retrieveActive();
				respJSON = CategoryUtil.getCategoryList(cagtegoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CategoryDO> categoryList = new CategoryService().retrieve();
				respJSON = CategoryUtil.getCategoryList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CategoryDO categoryDO = new CategoryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CategoryDO> categoryList = new CategoryService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		categoryDO = categoryList.get(0);
			 		categoryDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		categoryDO.setDescripetion(!inputJSON.get(CommonConstants.DESC).toString().isEmpty() ? inputJSON.get(CommonConstants.DESC).toString() : null);
			 		categoryDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			categoryDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		categoryDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new CategoryService().update(categoryDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Category Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
