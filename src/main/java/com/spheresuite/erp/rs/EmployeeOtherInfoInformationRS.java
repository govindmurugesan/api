package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeOtherInformationDO;
import com.spheresuite.erp.service.EmployeeOtherInformationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeOtherInformationUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeotherinformation")
public class EmployeeOtherInfoInformationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeOtherInfoInformationRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeOtherInformationDO employeeOtherInformationDO = new EmployeeOtherInformationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		employeeOtherInformationDO.setVoterId(!inputJSON.get(CommonConstants.VOTERID).toString().isEmpty() ? inputJSON.get(CommonConstants.VOTERID).toString() : null);
			 		employeeOtherInformationDO.setRationCard(!inputJSON.get(CommonConstants.RATIONCARD).toString().isEmpty() ? inputJSON.get(CommonConstants.RATIONCARD).toString() : null);
			 		employeeOtherInformationDO.setMaritalStatus(!inputJSON.get(CommonConstants.MARITALSTATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.MARITALSTATUS).toString() : null);
			 		employeeOtherInformationDO.setSpouseName(!inputJSON.get(CommonConstants.SPOUSENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.SPOUSENAME).toString() : null);
			 		employeeOtherInformationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeOtherInformationDO.setChildern(!inputJSON.get(CommonConstants.CHILDREN).toString().isEmpty() ?  Long.parseLong(inputJSON.get(CommonConstants.CHILDREN).toString()) : null);
			 		employeeOtherInformationDO.setBloodGroup(!inputJSON.get(CommonConstants.BLOODGROUP).toString().isEmpty() ? inputJSON.get(CommonConstants.BLOODGROUP).toString() : null);
			 		employeeOtherInformationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeOtherInformationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeOtherInformationDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new EmployeeOtherInformationService().persist(employeeOtherInformationDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Employee Other Information Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeOtherInformationDO> employeeOtherInformationList = new EmployeeOtherInformationService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeOtherInformationUtil.getEmployeeOtherInfoList(employeeOtherInformationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeOtherInformationDO employeeOtherInformationDO = new EmployeeOtherInformationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeOtherInformationDO> employeeOtherInformationList = new EmployeeOtherInformationService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeOtherInformationDO = employeeOtherInformationList.get(0);
			 		employeeOtherInformationDO.setVoterId(!inputJSON.get(CommonConstants.VOTERID).toString().isEmpty() ? inputJSON.get(CommonConstants.VOTERID).toString() : null);
			 		employeeOtherInformationDO.setRationCard(!inputJSON.get(CommonConstants.RATIONCARD).toString().isEmpty() ? inputJSON.get(CommonConstants.RATIONCARD).toString() : null);
			 		employeeOtherInformationDO.setMaritalStatus(!inputJSON.get(CommonConstants.MARITALSTATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.MARITALSTATUS).toString() : null);
			 		employeeOtherInformationDO.setSpouseName(!inputJSON.get(CommonConstants.SPOUSENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.SPOUSENAME).toString() : null);
			 		employeeOtherInformationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeOtherInformationDO.setChildern(!inputJSON.get(CommonConstants.CHILDREN).toString().isEmpty() ?  Long.parseLong(inputJSON.get(CommonConstants.CHILDREN).toString()) : null);
			 		employeeOtherInformationDO.setBloodGroup(!inputJSON.get(CommonConstants.BLOODGROUP).toString().isEmpty() ? inputJSON.get(CommonConstants.BLOODGROUP).toString() : null);
			 		employeeOtherInformationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeOtherInformationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new EmployeeOtherInformationService().update(employeeOtherInformationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Other Information Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
