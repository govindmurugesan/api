package com.spheresuite.erp.rs;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.service.TaxSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.TaxSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/taxsettings")
public class TaxSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TaxSettingsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				TaxSettingsDO taxSettingDO = new TaxSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		taxSettingDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		taxSettingDO.setSection(!inputJSON.get(CommonConstants.SECTION).toString().isEmpty() ? inputJSON.get(CommonConstants.SECTION).toString() : "");
			 		taxSettingDO.setPreposttax(!inputJSON.get(CommonConstants.PREPOST_TAX).toString().isEmpty() ? inputJSON.get(CommonConstants.PREPOST_TAX).toString() : "");
			 		taxSettingDO.setType(!inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : "");
			 		taxSettingDO.setFixedamount(!inputJSON.get(CommonConstants.FIXEDAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.FIXEDAMOUNT).toString() : "");
			 		taxSettingDO.setPercentageamount(!inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString() : "");
			 		taxSettingDO.setBasicgrosspay(!inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty() ? inputJSON.get(CommonConstants.BASICGROSS_PAY).toString() : "");
			 		taxSettingDO.setNotes(!inputJSON.get(CommonConstants.NOTES).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTES).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			taxSettingDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			taxSettingDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		taxSettingDO.setStatus('a');
			 		taxSettingDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 		taxSettingDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 		taxSettingDO.setDisplayname(!inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : "");
			 		taxSettingDO.setUpdatedon(new Date());
			 	}
				new TaxSettingsService().persist(taxSettingDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Tax Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TaxSettingsDO> adeductionSettingList = new TaxSettingsService().retrieve();
				respJSON = TaxSettingsUtil.getTaxSettingsList(adeductionSettingList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				int i =0;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			List<TaxSettingsDO> adeductionSettingList = new TaxSettingsService().retrieveActive(inputJSON.get(CommonConstants.NAME).toString());
			 			if(adeductionSettingList != null && adeductionSettingList.size() > 0){
			 				TaxSettingsDO taxSettingDO = new TaxSettingsDO();
			 				taxSettingDO = adeductionSettingList.get(0);
			 				taxSettingDO.setStatus('i');
			 				if(taxSettingDO.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						Calendar cal = Calendar.getInstance();
			 						cal.setTime(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 						cal.add(Calendar.DATE, -1);
			 						String enddate = CommonUtil.convertDateToYearWithOutTime(taxSettingDO.getStartdate());
			 						Calendar cal1 = Calendar.getInstance();
			 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
			 						Date dateBefore1Days = cal.getTime();
			 						if(cal1.before(cal)){
			 							taxSettingDO.setEnddate(dateBefore1Days);
			 						}else{
			 							taxSettingDO.setEnddate(taxSettingDO.getStartdate());
			 						}
			 					}else{
			 						i++;
			 					}
			 				}
			 				
			 				if(i == 0){
			 					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			taxSettingDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			taxSettingDO.setUpdatedon(new Date());
						 			new TaxSettingsService().update(taxSettingDO);
						 		}
			 				}
			 				TaxSettingsDO taxSetting = new TaxSettingsDO();
			 				taxSetting.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 				taxSetting.setSection(!inputJSON.get(CommonConstants.SECTION).toString().isEmpty() ? inputJSON.get(CommonConstants.SECTION).toString() : "");
			 				taxSetting.setPreposttax(!inputJSON.get(CommonConstants.PREPOST_TAX).toString().isEmpty() ? inputJSON.get(CommonConstants.PREPOST_TAX).toString() : "");
			 				taxSetting.setType(!inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : "");
			 				taxSetting.setFixedamount(!inputJSON.get(CommonConstants.FIXEDAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.FIXEDAMOUNT).toString() : "");
			 				taxSetting.setPercentageamount(!inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString() : "");
			 				taxSetting.setBasicgrosspay(!inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty() ? inputJSON.get(CommonConstants.BASICGROSS_PAY).toString() : "");
			 				taxSetting.setNotes(!inputJSON.get(CommonConstants.NOTES).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTES).toString() : "");
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			taxSetting.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		taxSetting .setUpdatedon(new Date());
					 		taxSetting.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
					 		taxSetting.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
					 		taxSetting.setDisplayname(!inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : "");
					 		taxSetting.setStatus('a');
					 		new TaxSettingsService().persist(taxSetting);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Tax Settings Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveAmount", method = RequestMethod.POST)
	public @ResponseBody String retriveById(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
					List<TaxSettingsDO> deductionSettingList = new TaxSettingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = TaxSettingsUtil.getTaxSettingsListForAmount(deductionSettingList, Double.parseDouble(inputJSON.get(CommonConstants.EMPCTC).toString()),Double.parseDouble(inputJSON.get(CommonConstants.BACSICPAYLABEL).toString())).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
