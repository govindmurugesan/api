package com.spheresuite.erp.rs;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.RecentActivityDO;
import com.spheresuite.erp.service.RecentActivityService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.RecentActivityUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/recentactivity")
public class RecentActivityRS {

	String validation = null;
	static Logger logger = Logger.getLogger(RecentActivityRS.class.getName());
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadid(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<RecentActivityDO> recentList = new RecentActivityService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
						respJSON = RecentActivityUtil.getRecentActivityList(recentList).toString();
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
