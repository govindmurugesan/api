package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AssignmentDO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.service.AssignmentService;
import com.spheresuite.erp.service.OpportunitiesDocService;
import com.spheresuite.erp.service.ProjectService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.AssignmentUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ProjectUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/assignment")
public class AssignmentRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AssignmentRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		AssignmentDO assignmentDO = new AssignmentDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		assignmentDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
			 		if(!inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString().isEmpty()){
			 			assignmentDO.setOpportunityId(Long.parseLong(inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			assignmentDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			assignmentDO.setProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 		}
			 		assignmentDO.setOrderNumber(!inputJSON.get(CommonConstants.ORDERNUMBER).toString().isEmpty() ? inputJSON.get(CommonConstants.ORDERNUMBER).toString() : null);
			 		assignmentDO.setProjectType(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTTYPE).toString() : null);
			 		assignmentDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
			 		assignmentDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : null);
			 		/*if(!inputJSON.get(CommonConstants.AMOUNTTYPE).toString().isEmpty()){
			 			assignmentDO.setAmountType(Long.parseLong(inputJSON.get(CommonConstants.AMOUNTTYPE).toString()));
			 		}*/
			 		if(!inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			assignmentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			assignmentDO.setPaymentTerms(Long.parseLong(inputJSON.get(CommonConstants.PAYMENTTERM).toString()));
			 		}
			 		
			 		assignmentDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		assignmentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			assignmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			assignmentDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
			 		}
			 	}
				new AssignmentService().persist(assignmentDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "New Assignment Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(assignmentDO.getId()).toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProjectDO> ProjectList = new ProjectService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	/*@RequestMapping(value = "/retrieveByOpportunityId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByOpportunityId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProjectDO> ProjectList = new ProjectService().retrieveByOpportunityId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	@RequestMapping(value = "/retrieveByEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AssignmentDO> assignmnetList = new AssignmentService().retrieveByEmp(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AssignmentUtil.getAllowanceList(assignmnetList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<AssignmentDO> assignmnetList = new AssignmentService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AssignmentUtil.getAllowanceList(assignmnetList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AssignmentDO> assignmnetList = new AssignmentService().retrieve();
				respJSON = AssignmentUtil.getAllowanceList(assignmnetList).toString();
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AssignmentDO assignmentDO = new AssignmentDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<AssignmentDO> assignmentList = new AssignmentService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		assignmentDO = assignmentList.get(0);
			 		assignmentDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
			 		if(!inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString().isEmpty()){
			 			assignmentDO.setOpportunityId(Long.parseLong(inputJSON.get(CommonConstants.OPPORTUNITY_ID).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			assignmentDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			assignmentDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString());
			 		}
			 		if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			assignmentDO.setProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 		}
			 		assignmentDO.setOrderNumber(!inputJSON.get(CommonConstants.ORDERNUMBER).toString().isEmpty() ? inputJSON.get(CommonConstants.ORDERNUMBER).toString() : null);
			 		assignmentDO.setProjectType(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTTYPE).toString() : null);
			 		assignmentDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
			 		assignmentDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : null);
			 		/*if(!inputJSON.get(CommonConstants.AMOUNTTYPE).toString().isEmpty()){
			 			assignmentDO.setAmountType(Long.parseLong(inputJSON.get(CommonConstants.AMOUNTTYPE).toString()));
			 		}*/
			 		if(!inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()){
			 			assignmentDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.AMOUNT).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			assignmentDO.setPaymentTerms(Long.parseLong(inputJSON.get(CommonConstants.PAYMENTTERM).toString()));
			 		}
			 		
			 		assignmentDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		assignmentDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			assignmentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			
			 		}
			 		new AssignmentService().update(assignmentDO);
			 		//new OpportunitiesDocService().delete(projectDO.getId());
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Assignment Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByProjectId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByProjectId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					//List<ProjectDO> ProjectList = new ProjectService().retrieveByProjectId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<ProjectDO> ProjectDetail = new ProjectService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProjectUtil.getProjectList(ProjectDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OpportunitiesDocDO opportunitiesDocDO = new OpportunitiesDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		opportunitiesDocDO.setOpportunityId(Long.parseLong(request.getParameter("id")));
			 		opportunitiesDocDO.setPhoto(request.getParameter("file"));
			 		opportunitiesDocDO.setFileName(request.getParameter("name"));
			 		new OpportunitiesDocService().update(opportunitiesDocDO);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
