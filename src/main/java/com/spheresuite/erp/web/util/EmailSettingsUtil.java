package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class EmailSettingsUtil {
	
	private EmailSettingsUtil() {}
	
	public static JSONObject getEmailSettingsList(List<EmailSettingsDO> emailSettingsDOList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmailSettingsDO emailSettingsDO : emailSettingsDOList) {
				resultJSONArray.put(getEmailSettingsObject(emailSettingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmailSettingsObject(EmailSettingsDO emailSettingsDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(emailSettingsDO.getId()));
		result.put(CommonConstants.MAIL_TYPE, String.valueOf(emailSettingsDO.getMailtype()));
		result.put(CommonConstants.IMAP_HOST, String.valueOf(emailSettingsDO.getImaphost()));
		result.put(CommonConstants.PORT, String.valueOf(emailSettingsDO.getImapport()));
		result.put(CommonConstants.SMTP_HOST, String.valueOf(emailSettingsDO.getSmtphost()));
		result.put(CommonConstants.SMTP_PORT, String.valueOf(emailSettingsDO.getSmtpport()));
		result.put(CommonConstants.STATUS, String.valueOf(emailSettingsDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(emailSettingsDO.getUpdatedon())));
		if(emailSettingsDO.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(emailSettingsDO.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
