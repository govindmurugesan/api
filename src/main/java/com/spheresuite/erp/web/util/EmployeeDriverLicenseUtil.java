package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeDriverLicenseDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeDriverLicenseUtil {
	
	private EmployeeDriverLicenseUtil() {}
	
	public static JSONObject getEmployeeLicenseList(List<EmployeeDriverLicenseDO> employeeDriverLicenseList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeDriverLicenseDO employeeDriverLicenseDO : employeeDriverLicenseList) {
				resultJSONArray.put(getEmployeeLicenseDetailObject(employeeDriverLicenseDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeLicenseDetailObject(EmployeeDriverLicenseDO employeeDriverLicenseDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeDriverLicenseDO.getId()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(employeeDriverLicenseDO.getCountryId()));
		result.put(CommonConstants.STATE_ID, String.valueOf(employeeDriverLicenseDO.getStateId()));
		CountryDAO obj = new CountryDAO();
		CountryDO workLocationdo = obj.retrieveByID(employeeDriverLicenseDO.getCountryId());
		if(workLocationdo != null){
			result.put(CommonConstants.COUNTRY_NAME, String.valueOf(workLocationdo.getName()));
		}
		
		StateService stateObj = new StateService();
		List<StateDO> stateDO = stateObj.retrieveById(employeeDriverLicenseDO.getStateId());
		if(stateDO != null && stateDO.size() > 0){
			result.put(CommonConstants.STATE_NAME, String.valueOf(stateDO.get(0).getName()));
		}
		result.put(CommonConstants.CITY, String.valueOf(employeeDriverLicenseDO.getCity()));
		
		result.put(CommonConstants.LICENSENUMBER, String.valueOf(employeeDriverLicenseDO.getLicenseNumber()));
		result.put(CommonConstants.ADDRESS1, String.valueOf(employeeDriverLicenseDO.getAddress1()));
		result.put(CommonConstants.ADDRESS2, String.valueOf(employeeDriverLicenseDO.getAddress2()!=null?employeeDriverLicenseDO.getAddress2():""));
		result.put(CommonConstants.NAME, String.valueOf(employeeDriverLicenseDO.getName()));
		result.put(CommonConstants.DATEISSUEDNAME, String.valueOf(CommonUtil.convertDateToStringWithdatetime(CommonUtil.convertStringToSqlDate(employeeDriverLicenseDO.getDateIssued().substring(0,10)))));
		result.put(CommonConstants.DATEISSUED, String.valueOf(employeeDriverLicenseDO.getDateIssued()));
		result.put(CommonConstants.EXPIRYDATE, String.valueOf(employeeDriverLicenseDO.getExpiryDate()));
		result.put(CommonConstants.EXPIRYDATENAME,  String.valueOf(CommonUtil.convertDateToStringWithdatetime(CommonUtil.convertStringToSqlDate(employeeDriverLicenseDO.getExpiryDate().substring(0,10)))));
		result.put(CommonConstants.EMPID, String.valueOf(employeeDriverLicenseDO.getEmpId()));
		if(employeeDriverLicenseDO.getZip() != null){
			result.put(CommonConstants.ZIP, String.valueOf(employeeDriverLicenseDO.getZip()));
		}else{
			result.put(CommonConstants.ZIP, "");
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeDriverLicenseDO.getUpdatedon())));
		if(employeeDriverLicenseDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(employeeDriverLicenseDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		
		return result;
	}
}
