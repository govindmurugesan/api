package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.HrPoliciesDO;
import com.spheresuite.erp.domainobject.HrPoliciesDocDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.HrPoliciesDocService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.util.CommonConstants;

public class HrPoliciesUtil {
	
	private HrPoliciesUtil() {}
	
	public static JSONObject getHrPoliciesList(List<HrPoliciesDO> hrPoliciesList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (HrPoliciesDO hrPolicies : hrPoliciesList) {
				resultJSONArray.put(getHrPoliciesDetailObject(hrPolicies));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHrPoliciesDetailObject(HrPoliciesDO hrPolicies)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(hrPolicies.getId()));
		result.put(CommonConstants.NAME, String.valueOf(hrPolicies.getName()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(hrPolicies.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(hrPolicies.getStatus()));
		if(hrPolicies.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(hrPolicies.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(hrPolicies.getRoleId() != null){
			List<RolesDO> roles = new RolesService().retriveById(hrPolicies.getRoleId());
			if(roles != null && roles.size() > 0){
				result.put(CommonConstants.ROLE_ID, String.valueOf(roles.get(0).getId()));
				result.put(CommonConstants.ROLE_NAME, String.valueOf(roles.get(0).getName()));
			}else{
				result.put(CommonConstants.ROLE_ID, "");
				result.put(CommonConstants.ROLE_NAME, "");
			}
		}else{
			
		}
		JSONArray resultJSONArray = new JSONArray();
		
		List<HrPoliciesDocDO> hrpoliciesdocList = new HrPoliciesDocService().retrieveByOppId(hrPolicies.getId());
		if(hrpoliciesdocList != null && hrpoliciesdocList.size()  > 0){
			for (HrPoliciesDocDO doc : hrpoliciesdocList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	

	public static JSONObject getDocDetailObject(HrPoliciesDocDO doc)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
}
