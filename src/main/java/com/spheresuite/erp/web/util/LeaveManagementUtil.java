package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.domainobject.LeaveTypeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveTypeService;
import com.spheresuite.erp.util.CommonConstants;

public class LeaveManagementUtil {
	
	private LeaveManagementUtil() {}
	
	public static JSONObject getLeaveList(List<LeaveManagementDO> leaveLists)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeaveManagementDO leaveDetail : leaveLists) {
				resultJSONArray.put(getLeaveRequestsDetailObject(leaveDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLeaveRequestsDetailObject(LeaveManagementDO leaveDetail)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(leaveDetail.getId()));
		result.put(CommonConstants.DAYS, String.valueOf(leaveDetail.getNumberOfDays()));
		
		if(leaveDetail.getLeaveType() != null){
			List<LeaveTypeDO> leaveType = new LeaveTypeService().retrieveById(leaveDetail.getLeaveType());
			if(leaveType != null && leaveType.size() > 0){
				if(leaveType.get(0).getType() != null){
					result.put(CommonConstants.TYPE, String.valueOf(leaveType.get(0).getId()));
					result.put(CommonConstants.TYPENAME, String.valueOf(leaveType.get(0).getType()));
				}
				else{
					result.put(CommonConstants.TYPE, "");
					result.put(CommonConstants.TYPENAME, "");
				}
			}else{
				result.put(CommonConstants.TYPE, "");
				result.put(CommonConstants.TYPENAME, "");
			}
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(leaveDetail.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(leaveDetail.getStatus()));
		
		
		if(leaveDetail.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(leaveDetail.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null){
					result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				}
				else{
					result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}
			}else{
				result.put(CommonConstants.UPDATED_BY, "");
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(leaveDetail.getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(leaveDetail.getEmpId());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null){
					result.put(CommonConstants.CREATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				}
				else{
					result.put(CommonConstants.CREATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}
			}else{
				result.put(CommonConstants.CREATED_BY, "");
			}
		}else{
			result.put(CommonConstants.CREATED_BY, "");
		}
		
		if(leaveDetail.getFromDate()!= null){
			result.put(CommonConstants.FROMDATE, String.valueOf(leaveDetail.getFromDate()));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		if(leaveDetail.getToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(leaveDetail.getToDate()));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		return result;
	}
}
