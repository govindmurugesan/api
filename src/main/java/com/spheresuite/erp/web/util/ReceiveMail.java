package com.spheresuite.erp.web.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.URLName;
import javax.mail.internet.MimeBodyPart;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;

import com.auth0.jwt.internal.org.apache.commons.lang3.ArrayUtils;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.util.CommonConstants;
import com.sun.mail.pop3.POP3Folder;
  
public class ReceiveMail{  
  
	public static List<LeadEmailDO> getAllInbox(final String userName, final String password) { 
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {  
		   Properties props = new Properties();  
		   props.put("mail.pop3.host", CommonConstants.POP3_HOST); 

		   Session emailSession = Session.getDefaultInstance(props, null);
		   
		   Store emailStore = emailSession.getStore(CommonConstants.POP3);  
		   emailStore.connect(CommonConstants.POP3_HOST , 110, userName, password); 
		   
		   Folder inbox = emailStore.getDefaultFolder();
		   inbox = inbox.getFolder("INBOX");
		   POP3Folder  uf = (POP3Folder)inbox; // cast folder to UIDFolder interface
		   inbox.open(Folder.READ_WRITE);
		   Message[] arrayMessages = inbox.getMessages();
		   ArrayUtils.reverse(arrayMessages);
		   
		  for (Message message : arrayMessages) {
		   	LeadEmailDO leadEmailDO = new LeadEmailDO();
				Date date = null;
		       date = CommonUtil.convertEmailDate(message.getSentDate().toString());
			    String formattedDate = "";
			    if( date != null ) {
			    	formattedDate = CommonUtil.convertEmailDateToDate( date );
			    }
			   Address[] fromAddress = message.getFrom();
				leadEmailDO.setContactId(null);
				leadEmailDO.setFromAddress(fromAddress[0].toString());
				leadEmailDO.setSubject(message.getSubject());
				leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
				leadEmailDO.setUpdatedon(new Date());
				leadEmailDO.setSeenMsg(message.isSet(Flags.Flag.SEEN));
				leadEmailDO.setPop3UId(uf.getUID(message));
		 		leadEmailDOlist.add(leadEmailDO);
			}
		  System.out.println("End Time ===== "+ new Date());
		   inbox.close(false);  
		   emailStore.close();  
		   return leadEmailDOlist;
  
  		} catch (Exception e) {System.out.println("Exception    "+e.getMessage());} 
  		return leadEmailDOlist;  
 	} 
 
	@SuppressWarnings("unused")
 	public static List<LeadEmailDO> getByMessageUid(final String userName, final String password, String uid, String label) throws Exception {
		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			if (session == null) {
				Properties props = null;
	            try {
	            	props = System.getProperties();
	                props.put("mail.pop3.host", CommonConstants.POP3_HOST);
	            } catch (SecurityException ex) {
	            }
	            session = Session.getDefaultInstance(props, null);
	        } 
			     
			   
			String type = null;
			String folderName = null;
			if(label.equalsIgnoreCase("Inbox")){
				type = CommonConstants.INBOX;
				folderName = CommonConstants.INBOX;;
			} else {
				type = "Sent Mail";
				folderName = "Sent Mail";
			}
			
			Store emailStore = session.getStore(CommonConstants.POP3);  
			emailStore.connect(CommonConstants.POP3_HOST , 110, userName, password); 
			 
			 Folder inbox = emailStore.getDefaultFolder();
			 inbox = inbox.getFolder("INBOX");
			 POP3Folder  uf = (POP3Folder)inbox; // cast folder to UIDFolder interface
			 
		   inbox.open(Folder.READ_WRITE);
		   Message[] arrayMessages = inbox.getMessages();
		   ArrayUtils.reverse(arrayMessages);
		   
		   System.out.println("start Time ===== "+ new Date());
		   for (Message message : arrayMessages) {
			   if(uid.equalsIgnoreCase(uf.getUID(message))){
				   message.setFlag(Flag.SEEN, true);
				   LeadEmailDO leadEmailDO = new LeadEmailDO();
				   Date date = null;
				   date = CommonUtil.convertEmailDate(message.getSentDate().toString());
				   String formattedDate = "";
				   if( date != null ) {
					  formattedDate = CommonUtil.convertEmailDateToDate( date );
				   }
				   Address[] fromAddress = message.getFrom();
				   leadEmailDO.setContactId(null);
				   leadEmailDO.setFromAddress(fromAddress[0].toString());
				   leadEmailDO.setSubject(message.getSubject());
				   leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
				   leadEmailDO.setUpdatedon(new Date());
				   leadEmailDO.setSeenMsg(true);
				   String messageContent = "";
				   leadEmailDO.setPop3UId(uid);
				   Object content = message.getContent();
				   if (content instanceof String) {
					  messageContent = content.toString();
				   }else if (content instanceof Multipart){
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                 	if(msgContent != null ){
	                 		messageContent = msgContent;
	                 	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                    	
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                        byte[] bytes = IOUtils.toByteArray(inStream);
		                        leadEmailDO.setContentType(bodyPart.getContentType());
		                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
	                         inStream.close();
		                    }
		                }
		           	}
					leadEmailDO.setMessageContent(messageContent);
			 		leadEmailDOlist.add(leadEmailDO);
			 		return leadEmailDOlist;
			   }			   
		   }
		   System.out.println("End Time ===== "+ new Date());
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	
	public static List<LeadEmailDO> getAllSentMail(String userName, String password) throws Exception {
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			//URLName url = new URLName(CommonConstants.IMAPS,CommonConstants.GMAIL_HOST_POP,Integer.parseInt(CommonConstants.GMAIL_PORT),CommonConstants.SENT_MAIL,userName, password); 
			if (session == null) {
	            Properties props = null;
	            try {
	                props = System.getProperties();
	            } catch (SecurityException sex) {
	                props = new Properties();
	            }
	            session = Session.getInstance(props, null);
	        }
			Store emailStore = session.getStore(CommonConstants.POP3);  
			emailStore.connect(CommonConstants.POP3_HOST , 110, userName, password);
	        Folder inbox = emailStore.getDefaultFolder();
		       
            inbox = inbox.getFolder("Sent Mail");
            UIDFolder uf = (UIDFolder)inbox; // cast folder to UIDFolder interface
            inbox.open(Folder.READ_WRITE);
            Message[] arrayMessages = inbox.getMessages();
            ArrayUtils.reverse(arrayMessages);
            System.out.println("Start Time :==" + arrayMessages.length);    
            Folder[] folderList = emailStore.getDefaultFolder().list();
            for (int i = 0; i < folderList.length; i++) {
                System.out.println(" ======   "+folderList[i].getFullName());
            }
	        /*
	        //int len = arrayMessages.length - 50;
			for (Message message: arrayMessages) {
				//Message message = arrayMessages[i];
				Address[] fromAddress = message.getFrom();
				Address[] toAddress = message.getAllRecipients();
				String from = fromAddress[0].toString();
				String to = toAddress[0].toString();
				String subject = message.getSubject();
				String sentDate = message.getSentDate().toString();
				String messageContent = "";
				String attachFiles = "";
				Object content = message.getContent();
				LeadEmailDO leadEmailDO = new LeadEmailDO();
				
				 if (content instanceof String) {
		                messageContent = content.toString();
		            } else if (content instanceof Multipart) {
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                    	
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                        byte[] bytes = IOUtils.toByteArray(inStream);
		                        leadEmailDO.setContentType(bodyPart.getContentType());
		                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
                                inStream.close();
		                    }
		                }
		            }
				    
				    Date date = null;
			        date = CommonUtil.convertEmailDate(sentDate);
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
		 			leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(from);
		 			leadEmailDO.setToAddress(to);
			 		leadEmailDO.setSubject(subject);
			 		leadEmailDO.setMessageContent(messageContent);
			 		leadEmailDO.setuId(uf.getUID(message));
			 		if(!attachFiles.isEmpty() && attachFiles.length() > 0){
			 			leadEmailDO.setAttachFiles(attachFiles);
			 		}
			 		
			 		
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 					 		
			 		leadEmailDO.setUpdatedon(new Date());
			 		
			 		leadEmailDOlist.add(leadEmailDO);
			}*/
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
 
 	private static String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String)p.getContent();
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			Multipart mp = (Multipart)p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
			    Part bp = mp.getBodyPart(i);
			    if (bp.isMimeType("text/plain")) {
			        if (text == null)
			            text = getText(bp);
			        continue;
			    } else if (bp.isMimeType("text/html")) {
			        String s = getText(bp);
			        if (s != null)
			            return s;
			    } else {
			        return getText(bp);
			    }
			}
		return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart)p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
				}
		}

		return null;
	}
	
	public static List<LeadEmailDO> getSingleMessage(List<LeadEmailDO> emailList) throws Exception {
		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			LeadEmailDO leadEmailDO = new LeadEmailDO();
			leadEmailDO.setContactId(null);
			leadEmailDO.setFromAddress(emailList.get(0).getFromAddress());
	 		leadEmailDO.setSubject(emailList.get(0).getSubject());
	 		leadEmailDO.setSentDate(emailList.get(0).getSentDate());
	 		leadEmailDO.setUpdatedon(new Date());
			String messageContent = "";
			leadEmailDO.setuId(emailList.get(0).getuId());
			Object content = emailList.get(0).getMsgObject();
			//byte[] decodedBytes = Base64.getDecoder().decode(emailList.get(0).getMessageContent());
			//Object content = SerializationUtils.serialize(leadEmailDO.getMsgObject());
			
		//	Object content = message.getContent();
			if (content instanceof String) {
             messageContent = content.toString();
			}else if (content instanceof Multipart){
	                Multipart multiPart = (Multipart) content;
	                for (int j = 0; j < multiPart.getCount(); j++) {
	                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
	                    Object o = bodyPart.getContent();
	                    String msgContent = getText(multiPart.getBodyPart(j));
                 	if(msgContent != null ){
                 		messageContent = msgContent;
                 	}
	                    
	                    if (o instanceof String) {
	                        messageContent = o.toString();
	                    } else if (null != bodyPart.getDisposition()
	                            && bodyPart.getDisposition().equalsIgnoreCase(
	                                    Part.ATTACHMENT)) {
	                    	
	                        String fileName = bodyPart.getFileName();
	                        leadEmailDO.setAttachFiles(fileName);
	                        InputStream inStream = bodyPart.getInputStream();
	                        byte[] bytes = IOUtils.toByteArray(inStream);
	                        leadEmailDO.setContentType(bodyPart.getContentType());
	                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
                         inStream.close();
	                    }
	                }
	           }
				leadEmailDO.setMessageContent(messageContent);
		 		leadEmailDOlist.add(leadEmailDO);
			//}
        // }
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	
	public static byte[] toByteArray(Object obj) throws IOException {
     byte[] bytes = null;
     ByteArrayOutputStream bos = null;
     ObjectOutputStream oos = null;
     try {
         bos = new ByteArrayOutputStream();
         oos = new ObjectOutputStream(bos);
         oos.writeObject(obj);
         oos.flush();
         bytes = bos.toByteArray();
     } finally {
         if (oos != null) {
             oos.close();
         }
         if (bos != null) {
             bos.close();
         }
     }
     return bytes;
 }

 public static Object toObject(byte[] bytes) throws IOException, ClassNotFoundException {
     Object obj = null;
     ByteArrayInputStream bis = null;
     ObjectInputStream ois = null;
     try {
         bis = new ByteArrayInputStream(bytes);
         ois = new ObjectInputStream(bis);
         obj = ois.readObject();
     } finally {
         if (bis != null) {
             bis.close();
         }
         if (ois != null) {
             ois.close();
         }
     }
     return obj;
 }
}  