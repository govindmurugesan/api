package com.spheresuite.erp.web.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;

import com.auth0.jwt.internal.org.apache.commons.lang3.ArrayUtils;
import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.service.EmailSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.sun.mail.imap.IMAPFolder.FetchProfileItem;

public class GmailUtil {
	@SuppressWarnings("unused")
	public static List<LeadEmailDO> getAllInbox(String userName, String password,String mailType) throws Exception {
		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			if (session == null) {
				Properties props = null;
	            try {
	            	props = System.getProperties();
	            	if(mailType.equalsIgnoreCase("Gmail")){
	            		props.put("mail.imap.partialfetch","false");
	 	                props.put("mail.imap.fetchsize", "1048576"); 
	 	                props.put("mail.imaps.partialfetch", "false"); 
	 	                props.put("mail.imaps.fetchsize", "1048576");
	            	}else if(mailType.equalsIgnoreCase("Exchange")){
	            		props.setProperty("mail.store.protocol", CommonConstants.EXCHANGE_IMAPS);
		            	props.put("mail.imap.auth", "true");
		            	props.put("mail.imap.socketFactory.fallback", "true");
		                props.put("mail.imap.partialfetch","false");
		                props.put("mail.imap.fetchsize", "1048576"); 
		                props.put("mail.imaps.partialfetch", "false"); 
		                props.put("mail.imaps.fetchsize", "1048576");
	            	}
	               
	            } catch (SecurityException sex) {
	            }
	            session = Session.getInstance(props, null);
	        }
			List<EmailSettingsDO> emailSettingList = new EmailSettingsService().retrieveByMailType(mailType);
			
			if(emailSettingList != null && emailSettingList.size() > 0){
				if(emailSettingList.get(0).getImaphost() != null &&
						emailSettingList.get(0).getImapport() != null){
					
					Store store = null;
					if(mailType.equalsIgnoreCase("Gmail")){
						store = session.getStore(new URLName(CommonConstants.IMAPS,emailSettingList.get(0).getImaphost(),Integer.parseInt(emailSettingList.get(0).getImapport()),CommonConstants.INBOX,userName, password));
						store.connect();
					}else if(mailType.equalsIgnoreCase("Exchange")){
						store = session.getStore(CommonConstants.EXCHANGE_IMAPS);
						userName = userName.split("@")[0];
				        store.connect(emailSettingList.get(0).getImaphost(),userName, password);
					}
			        
			        Folder inbox = store.getDefaultFolder();
		            inbox = inbox.getFolder("INBOX");
		            UIDFolder uf = (UIDFolder)inbox; // cast folder to UIDFolder interface
		            inbox.open(Folder.READ_WRITE);
		            Message[] arrayMessages = inbox.getMessages();
		            ArrayUtils.reverse(arrayMessages);
		            FetchProfile fp = new FetchProfile();
		            fp.add(FetchProfile.Item.ENVELOPE);
		            fp.add(FetchProfileItem.FLAGS);
		            fp.add(FetchProfileItem.CONTENT_INFO);
		            fp.add("X-mailer");
		            if (inbox instanceof UIDFolder) {
		            	fp.add(UIDFolder.FetchProfileItem.UID);
		            }
		            inbox.fetch(arrayMessages, fp);
		           
		           for (Message message : arrayMessages) {
		            	/*LeadEmailDO leadEmailDO = new LeadEmailDO();
						Date date = null;
				        date = CommonUtil.convertEmailDate(message.getSentDate().toString());
					    String formattedDate = "";
					    if( date != null ) {
					    	formattedDate = CommonUtil.convertEmailDateToDate( date );
					    }
					   Address[] fromAddress = message.getFrom();
			 			leadEmailDO.setContactId(null);
			 			leadEmailDO.setFromAddress(fromAddress[0].toString());
				 		leadEmailDO.setSubject(message.getSubject());
				 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
				 		leadEmailDO.setUpdatedon(new Date());
				 		leadEmailDO.setSeenMsg(message.isSet(Flags.Flag.SEEN));
						String messageContent = "";
						String attachFiles = "";
						leadEmailDO.setuId(uf.getUID(message));*/
		        	   
		        	   LeadEmailDO leadEmailDO = new LeadEmailDO();
						Date date = null;
				        date = CommonUtil.convertEmailDate(message.getReceivedDate().toString());
					    String formattedDate = "";
					    if( date != null ) {
					    	formattedDate = CommonUtil.convertEmailDateToDate( date );
					    }
					   Address[] fromAddress = message.getFrom();
			 			leadEmailDO.setContactId(null);
			 			leadEmailDO.setFromAddress(fromAddress[0].toString());
				 		leadEmailDO.setSubject(message.getSubject());
				 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
				 		leadEmailDO.setUpdatedon(new Date());
				 		leadEmailDO.setSeenMsg(message.isSet(Flags.Flag.SEEN));
						String messageContent = "";
						String attachFiles = "";
						leadEmailDO.setuId(uf.getUID(message));
					
						
						/*Object content = message.getContent();
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
					    ObjectOutputStream oos = new ObjectOutputStream(baos);
					    oos.writeObject(content);*/
						//byte[] data = SerializationUtils.serialize((Serializable) content);
						//leadEmailDO.setMsgObject(baos.toByteArray());
						/*
						if (content instanceof String) {
			                messageContent = content.toString();
						}else if (content instanceof Multipart){
				                Multipart multiPart = (Multipart) content;
				                for (int j = 0; j < multiPart.getCount(); j++) {
				                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
				                    Object o = bodyPart.getContent();
				                    String msgContent = getText(multiPart.getBodyPart(j));
			                    	if(msgContent != null ){
			                    		messageContent = msgContent;
			                    	}
				                    
				                    if (o instanceof String) {
				                        messageContent = o.toString();
				                    } else if (null != bodyPart.getDisposition()
				                            && bodyPart.getDisposition().equalsIgnoreCase(
				                                    Part.ATTACHMENT)) {
				                    	
				                        String fileName = bodyPart.getFileName();
				                        leadEmailDO.setAttachFiles(fileName);
				                        InputStream inStream = bodyPart.getInputStream();
				                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				                        
				                        byte[] tempBuffer = new byte[4096];// 4 KB
				                        int numRead;
				                        while ((numRead = inStream.read(tempBuffer)) != -1) {
				                            outStream.write(tempBuffer);
				                        }
				                        
				                        byte[] bytes = IOUtils.toByteArray(inStream);
				                        leadEmailDO.setContentType(bodyPart.getContentType());
				                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
		                                inStream.close();
				                    }
				                }
				           }
							leadEmailDO.setMessageContent(messageContent);*/
					 		leadEmailDOlist.add(leadEmailDO);
					}
				}
				
			}
	        
           //new LeadEmailService().persistList(leadEmailDOlist);
           // }
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	
	public static List<LeadEmailDO> getAllSentMail(String userName, String password) throws Exception {
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			URLName url = new URLName(CommonConstants.IMAPS,CommonConstants.GMAIL_HOST_POP,Integer.parseInt(CommonConstants.GMAIL_PORT),CommonConstants.SENT_MAIL,userName, password); 
			if (session == null) {
	            Properties props = null;
	            try {
	                props = System.getProperties();
	            } catch (SecurityException sex) {
	                props = new Properties();
	            }
	            session = Session.getInstance(props, null);
	        }
	        Store store = session.getStore(url);
	        store.connect();
	        Folder inbox = store.getDefaultFolder();
		       
            inbox = inbox.getFolder("[Gmail]/Sent Mail");
            UIDFolder uf = (UIDFolder)inbox; // cast folder to UIDFolder interface
            inbox.open(Folder.READ_WRITE);
            Message[] arrayMessages = inbox.getMessages();
            ArrayUtils.reverse(arrayMessages);
            System.out.println("Start Time :==" + new Date());    
            /*FetchProfile fp = new FetchProfile();
            fp.add(FetchProfile.Item.ENVELOPE);
            fp.add(FetchProfileItem.FLAGS);
            fp.add(FetchProfileItem.CONTENT_INFO);
            fp.add("X-mailer");*/
	        /*Folder folder = store.getFolder(url);
	        folder.open(Folder.READ_WRITE);
	        Message[] arrayMessages = folder.getMessages();
	        ArrayUtils.reverse(arrayMessages);*/
			/*SearchTerm sender = new FromTerm(new InternetAddress("lshruthi@saptalabs.com"));
			arrayMessages = folder.search(sender);*/
	        FetchProfile fetchProfile = new FetchProfile();
	        fetchProfile.add(FetchProfile.Item.ENVELOPE);
	        inbox.fetch(arrayMessages, fetchProfile);
	        //int len = arrayMessages.length - 50;
			for (Message message: arrayMessages) {
				//Message message = arrayMessages[i];
				Address[] fromAddress = message.getFrom();
				Address[] toAddress = message.getAllRecipients();
				String from = fromAddress[0].toString();
				String to = toAddress[0].toString();
				String subject = message.getSubject();
				String sentDate = message.getSentDate().toString();
				String messageContent = "";
				String attachFiles = "";
				Object content = message.getContent();
				LeadEmailDO leadEmailDO = new LeadEmailDO();
				
				 if (content instanceof String) {
		                messageContent = content.toString();
		            } else if (content instanceof Multipart) {
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                    	
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                       /* ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		                        
		                        byte[] tempBuffer = new byte[4096];// 4 KB
		                        int numRead;
		                        while ((numRead = inStream.read(tempBuffer)) != -1) {
		                            outStream.write(tempBuffer);
		                        }
		                        */
		                        byte[] bytes = IOUtils.toByteArray(inStream);
		                        leadEmailDO.setContentType(bodyPart.getContentType());
		                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
                                inStream.close();
		                    }
		                }
		            }
				    
				    Date date = null;
			        date = CommonUtil.convertEmailDate(sentDate);
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
		 			leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(from);
		 			leadEmailDO.setToAddress(to);
			 		leadEmailDO.setSubject(subject);
			 		leadEmailDO.setMessageContent(messageContent);
			 		leadEmailDO.setuId(uf.getUID(message));
			 		if(!attachFiles.isEmpty() && attachFiles.length() > 0){
			 			leadEmailDO.setAttachFiles(attachFiles);
			 		}
			 		
			 		
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 					 		
			 		leadEmailDO.setUpdatedon(new Date());
			 		
			 		leadEmailDOlist.add(leadEmailDO);
			}
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	private static String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String)p.getContent();
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			Multipart mp = (Multipart)p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
			    Part bp = mp.getBodyPart(i);
			    if (bp.isMimeType("text/plain")) {
			        if (text == null)
			            text = getText(bp);
			        continue;
			    } else if (bp.isMimeType("text/html")) {
			        String s = getText(bp);
			        if (s != null)
			            return s;
			    } else {
			        return getText(bp);
			    }
			}
		return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart)p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
				}
		}

		return null;
	}
	
	public static List<LeadEmailDO> getSingleMessage(List<LeadEmailDO> emailList) throws Exception {
		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			LeadEmailDO leadEmailDO = new LeadEmailDO();
 			leadEmailDO.setContactId(null);
 			leadEmailDO.setFromAddress(emailList.get(0).getFromAddress());
	 		leadEmailDO.setSubject(emailList.get(0).getSubject());
	 		leadEmailDO.setSentDate(emailList.get(0).getSentDate());
	 		leadEmailDO.setUpdatedon(new Date());
			String messageContent = "";
			leadEmailDO.setuId(emailList.get(0).getuId());
			Object content = emailList.get(0).getMsgObject();
			//byte[] decodedBytes = Base64.getDecoder().decode(emailList.get(0).getMessageContent());
			//Object content = SerializationUtils.serialize(leadEmailDO.getMsgObject());
			
		//	Object content = message.getContent();
			if (content instanceof String) {
                messageContent = content.toString();
			}else if (content instanceof Multipart){
	                Multipart multiPart = (Multipart) content;
	                for (int j = 0; j < multiPart.getCount(); j++) {
	                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
	                    Object o = bodyPart.getContent();
	                    String msgContent = getText(multiPart.getBodyPart(j));
                    	if(msgContent != null ){
                    		messageContent = msgContent;
                    	}
	                    
	                    if (o instanceof String) {
	                        messageContent = o.toString();
	                    } else if (null != bodyPart.getDisposition()
	                            && bodyPart.getDisposition().equalsIgnoreCase(
	                                    Part.ATTACHMENT)) {
	                    	
	                        String fileName = bodyPart.getFileName();
	                        leadEmailDO.setAttachFiles(fileName);
	                        InputStream inStream = bodyPart.getInputStream();
	                        byte[] bytes = IOUtils.toByteArray(inStream);
	                        leadEmailDO.setContentType(bodyPart.getContentType());
	                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
                            inStream.close();
	                    }
	                }
	           }
				leadEmailDO.setMessageContent(messageContent);
		 		leadEmailDOlist.add(leadEmailDO);
			//}
           // }
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	
	public static byte[] toByteArray(Object obj) throws IOException {
        byte[] bytes = null;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray();
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (bos != null) {
                bos.close();
            }
        }
        return bytes;
    }

    public static Object toObject(byte[] bytes) throws IOException, ClassNotFoundException {
        Object obj = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try {
            bis = new ByteArrayInputStream(bytes);
            ois = new ObjectInputStream(bis);
            obj = ois.readObject();
        } finally {
            if (bis != null) {
                bis.close();
            }
            if (ois != null) {
                ois.close();
            }
        }
        return obj;
    }
    
    @SuppressWarnings("unused")
	public static List<LeadEmailDO> getLeadMails(String userName, String password, String primaryEmail, String mailType) throws Exception {
		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			if (session == null) {
				Properties props = null;
	            try {
	            	props = System.getProperties();
	            	if(mailType.equalsIgnoreCase("Gmail")){
	            		props.put("mail.imap.partialfetch","false");
	 	                props.put("mail.imap.fetchsize", "1048576"); 
	 	                props.put("mail.imaps.partialfetch", "false"); 
	 	                props.put("mail.imaps.fetchsize", "1048576");
	            	}else if(mailType.equalsIgnoreCase("Exchange")){
	            		props.setProperty("mail.store.protocol", CommonConstants.EXCHANGE_IMAPS);
		            	props.put("mail.imap.auth", "true");
		            	props.put("mail.imap.socketFactory.fallback", "true");
		                props.put("mail.imap.partialfetch","false");
		                props.put("mail.imap.fetchsize", "1048576"); 
		                props.put("mail.imaps.partialfetch", "false"); 
		                props.put("mail.imaps.fetchsize", "1048576");
	            	}
	               
	            } catch (SecurityException sex) {
	            }
	            session = Session.getInstance(props, null);
	        }
			List<EmailSettingsDO> emailSettingList = new EmailSettingsService().retrieveByMailType(mailType);
			String mailId = null;
			if(emailSettingList != null && emailSettingList.size() > 0){
				if(emailSettingList.get(0).getImaphost() != null &&
						emailSettingList.get(0).getImapport() != null){
					
					Store store = null;
					if(mailType.equalsIgnoreCase("Gmail")){
						mailId = primaryEmail;
						store = session.getStore(new URLName(CommonConstants.IMAPS,emailSettingList.get(0).getImaphost(),Integer.parseInt(emailSettingList.get(0).getImapport()),CommonConstants.INBOX,userName, password));
						store.connect();
					}else if(mailType.equalsIgnoreCase("Exchange")){
						mailId = primaryEmail.split("@")[0];
						store = session.getStore(CommonConstants.EXCHANGE_IMAPS);
						userName = userName.split("@")[0];
				        store.connect(emailSettingList.get(0).getImaphost(),userName, password);
				        
					}
					
					 Folder inbox = store.getDefaultFolder();
		            inbox = inbox.getFolder("INBOX");
		            UIDFolder uf = (UIDFolder)inbox; // cast folder to UIDFolder interface
		            inbox.open(Folder.READ_WRITE);
		            if(primaryEmail != null){
			            SearchTerm sender = new FromTerm(new InternetAddress(mailId));
			            Message[] arrayMessages = inbox.search(sender);
			            //Message[] arrayMessages = inbox.getMessages();
			            ArrayUtils.reverse(arrayMessages);
			            System.out.println("Start Time :==" + new Date());    
			            FetchProfile fp = new FetchProfile();
			            fp.add(FetchProfile.Item.ENVELOPE);
			            fp.add(FetchProfileItem.FLAGS);
			            fp.add(FetchProfileItem.CONTENT_INFO);
			            fp.add("X-mailer");
			            if (inbox instanceof UIDFolder) {
			            	fp.add(UIDFolder.FetchProfileItem.UID);
			            }
			            inbox.fetch(arrayMessages, fp);
			           for (Message message : arrayMessages) {
			            	LeadEmailDO leadEmailDO = new LeadEmailDO();
							Date date = null;
					        date = CommonUtil.convertEmailDate(message.getReceivedDate().toString());
						    String formattedDate = "";
						    if( date != null ) {
						    	formattedDate = CommonUtil.convertEmailDateToDate( date );
						    }
						   Address[] fromAddress = message.getFrom();
				 			leadEmailDO.setContactId(null);
				 			leadEmailDO.setFromAddress(fromAddress[0].toString());
				 			String[] name = fromAddress[0].toString().split("<");
				 			String [] names = name[0].split(" ");
				 			if(name.length > 1) {
					 			leadEmailDO.setName(name[0]);
					 			leadEmailDO.setEmailid(name[1].substring(0, name[1].length() - 1));
				 			} else{
				 				leadEmailDO.setName(name[0].split("@")[0]);
					 			leadEmailDO.setEmailid(name[0].substring(0, name[0].length() - 1));
				 			}
				 			if(names.length > 2){
				 				leadEmailDO.setFirstname(names[0]);
				 				leadEmailDO.setMiddlename(names[1]);
				 				leadEmailDO.setLastname(names[2]);
				 			}else if(names.length > 1 && names.length == 2){
				 				leadEmailDO.setFirstname(names[0]);
				 				leadEmailDO.setLastname(names[1]);
				 			}else{
				 				leadEmailDO.setFirstname(name[0]);
				 			}
					 		leadEmailDO.setSubject(message.getSubject());
					 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
					 		leadEmailDO.setUpdatedon(new Date());
					 		leadEmailDO.setSeenMsg(message.isSet(Flags.Flag.SEEN));
							String messageContent = "";
							String attachFiles = "";
							leadEmailDO.setuId(uf.getUID(message));
					 		leadEmailDOlist.add(leadEmailDO);
						}
			           return leadEmailDOlist;
		            }
				}
			}
	       
		} catch (Exception ex) {System.out.println("Exception   " + ex.getMessage());
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
    
    @SuppressWarnings({ "unused", "deprecation" })
	public static List<LeadEmailDO> getUnReadMessageInbox(String userName, String password,String mailType) throws Exception {
		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			if (session == null) {
				Properties props = null;
	            try {
	            	props = System.getProperties();
	            	if(mailType.equalsIgnoreCase("Gmail")){
	            		props.put("mail.imap.partialfetch","false");
	 	                props.put("mail.imap.fetchsize", "1048576"); 
	 	                props.put("mail.imaps.partialfetch", "false"); 
	 	                props.put("mail.imaps.fetchsize", "1048576");
	            	}else if(mailType.equalsIgnoreCase("Exchange")){
	            		props.setProperty("mail.store.protocol", CommonConstants.EXCHANGE_IMAPS);
		            	props.put("mail.imap.auth", "true");
		            	props.put("mail.imap.socketFactory.fallback", "true");
		                props.put("mail.imap.partialfetch","false");
		                props.put("mail.imap.fetchsize", "1048576"); 
		                props.put("mail.imaps.partialfetch", "false");  
		                props.put("mail.imaps.fetchsize", "1048576");
	            	}
	               
	            } catch (SecurityException sex) {
	            }
	            session = Session.getInstance(props, null);
	        }
			List<EmailSettingsDO> emailSettingList = new EmailSettingsService().retrieveByMailType(mailType);
			
			if(emailSettingList != null && emailSettingList.size() > 0){
				if(emailSettingList.get(0).getImaphost() != null &&
						emailSettingList.get(0).getImapport() != null){
					
					Store store = null;
					if(mailType.equalsIgnoreCase("Gmail")){
						store = session.getStore(new URLName(CommonConstants.IMAPS,emailSettingList.get(0).getImaphost(),Integer.parseInt(emailSettingList.get(0).getImapport()),CommonConstants.INBOX,userName, password));
						store.connect();
					}else if(mailType.equalsIgnoreCase("Exchange")){
						store = session.getStore(CommonConstants.EXCHANGE_IMAPS);
						userName = userName.split("@")[0];
				        store.connect(emailSettingList.get(0).getImaphost(),userName, password);
					}
			        
			        Folder inbox = store.getDefaultFolder();
		            inbox = inbox.getFolder("INBOX");
		            UIDFolder uf = (UIDFolder)inbox; // cast folder to UIDFolder interface
		            inbox.open(Folder.READ_WRITE);
		            Message[] arrayMessages = inbox.getMessages();
		            ArrayUtils.reverse(arrayMessages);
		            FetchProfile fp = new FetchProfile();
		            fp.add(FetchProfile.Item.ENVELOPE);
		            fp.add(FetchProfileItem.FLAGS);
		            fp.add(FetchProfileItem.CONTENT_INFO);
		            fp.add("X-mailer");
		            if (inbox instanceof UIDFolder) {
		            	fp.add(UIDFolder.FetchProfileItem.UID);
		            }
		            inbox.fetch(arrayMessages, fp);
		           int i=0;
		           for (Message message : arrayMessages) {
		        	   if(i==0){
	        		   LeadEmailDO leadEmailDO = new LeadEmailDO();
						Date date = null;
						Date date1 = new Date();
				        date = CommonUtil.convertEmailDate(message.getReceivedDate().toString());
				        Calendar now = Calendar.getInstance();
				        Calendar maildate = Calendar.getInstance();
				        now.setTime(date1);
				        maildate.setTime(date);
				        now.add(Calendar.MINUTE, -1);
				        if(now.getTime().getHours() == maildate.getTime().getHours()){
				        	if(now.getTime().getMinutes() == maildate.getTime().getMinutes()){
					        	 String formattedDate = "";
								    if( date != null ) {
								    	formattedDate = CommonUtil.convertEmailDateToDate( date );
								    }
								   Address[] fromAddress = message.getFrom();
						 			leadEmailDO.setContactId(null);
						 			leadEmailDO.setFromAddress(fromAddress[0].toString());
							 		leadEmailDO.setSubject(message.getSubject());
							 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
							 		leadEmailDO.setUpdatedon(new Date());
									String messageContent = "";
									String attachFiles = "";
									leadEmailDO.setuId(uf.getUID(message));
							 		leadEmailDOlist.add(leadEmailDO);
					        }
				        }
				 		i++;
		        	  }
					}
				}
			}
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}

public static List<LeadEmailDO> getByMessageUid(String userName, String password,Long uid, String label,String mailType) throws Exception {
		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			if (session == null) {
				Properties props = null;
	            try {
	            	props = System.getProperties();
	            	if(mailType.equalsIgnoreCase("Gmail")){
	            		props.put("mail.imap.partialfetch","false");
		                props.put("mail.imap.fetchsize", "1048576"); 
		                props.put("mail.imaps.partialfetch", "false"); 
		                props.put("mail.imaps.fetchsize", "1048576");
	            	}else if(mailType.equalsIgnoreCase("Exchange")){
	            		props.setProperty("mail.store.protocol", CommonConstants.EXCHANGE_IMAPS);
		            	props.put("mail.imap.auth", "true");
		            	props.put("mail.imap.socketFactory.fallback", "true");
		                props.put("mail.imap.partialfetch","false");
		                props.put("mail.imap.fetchsize", "1048576"); 
		                props.put("mail.imaps.partialfetch", "false"); 
		                props.put("mail.imaps.fetchsize", "1048576");
	            	}
	               
	            } catch (SecurityException sex) {
	            }
	            session = Session.getInstance(props, null);
	        }
			List<EmailSettingsDO> emailSettingList = new EmailSettingsService().retrieveByMailType(mailType);
			String type = null;
			String folderName = null;
			if(label.equalsIgnoreCase("Inbox")){
				type = CommonConstants.INBOX;
				folderName = CommonConstants.INBOX;;
			} else {
				type = CommonConstants.SENT_MAIL;
				folderName = CommonConstants.SENT_MAIL;
			}
			if(emailSettingList != null && emailSettingList.size() > 0){
				if(emailSettingList.get(0).getImaphost() != null &&
						emailSettingList.get(0).getImapport() != null){
					
					Store store = null;
					if(mailType.equalsIgnoreCase("Gmail")){
						store = session.getStore(new URLName(CommonConstants.IMAPS,emailSettingList.get(0).getImaphost(),Integer.parseInt(emailSettingList.get(0).getImapport()),type,userName, password));
						store.connect();
					}else if(mailType.equalsIgnoreCase("Exchange")){
						store = session.getStore(CommonConstants.EXCHANGE_IMAPS);
						userName = userName.split("@")[0];
				        store.connect(emailSettingList.get(0).getImaphost(),userName, password);
					}
	        
		        
		       
		        Folder inbox = store.getDefaultFolder();
		       
	            inbox = inbox.getFolder(folderName);
	            UIDFolder uf = (UIDFolder)inbox; // cast folder to UIDFolder interface
	            inbox.open(Folder.READ_WRITE);
	            Message message = uf.getMessageByUID(uid);
	            message.setFlag(Flag.SEEN, true);
	        	LeadEmailDO leadEmailDO = new LeadEmailDO();
				Date date = null;
		        date = CommonUtil.convertEmailDate(message.getSentDate().toString()); 
			    String formattedDate = "";
			    if( date != null ) {
			    	formattedDate = CommonUtil.convertEmailDateToDate( date );
			    }
			    Address[] fromAddress = message.getFrom();
				leadEmailDO.setContactId(null);
				leadEmailDO.setFromAddress(fromAddress[0].toString());
				String[] name = fromAddress[0].toString().split("<");
				String [] names = name[0].split(" ");
				if(name.length > 1) {
		 			leadEmailDO.setName(name[0]);
		 			leadEmailDO.setEmailid(name[1].substring(0, name[1].length() - 1));
	 			} else{
	 				leadEmailDO.setName(name[0].split("@")[0]);
		 			leadEmailDO.setEmailid(name[0].substring(0, name[0].length() - 1));
	 			}
	 			if(names.length > 2){
	 				leadEmailDO.setFirstname(names[0]);
	 				leadEmailDO.setMiddlename(names[1]);
	 				leadEmailDO.setLastname(names[2]);
	 			}else if(names.length > 1 && names.length == 2){
	 				leadEmailDO.setFirstname(names[0]);
	 				leadEmailDO.setLastname(names[1]);
	 			}else{
	 				leadEmailDO.setFirstname(name[0]);
	 			}
				leadEmailDO.setSubject(message.getSubject()); 
				leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
				leadEmailDO.setUpdatedon(new Date());
				leadEmailDO.setSeenMsg(true);
				String messageContent = "";
				leadEmailDO.setuId(uid);
				Object content = message.getContent();
				if (content instanceof String) {
	                messageContent = content.toString();
				}else if (content instanceof Multipart){
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                    	
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                        byte[] bytes = IOUtils.toByteArray(inStream);
		                        leadEmailDO.setContentType(bodyPart.getContentType());
		                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
	                            inStream.close();
		                    }
		                }
		           }
					leadEmailDO.setMessageContent(messageContent);
					leadEmailDOlist.add(leadEmailDO);
			}
          }
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
}
