package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.GSTTaxSlabDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.GSTTaxSlabService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.util.CommonConstants;

public class ProjectUtil {
	
	private ProjectUtil() {}
	
	public static JSONObject getProjectList(List<ProjectDO> projectList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ProjectDO project : projectList) {
				resultJSONArray.put(getProjectDetailObject(project));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getProjectDetailObject(ProjectDO project)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(project.getId()));
		
		if(project.getOpportunityId() != null){
			List<OpportunitiesDO> opportunityList = new OpportunitiesService().retrieveById(project.getOpportunityId());
			result.put(CommonConstants.OPPORTUNITY_ID, String.valueOf(project.getOpportunityId()));
			if(opportunityList != null && opportunityList.size() > 0){
				result.put(CommonConstants.OPPORTUNITY_NAME, String.valueOf(opportunityList.get(0).getProjectname()));
			}else{
				result.put(CommonConstants.OPPORTUNITY_NAME, "");
			}
		}else{
			result.put(CommonConstants.OPPORTUNITY_ID, "");
			result.put(CommonConstants.OPPORTUNITY_NAME, "");
		}
		if(project.getProjectname() != null){
			result.put(CommonConstants.PROJECTNAME, String.valueOf(project.getProjectname()));
		}else{
			result.put(CommonConstants.PROJECTNAME,"");
		}
		if(project.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(project.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME,"");
		}
		if(project.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(project.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(project.getEnddate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(project.getEnddate()));
		}else{
			result.put(CommonConstants.ENDDATE,"");
		}
		if(project.getEmpId() != null){
			result.put(CommonConstants.UPDATED_BY, String.valueOf(project.getEmpId()));
		}else{
			result.put(CommonConstants.UPDATED_BY,"");
		}
		if(project.getProjectduration() != null){
			result.put(CommonConstants.PROJECTDURATION, String.valueOf(project.getProjectduration()));
		}else{
			result.put(CommonConstants.PROJECTDURATION,"");
		}
		if(project.getComment() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(project.getComment()));
		}else{
			result.put(CommonConstants.COMMENT,"");
		}
		if(project.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(project.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY_NAME, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(project.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(project.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON,"");
		}
		
		if(project.getAmount() != null){
			result.put(CommonConstants.COST, String.valueOf(project.getAmount()));
		}else{
			result.put(CommonConstants.COST, "");
		}
		
		if(project.getProbability() != null){
			result.put(CommonConstants.PROBOBILITY, String.valueOf(project.getProbability()));
		}else{
			result.put(CommonConstants.PROBOBILITY, "");
		}
		
		if(project.getGstId() != null){
			List<GSTTaxSlabDO> gstTaxList = new GSTTaxSlabService().retrieveById(project.getGstId());
			result.put(CommonConstants.GST, String.valueOf(project.getGstId()));
			if(gstTaxList != null && gstTaxList.size() > 0){
				result.put(CommonConstants.GST_PERCENTAGE, String.valueOf(gstTaxList.get(0).getPercentage()));
			}else{
				result.put(CommonConstants.GST_PERCENTAGE, "");
			}
		}else{
			result.put(CommonConstants.GST, "");
			result.put(CommonConstants.GST_PERCENTAGE, "");
		}
		
		return result;
	}
}
