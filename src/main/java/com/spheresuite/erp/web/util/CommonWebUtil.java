package com.spheresuite.erp.web.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.spheresuite.erp.util.CommonConstants;

public final class CommonWebUtil {

	private CommonWebUtil() {

	}

	public static JSONObject buildSuccessResponse() {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		JSONObject dataJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			resultJSON.put(CommonConstants.RESULTS, dataJSON);
			
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject buildErrorResponse(String errorMsg) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		JSONObject dataJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.FALSE);
			resultJSON.put(CommonConstants.ERRORS, errorMsg);
			//resultJSON.put(CommonConstants.ERROR_CODE, errorCode);
			resultJSON.put(CommonConstants.RESULTS, dataJSON);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
		
	public static org.json.simple.JSONObject getInputParams(String inputParams) {
		JSONParser parser = new JSONParser(); 
		org.json.simple.JSONObject inputJSON = new org.json.simple.JSONObject();
		try {
			inputJSON = (org.json.simple.JSONObject) parser.parse(inputParams);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return inputJSON;
	}
	
	public static org.json.simple.JSONArray getInputParamsArray(String inputParams) {
		JSONParser parser = new JSONParser(); 
		org.json.simple.JSONArray inputJSON = new org.json.simple.JSONArray();
		try {
			inputJSON = (org.json.simple.JSONArray) parser.parse(inputParams);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return inputJSON;
	}
	
	public static JSONObject buildSuccessResponseId(Long id) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			resultJSON.put(CommonConstants.RESULTS, id);
			
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject buildAuthenticationErrorResponse(String errorCode) {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.FALSE);
			resultJSON.put(CommonConstants.ERROR_CODE, errorCode);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
}
