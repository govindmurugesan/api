package com.spheresuite.erp.web.util;

import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeDeductionDO;
import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.domainobject.EmployeeTaxDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.DeductionSettingsService;
import com.spheresuite.erp.service.EmployeeBonusService;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeDeductionService;
import com.spheresuite.erp.service.EmployeeEarningsService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeePayrollService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTaxService;
import com.spheresuite.erp.service.TaxSettingsService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeCompensationUtil {
	
	private EmployeeCompensationUtil() {}
	
	public static JSONObject getempCompensationList(List<EmployeeCompensationDO> empCompensationList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				List<EmployeeDO> employeeList = new EmployeeService().isEmpActive(empCompensation.getEmpId());
				if(employeeList != null && employeeList.size() > 0){
					if(employeeList.get(0).getStatus() == 'a'){
						resultJSONArray.put(getCompensationDetailObject(empCompensation, null));
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithEarningList(List<EmployeeCompensationDO> empCompensationList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				List<EmployeeDO> employeeList = new EmployeeService().isEmpActive(empCompensation.getEmpId());
				if(employeeList != null && employeeList.size() > 0){
					if(employeeList.get(0).getStatus() == 'a'){
						resultJSONArray.put(getCompensationDetailObjectWithEarnings(empCompensation, null));
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithBatch(List<EmployeeCompensationDO> empCompensationList, Long batchId)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				List<EmployeeDO> employeeList = new EmployeeService().isEmpActive(empCompensation.getEmpId());
				if(employeeList != null && employeeList.size() > 0){
					if(employeeList.get(0).getStatus() == 'a'){
						List<EmployeeCtcDO> compensationList = new EmployeeCtcService().retrieveActive(empCompensation.getEmpId());
						if(compensationList != null && compensationList.size() > 0){
							if(compensationList.get(0).getPayrollbatchId() != null && compensationList.get(0).getPayrollbatchId().equals(batchId)){
								resultJSONArray.put(getCompensationDetailObject(empCompensation, null));
							}
						}
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithBatch(List<EmployeeCompensationDO> empCompensationList, Long batchId, String payMonth)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			Map<Long, Long> empIds = new HashMap<Long,Long>();  
			for (EmployeeCompensationDO empCompensation : empCompensationList) {
				if(CommonUtil.convertDateToStringWithOutDate(payMonth).after(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(empCompensation.getEffectivefrom())))
						|| CommonUtil.convertDateToStringWithOutDate(payMonth).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(empCompensation.getEffectivefrom())))){
					if(empCompensation.getEffectiveto() == null || CommonUtil.convertDateToStringWithOutDate(payMonth).before(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(empCompensation.getEffectiveto())))
							|| CommonUtil.convertDateToStringWithOutDate(payMonth).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(empCompensation.getEffectiveto())))){
						List<EmployeeDO> employeeList = new EmployeeService().isEmpActive(empCompensation.getEmpId());
						if(employeeList != null && employeeList.size() > 0){
							if(employeeList.get(0).getStatus() == 'a'){
								List<EmployeeCtcDO> compensationList = new EmployeeCtcService().retrieveActive(empCompensation.getEmpId());
								if(compensationList != null && compensationList.size() > 0){
									if(compensationList.get(0).getStartdate() != null && CommonUtil.convertDateToStringWithOutDate(payMonth).after(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensationList.get(0).getStartdate())))
											|| CommonUtil.convertDateToStringWithOutDate(payMonth).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensationList.get(0).getStartdate())))){
										if(compensationList.get(0).getEnddate() == null || CommonUtil.convertDateToStringWithOutDate(payMonth).before(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensationList.get(0).getEnddate())))){
											if(compensationList.get(0).getPayrollbatchId() != null && compensationList.get(0).getPayrollbatchId().equals(batchId)){
												if(!empIds.containsKey(empCompensation.getEmpId())){
													int i = 0;
													for (EmployeeCompensationDO tempList : empCompensationList) {
														if(empCompensation.getEmpId().equals(tempList.getEmpId())){
															i++;
														}
													}
													
													if(i > 1){
														for (EmployeeCompensationDO tempList : empCompensationList) {
															if(empCompensation.getEmpId().equals(tempList.getEmpId()) && tempList.getStatus() == 'a'){
																empIds.put(empCompensation.getEmpId(), empCompensation.getEmpId());
																resultJSONArray.put(getCompensationDetailObject(empCompensation,payMonth));
															}
														}
													}else{
														empIds.put(empCompensation.getEmpId(), empCompensation.getEmpId());
														resultJSONArray.put(getCompensationDetailObject(empCompensation,payMonth));
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCompensationDetailObject(EmployeeCompensationDO empCompensation , String payrollMonth)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEffectivefrom()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEffectiveto()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		List<EmployeePayrollDO> payrollList = new EmployeePayrollService().retriveByMonthAndEmp(payrollMonth, empCompensation.getEmpId());
		if(payrollList.size() > 0){
			result.put(CommonConstants.PAYROLLSTATUS, String.valueOf(payrollList.get(0).getStatus()));
		}else{
			result.put(CommonConstants.PAYROLLSTATUS, String.valueOf(CommonConstants.NEW));
		}
		
		Long bonusAmountMonth = 0L;
		if(payrollMonth != null){
			List<EmployeeBonusDO> employeeBonusList = new EmployeeBonusService().retrieveByEmpIdAndMonth(empCompensation.getEmpId(), payrollMonth);
			if(employeeBonusList != null && employeeBonusList.size() > 0){
				bonusAmountMonth = employeeBonusList.get(0).getAmount();
			}
		}
		//bonusAmountMonth = (long) (bonusAmountMonth + empCompensation.getNetmonth());
		int noOfDaysLop = 0;
		if(payrollMonth != null){
			List<EmployeeLopDO> employeeLopList = new EmployeeLopService().retrieveByEmpId(empCompensation.getEmpId());
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(payrollMonth.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		
		Double lossOfPayAmount = 0D;
		if(payrollMonth != null){
			List<EmployeeCtcDO> ctcList = new EmployeeCtcService().retrieveActive(empCompensation.getEmpId());
			if(ctcList != null && ctcList.size() > 0){
				result.put(CommonConstants.EMPCTC, String.valueOf(ctcList.get(0).getEmpctc()));
				Long ctc = ctcList.get(0).getEmpctc()/12;
				Date s = CommonUtil.convertDateToStringWithOutDate(payrollMonth);
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = ctc/days;
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
				result.put(CommonConstants.BACSICPAYLABEL, String.valueOf(ctc/2));
			}else{
				result.put(CommonConstants.EMPCTC, "");
				result.put(CommonConstants.BACSICPAYLABEL, "");
			}
		}else{
			result.put(CommonConstants.EMPCTC, "");
			result.put(CommonConstants.BACSICPAYLABEL, "");
		}
		
		result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(lossOfPayAmount));
		result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(lossOfPayAmount * 12));
		
		
		if(empCompensation.getEmpId() != null && payrollMonth != null){
			List<EmployeeDO> employee = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
			if(employee != null && employee.size() > 0){
				if(employee.get(0).getStartdate() != null){
					String splitMonth = employee.get(0).getStartdate().substring(0, 10);
					String joiningMonth = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertStringToSqlDate(splitMonth));
					//String[] joinMonthArray = joiningMonth.split(",");
					if(joiningMonth.equalsIgnoreCase(payrollMonth)){
						Date joiningDate = CommonUtil.convertStringToSqlDate(employee.get(0).getStartdate());
						Calendar c=Calendar.getInstance();
				        c.setTime(joiningDate);
				        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				        String[] noofdays = splitMonth.split("-");
				        int totalDays = days;
				        if(!noofdays[2].equals("01")){
				        	 totalDays = days - Integer.parseInt(noofdays[2]);
				        }
				        Double perdaySalary = empCompensation.getEarningmonth()/days;
				        result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(perdaySalary * totalDays) + bonusAmountMonth);
				        Double perdayTax = 0D;
				        if(empCompensation.getTaxmonth() != null && empCompensation.getTaxmonth() > 0){
				        	perdayTax = empCompensation.getTaxmonth()/days;
				        	result.put(CommonConstants.TAXMONTHLY, String.valueOf(perdayTax * totalDays));
				        }else{
				        	result.put(CommonConstants.TAXMONTHLY, String.valueOf(perdayTax * totalDays));
				        }
				        Double perdayDeduction = 0D;
				        if(empCompensation.getDeductionmonth() != null && empCompensation.getDeductionmonth() > 0){
				        	perdayDeduction = empCompensation.getDeductionmonth()/days;
				        	result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf((perdayDeduction * totalDays) + lossOfPayAmount));
				        }else{
				        	result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf((perdayDeduction * totalDays) + lossOfPayAmount));
				        }
				        Double netmonthly = 0D;
				        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
				        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
						result.put(CommonConstants.NETMONTHLY, String.valueOf(netmonthly));
						
					}else{
						result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(empCompensation.getEarningmonth() + bonusAmountMonth));
						result.put(CommonConstants.TAXMONTHLY, String.valueOf(empCompensation.getTaxmonth()));
						result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(empCompensation.getDeductionmonth() + lossOfPayAmount));
						Double netmonthly = 0D;
				        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
				        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
						result.put(CommonConstants.NETMONTHLY, String.valueOf(netmonthly));
					}
				}else{
					result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(empCompensation.getEarningmonth() + bonusAmountMonth));
					result.put(CommonConstants.TAXMONTHLY, String.valueOf(empCompensation.getTaxmonth()));
					result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(empCompensation.getDeductionmonth() + lossOfPayAmount));
					Double netmonthly = 0D;
			        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
			        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
					result.put(CommonConstants.NETMONTHLY, String.valueOf(netmonthly));
				}
				
			}else{
				result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(empCompensation.getEarningmonth() + bonusAmountMonth));
				result.put(CommonConstants.TAXMONTHLY, String.valueOf(empCompensation.getTaxmonth()));
				result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(empCompensation.getDeductionmonth() + lossOfPayAmount));
				Double netmonthly = 0D;
		        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
		        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
				result.put(CommonConstants.NETMONTHLY, String.valueOf(netmonthly));
			}
		}else{
			result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(empCompensation.getEarningmonth()));
			result.put(CommonConstants.TAXMONTHLY, String.valueOf(empCompensation.getTaxmonth()));
			result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(empCompensation.getDeductionmonth()));
			Double netmonthly = 0D;
	        netmonthly = Double.parseDouble(result.get(CommonConstants.EARNINGMONTHLY).toString()) - (Double.parseDouble(result.get(CommonConstants.TAXMONTHLY).toString())
	        		+ Double.parseDouble(result.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
			result.put(CommonConstants.NETMONTHLY, String.valueOf(netmonthly));
		}
		
		result.put(CommonConstants.EARNINGYEAR, String.valueOf(empCompensation.getEarningyear()));
		result.put(CommonConstants.NETYEAR, String.valueOf(empCompensation.getNetyear()));
		result.put(CommonConstants.TAXYEAR, String.valueOf(empCompensation.getTaxyear()));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(empCompensation.getGrossPay()));
		result.put(CommonConstants.DEDUCTIONYEAR, String.valueOf(empCompensation.getDeductionyear() != null ? empCompensation.getDeductionyear() : 0));
		if(empCompensation.getEmpId() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(empCompensation.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(empCompensation.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(empCompensation.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getCompensationDetailObjectWithEarnings(EmployeeCompensationDO empCompensation , String payrollMonth)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEffectivefrom()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEffectiveto()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		Long bonusAmountMonth = 0L;
		if(payrollMonth != null){
			List<EmployeeBonusDO> employeeBonusList = new EmployeeBonusService().retrieveByEmpIdAndMonth(empCompensation.getEmpId(), payrollMonth);
			if(employeeBonusList != null && employeeBonusList.size() > 0){
				bonusAmountMonth = employeeBonusList.get(0).getAmount();
			}
		}
		
		bonusAmountMonth = (long) (bonusAmountMonth + empCompensation.getNetmonth());
		int noOfDaysLop = 0;
		if(payrollMonth != null){
			List<EmployeeLopDO> employeeLopList = new EmployeeLopService().retrieveByEmpId(empCompensation.getEmpId());
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(payrollMonth.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		
		
		result.put(CommonConstants.NETYEAR, String.valueOf(empCompensation.getNetyear()));
		result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(empCompensation.getEarningmonth()));
		result.put(CommonConstants.EARNINGYEAR, String.valueOf(empCompensation.getEarningyear()));
		
		result.put(CommonConstants.TAXMONTHLY, String.valueOf(empCompensation.getTaxmonth()));
		result.put(CommonConstants.TAXYEAR, String.valueOf(empCompensation.getTaxyear()));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(empCompensation.getGrossPay()));
		Double lossOfPayAmount = 0D;
		if(payrollMonth != null){
			List<EmployeeCtcDO> ctcList = new EmployeeCtcService().retrieveActive(empCompensation.getEmpId());
			if(ctcList != null && ctcList.size() > 0){
				result.put(CommonConstants.EMPCTC, String.valueOf(ctcList.get(0).getEmpctc()));
				Long ctc = ctcList.get(0).getEmpctc()/12;
				Date s = CommonUtil.convertDateToStringWithOutDate(payrollMonth);
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = ctc/days;
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
				result.put(CommonConstants.BACSICPAYLABEL, String.valueOf(ctc/2));
			}else{
				result.put(CommonConstants.EMPCTC, "");
				result.put(CommonConstants.BACSICPAYLABEL, "");
			}
		}else{
			result.put(CommonConstants.EMPCTC, "");
			result.put(CommonConstants.BACSICPAYLABEL, "");
		}
		
		result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(lossOfPayAmount));
		result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(lossOfPayAmount * 12));
		
		result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(empCompensation.getDeductionmonth() != null ?  empCompensation.getDeductionmonth()+ lossOfPayAmount : lossOfPayAmount));
		result.put(CommonConstants.NETMONTHLY, String.valueOf(bonusAmountMonth  - lossOfPayAmount));
		result.put(CommonConstants.DEDUCTIONYEAR, String.valueOf(empCompensation.getDeductionyear() != null ? empCompensation.getDeductionyear() : 0));
		List<EmployeeEarningsDO> earningsList = new EmployeeEarningsService().retrieveByEmpCompensationId(empCompensation.getId());
		List<EmployeeTaxDO> taxList = new EmployeeTaxService().retrieveByEmpCompensationId(empCompensation.getId());
		List<EmployeeDeductionDO> deductionList = new EmployeeDeductionService().retrieveByEmpCompensationId(empCompensation.getId());
		JSONArray resultJSONArray = new JSONArray();
		
		if(earningsList != null && earningsList.size() > 0){
			for (EmployeeEarningsDO empEarnings : earningsList) {
				resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings));
			}
			result.put(CommonConstants.EARNINGLIST, resultJSONArray);
		}else{
			result.put(CommonConstants.EARNINGLIST,"");
		}
		JSONArray resultJSONArray1 = new JSONArray();
		
		if(deductionList != null && deductionList.size() > 0){
			for (EmployeeDeductionDO empDeduction : deductionList) {
				resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction));
			}
			result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
		}else{
			result.put(CommonConstants.DEDUCTIONLIST,"");
		}
		
		JSONArray resultJSONArray2 = new JSONArray();
		
		if(taxList != null && taxList.size() > 0){
			for (EmployeeTaxDO empDeduction : taxList) {
				resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction));
			}
			result.put(CommonConstants.TAXLIST, resultJSONArray2);
		}else{
			result.put(CommonConstants.TAXLIST,"");
		}
		if(empCompensation.getEmpId() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(empCompensation.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(empCompensation.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(empCompensation.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getEmployeeEarningsDetailObject(EmployeeEarningsDO empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.EARNINGID, String.valueOf(empCompensation.getEarningsId()));
		List<AllowanceSettingDO> allowanceList = new AllowanceSettingsService().retrieveById(empCompensation.getEarningsId());
		if(allowanceList != null && allowanceList.size() > 0){
			result.put(CommonConstants.EARNINGNAME, String.valueOf(allowanceList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.EARNINGNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
		result.put(CommonConstants.YTD, String.valueOf(empCompensation.getYtd()));
		return result;
	}
	
	public static JSONObject getEmployeeDeductionDetailObject(EmployeeDeductionDO empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(empCompensation.getEmpdeductionid()));
		List<DeductionSettingDO> deductionList = new DeductionSettingsService().retrieveById(empCompensation.getEmpdeductionid());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.DEDUCTIONNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
		result.put(CommonConstants.YTD, String.valueOf(empCompensation.getYtd()));
		return result;
	}
	
	public static JSONObject getEmployeeTaxDetailObject(EmployeeTaxDO empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.TAXID, String.valueOf(empCompensation.getEmptaxid()));
		List<TaxSettingsDO> deductionList = new TaxSettingsService().retrieveById(empCompensation.getEmptaxid());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.TAXNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.TAXNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
		result.put(CommonConstants.YTD, String.valueOf(empCompensation.getYtd()));
		return result;
	}
}
