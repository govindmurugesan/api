package com.spheresuite.erp.web.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.DeductionSettingsService;
import com.spheresuite.erp.service.EmployeeBonusService;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeePayrollService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.service.PayrollDeductionDetailsService;
import com.spheresuite.erp.service.PayrollEarningsDetailsService;
import com.spheresuite.erp.service.PayrollTaxDetailsService;
import com.spheresuite.erp.service.TaxSettingsService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeePayrollUtil {
	
	private EmployeePayrollUtil() {}
	
	public static JSONObject getempPayrollList(List<EmployeePayrollDO> employeePayrollList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO empCompensation : employeePayrollList) {
				List<EmployeeDO> employeeList = new EmployeeService().isEmpActive(empCompensation.getEmpId());
				if(employeeList.get(0).getStatus() == 'a'){
					resultJSONArray.put(getPayrollDetailObject(empCompensation));
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject ObjectArray(List<Object[]> employeePayrollList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (Object[] empCompensation : employeePayrollList) {
					resultJSONArray.put(getObjectDetailObject(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject SingleMonthDeductionReport(List<EmployeePayrollDO> payrollList, String frequency, String period)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO payroll : payrollList) {
					resultJSONArray.put(getSingleMonthDetailObject(payroll));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject YtdReport(List<EmployeePayrollDO> payrollList, String fromMonth, String frequency)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(fromMonth != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(fromMonth));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			JSONArray resultJSONArray = new JSONArray();
			Map<Long, Long> empids = new HashMap<Long,Long>();
			for (EmployeePayrollDO payroll : payrollList) {
				if(!empids.containsKey(payroll.getEmpId())){
					empids.put(payroll.getEmpId(), payroll.getEmpId());
					resultJSONArray.put(getSingleMonthYtd(payroll, fromMonth));
				}
					
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject YtdForMultipleMonth(List<EmployeePayrollDO> payrollList, String fromMonth, String toMonth, String frequency)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			resultJSON.put(CommonConstants.PERIOD, String.valueOf(fromMonth+" - "+toMonth));
			JSONArray resultJSONArray = new JSONArray();
			Map<Long, Long> empids = new HashMap<Long,Long>();
			for (EmployeePayrollDO payroll : payrollList) {
				if(!empids.containsKey(payroll.getEmpId())){
					empids.put(payroll.getEmpId(), payroll.getEmpId());
					resultJSONArray.put(getMultipleMonthYtd(payroll, fromMonth, toMonth));
				}
					
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMultipleMonthYtd(EmployeePayrollDO payroll, String fromMonth, String toMonth)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		Long bonusMonth = 0L;
		/*List<EmployeeBonusDO> employeeBonusList = new EmployeeBonusService().retrieveByEmpIdAndMonth(payroll.getEmpId(), fromMonth);
		if(employeeBonusList != null && employeeBonusList.size() > 0){
			bonusMonth = employeeBonusList.get(0).getAmount();
		}*/
		
		if(payroll.getEmpId() != null){
			result.put(CommonConstants.EMPID, String.valueOf(payroll.getEmpId()));
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(payroll.getEmpId() );
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
			result.put(CommonConstants.EMPID, String.valueOf(""));
		}
		
		Object[] earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDateSum(payroll.getEmpId(), fromMonth, toMonth);
		if(earningYear != null){
			double eraningYear = 0D;
			double deductionYear = 0D;
			double taxYear = 0D;
			
			if(earningYear[1] != null){
				eraningYear = Double.parseDouble(earningYear[1].toString());
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(eraningYear));
			}else{
				eraningYear = bonusMonth;
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(eraningYear));
			}
			
			if(earningYear[2] != null){
				deductionYear = Double.parseDouble(earningYear[2].toString());
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(deductionYear));
			}else{
				deductionYear = 0D;
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(deductionYear));
			}
			
			if(earningYear[3] != null){
				taxYear = Double.parseDouble(earningYear[3].toString());
				result.put(CommonConstants.TAXYEAR,String.valueOf(Double.parseDouble(earningYear[3].toString())));
			}else{
				taxYear = 0D;
				result.put(CommonConstants.TAXYEAR,String.valueOf(taxYear));
			}
			result.put(CommonConstants.NETYEAR,String.valueOf(eraningYear - (deductionYear+taxYear)));
			
		}else{
			result.put(CommonConstants.EARNINGYEAR,String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(""));
			result.put(CommonConstants.TAXYEAR,String.valueOf(""));
			result.put(CommonConstants.NETYEAR,String.valueOf(""));
		}
		
		return result;
	}
	
	public static JSONObject getSingleMonthYtd(EmployeePayrollDO payroll, String fromMonth)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		Long bonusMonth = 0L;
		/*List<EmployeeBonusDO> employeeBonusList = new EmployeeBonusService().retrieveByEmpIdAndMonth(payroll.getEmpId(), fromMonth);
		if(employeeBonusList != null && employeeBonusList.size() > 0){
			bonusMonth = employeeBonusList.get(0).getAmount();
		}*/
		
		if(payroll.getEmpId() != null){
			result.put(CommonConstants.EMPID, String.valueOf(payroll.getEmpId()));
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(payroll.getEmpId() );
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
			result.put(CommonConstants.EMPID, String.valueOf(""));
		}
		
		Object[] earningYear = new EmployeePayrollService().retrieveByEmpIdForMonthSum(payroll.getEmpId(), fromMonth);
		if(earningYear != null){
			double eraningYear = 0D;
			double deductionYear = 0D;
			double taxYear = 0D;
			
			if(earningYear[1] != null){
				eraningYear = Double.parseDouble(earningYear[1].toString());
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(eraningYear));
			}else{
				eraningYear = bonusMonth;
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(eraningYear));
			}
			
			if(earningYear[2] != null){
				deductionYear = Double.parseDouble(earningYear[2].toString());
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(deductionYear));
			}else{
				deductionYear = 0D;
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(deductionYear));
			}
			
			if(earningYear[3] != null){
				taxYear = Double.parseDouble(earningYear[3].toString());
				result.put(CommonConstants.TAXYEAR,String.valueOf(Double.parseDouble(earningYear[3].toString())));
			}else{
				taxYear = 0D;
				result.put(CommonConstants.TAXYEAR,String.valueOf(taxYear));
			}
			result.put(CommonConstants.NETYEAR,String.valueOf(eraningYear - (deductionYear+taxYear)));
			
		}else{
			result.put(CommonConstants.EARNINGYEAR,String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(""));
			result.put(CommonConstants.TAXYEAR,String.valueOf(""));
			result.put(CommonConstants.NETYEAR,String.valueOf(""));
		}
		
		return result;
	}
	
	public static JSONObject MultipleMonthDeductionReport(List<EmployeePayrollDO> payrollList, String fromMonth, String toMonth, String frequency)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			resultJSON.put(CommonConstants.PERIOD, String.valueOf(fromMonth +" - "+toMonth));
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO empdeduction : payrollList) {
				 Map<Long,Long> map=new HashMap<Long,Long>();
				 if(!map.containsKey(empdeduction.getEmpId())){
					 List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retrieveByEmpIdBetweenDate(empdeduction.getEmpId(), fromMonth, toMonth);
					 resultJSONArray.put(getMultipleMonthDetailObject(compensationList, fromMonth, toMonth));
				 }
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMultipleMonthDetailObject(List<EmployeePayrollDO> empCompensation, String fromMonth, String toMonth)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		Map<Long,Long> deduction = new HashMap<Long,Long>();
		if(empCompensation.get(0).getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(empCompensation.get(0).getEmpId() );
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}
		List<Long> payrollIds = new ArrayList<Long>();
		if(empCompensation != null && empCompensation.size() > 0){
			for (EmployeePayrollDO employeePayrollDO : empCompensation) {
				 payrollIds.add(employeePayrollDO.getId());
			}
		}
		 JSONArray resultJSONArray = new JSONArray();
		 List<PayrollDeductionDetailsDO> payrollDeductionList = new PayrollDeductionDetailsService().retrieveByEmpCompensationIds(payrollIds);
		 if(payrollDeductionList != null && payrollDeductionList.size() > 0){
			 for (PayrollDeductionDetailsDO payrollDeductionDetailsDO : payrollDeductionList) {
				 Double amount = 0D;
				 Long deductionId = 0L;
				
				 if(!deduction.containsKey(payrollDeductionDetailsDO.getDeductionId())){
					 deduction.put(payrollDeductionDetailsDO.getDeductionId(),payrollDeductionDetailsDO.getDeductionId());
					 deductionId = payrollDeductionDetailsDO.getDeductionId();
					 for (PayrollDeductionDetailsDO payrollDeductionDetails : payrollDeductionList) {
						 if(payrollDeductionDetails.getDeductionId().equals(deductionId)){
							 amount = amount + payrollDeductionDetails.getMonthly();
						 }
					 }
					 resultJSONArray.put(getDeuctionDetatilsWithAmoount(payrollDeductionDetailsDO, amount));
				 }
			}
			 JSONArray resultJSONArrayTemp = new JSONArray();
			 List<DeductionSettingDO> deductionList = new DeductionSettingsService().retrieve();
			 if(deductionList != null && deductionList.size() > 0){
					for (DeductionSettingDO deductionobj : deductionList) {
						JSONObject deductionResult = new JSONObject();
						if(resultJSONArray.length() <= 0){
							deductionResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
							deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionobj.getId()));
							deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionobj.getName()));
						}else{
							String amount="";
							int count = 0;
							for(int i= 0; i<resultJSONArray.length();i++){
								amount="";
								count = 0;
								JSONObject inputJSON1 = (JSONObject) resultJSONArray.get(i);
								if(deductionobj.getId().equals(Long.parseLong(inputJSON1.get(CommonConstants.DEDUCTIONID).toString()))){
									count++;
									amount = inputJSON1.get(CommonConstants.AMOUNT).toString();
								}
							}
							if(count > 0){
								deductionResult.put(CommonConstants.AMOUNT, String.valueOf(amount));
								deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionobj.getId()));
								deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionobj.getName()));
							}else{
								deductionResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
								deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionobj.getId()));
								deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionobj.getName()));
							}
						}
						resultJSONArrayTemp.put(deductionResult);
					}
					result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
				}else{
					result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
				}
			 
		 }else{
			 result.put(CommonConstants.DEDUCTIONLIST, "");
		 }
		return result;
	}
	
	public static JSONObject getSingleMonthDetailObject(EmployeePayrollDO empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		if(empCompensation != null){
			if(empCompensation.getEmpId() != null){
				List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
				if(empList != null && empList.size() > 0){
					if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
					else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}else{
					result.put(CommonConstants.EMPNAME, ""); 
				}
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
			List<DeductionSettingDO> deductionList = new DeductionSettingsService().retrieve();
			List<PayrollDeductionDetailsDO> payrollDeductionList = new PayrollDeductionDetailsService().retrieveByEmpCompensationId(empCompensation.getId());
			JSONArray resultJSONArray = new JSONArray();
			JSONArray resultJSONArrayTemp = new JSONArray();
			if(payrollDeductionList != null && payrollDeductionList.size()  > 0){
				for (PayrollDeductionDetailsDO payrollDeductionDetailsDO : payrollDeductionList) {
					resultJSONArray.put(getDeuctionDetatils(payrollDeductionDetailsDO));
				}
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
				
			}else{
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
			}
			if(deductionList != null && deductionList.size() > 0){
				for (DeductionSettingDO deduction : deductionList) {
					JSONObject deductionResult = new JSONObject();
					if(resultJSONArray.length() <= 0){
						deductionResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
						deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deduction.getId()));
						deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deduction.getName()));
					}else{
						String amount="";
						int count = 0;
						for(int i= 0; i<resultJSONArray.length();i++){
							JSONObject inputJSON1 = (JSONObject) resultJSONArray.get(i);
							if(deduction.getId().equals(Long.parseLong(inputJSON1.get(CommonConstants.DEDUCTIONID).toString()))){
								count++;
								amount = inputJSON1.get(CommonConstants.AMOUNT).toString();
							}
						}
						if(count > 0){
							deductionResult.put(CommonConstants.AMOUNT, String.valueOf(amount));
							deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deduction.getId()));
							deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deduction.getName()));
						}else{
							deductionResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
							deductionResult.put(CommonConstants.DEDUCTIONID, String.valueOf(deduction.getId()));
							deductionResult.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deduction.getName()));
						}
					}
					resultJSONArrayTemp.put(deductionResult);
				}
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
			}else{
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArrayTemp);
			}
		}else{
			result.put(CommonConstants.DEDUCTIONLIST, "");
		}
		
		return result;
	}
	public static JSONObject getDeuctionDetatils(PayrollDeductionDetailsDO payrollDeduction)throws JSONException, AppException {
		List<DeductionSettingDO> deductionList = new DeductionSettingsService().retrieveById(payrollDeduction.getDeductionId());
		JSONObject result = new JSONObject();
		if(deductionList != null && deductionList.size() > 0 ){
			result.put(CommonConstants.AMOUNT, String.valueOf(payrollDeduction.getMonthly()));
			result.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionList.get(0).getId()));
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getName()));
			
		}else{
			result.put(CommonConstants.DEDUCTIONID, String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(""));
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}
		return result;
	}
	
	public static JSONObject getDeuctionDetatilsWithAmoount(PayrollDeductionDetailsDO payrollDeduction, Double amount)throws JSONException, AppException {
		List<DeductionSettingDO> deductionList = new DeductionSettingsService().retrieveById(payrollDeduction.getDeductionId());
		JSONObject result = new JSONObject();
		if(deductionList != null && deductionList.size() > 0 ){
			result.put(CommonConstants.AMOUNT, String.valueOf(amount));
			result.put(CommonConstants.DEDUCTIONID, String.valueOf(deductionList.get(0).getId()));
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getName()));
			
		}else{
			result.put(CommonConstants.DEDUCTIONID, String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(""));
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}
		return result;
	}
	

	public static JSONObject getPayrollDetailObject(EmployeePayrollDO empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		
		List<EmployeeLopDO> employeeLopList = new EmployeeLopService().retrieveByEmpId(empCompensation.getEmpId());
		int noOfDaysLop = 0;
		if(employeeLopList != null && employeeLopList.size() > 0){
			Calendar start = Calendar.getInstance();
			start.setTime(employeeLopList.get(0).getStartdate());
			Calendar end = Calendar.getInstance();
			end.setTime(employeeLopList.get(0).getEnddate());
			
			for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
				Calendar weekend = Calendar.getInstance();
				weekend.setTime(date);
				if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
					weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
					if(empCompensation.getPayrollMonth().equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
						noOfDaysLop++;
					}
				}
			}
		}
		List<EmployeeCtcDO> ctcList = new EmployeeCtcService().retrieveActive(empCompensation.getEmpId());
		Double lossOfPayAmount = 0D;
		if(ctcList != null && ctcList.size() > 0){
			result.put(CommonConstants.EMPCTC, String.valueOf(ctcList.get(0).getEmpctc()));
			Long ctc = ctcList.get(0).getEmpctc()/12;
			Long perDay = ctc/30;
			if(noOfDaysLop > 0){
				lossOfPayAmount = (double) (noOfDaysLop * perDay);
			}
			result.put(CommonConstants.BACSICPAYLABEL, String.valueOf(ctc/2));
		}else{
			result.put(CommonConstants.EMPCTC, "");
			result.put(CommonConstants.BACSICPAYLABEL, "");
		}
		
		result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(lossOfPayAmount));
		
		result.put(CommonConstants.PAYROLLMONTH,String.valueOf(empCompensation.getPayrollMonth() != null ? empCompensation.getPayrollMonth() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		result.put(CommonConstants.NETMONTHLY, String.valueOf(empCompensation.getNetmonth()));
		result.put(CommonConstants.EARNINGMONTHLY, String.valueOf(empCompensation.getEarningmonth() + lossOfPayAmount));
		result.put(CommonConstants.DEDUCTIONMONTHLY, String.valueOf(empCompensation.getDeductionmonth() != null ? empCompensation.getDeductionmonth() : 0));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(empCompensation.getGrossPay()));
		result.put(CommonConstants.NAME, String.valueOf(empCompensation.getEmpName()));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		result.put(CommonConstants.TAXMONTHLY, String.valueOf(empCompensation.getTaxMonth() != null ? empCompensation.getTaxMonth() : "0"));
		List<EmployeeBonusDO> employeeBonusList = new EmployeeBonusService().retrieveByEmpIdAndMonth(empCompensation.getEmpId(), empCompensation.getPayrollMonth());
		if(employeeBonusList != null && employeeBonusList.size() > 0){
			result.put(CommonConstants.BONUSMONTH,String.valueOf(employeeBonusList.get(0).getAmount()));
		}else{
			result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
		}
		Long bonusYear = 0L;
		String fromMnth = "";
		String toMnth = "";
		String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
		String currentMonth [] = todayMnth.split(" ");
		if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
			String mnthYear [] = todayMnth.split(" ");
			int year = Integer.parseInt(mnthYear[1]) - 1;
			String mnth= "Apr";
			fromMnth = mnth + " "+year;
			toMnth = todayMnth;
		}else{
			fromMnth = todayMnth;
			String mnthYear [] = todayMnth.split(" ");
			int year = Integer.parseInt(mnthYear[1]) - 1;
			String mnth= "Apr";
			toMnth = mnth + " "+year;
		}
		
		bonusYear = new EmployeeBonusService().retrieveByEmpIdBetweenMonth(empCompensation.getEmpId(), fromMnth, toMnth);
		if(bonusYear == null){
			bonusYear = 0L;
		}
		result.put(CommonConstants.BONUSYEAR,String.valueOf(bonusYear));
		List<EmployeeLopDO> lopList = new EmployeeLopService().retrieveByEmpId(empCompensation.getEmpId());
		Double lossOfPayAmountForEmp = 0D;
		if(lopList != null && lopList.size() > 0){
			String todayMnth1 = CommonUtil.convertDateToStringWithOutDate(new Date());
			String fromMnth1 = "";
			String toMnth1 = "";
			String currentMonth1 [] = todayMnth1.split(" ");
			if(currentMonth1[0].equalsIgnoreCase("Jan") || currentMonth1[0].equalsIgnoreCase("Feb") || currentMonth1[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth1.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "04";
				fromMnth1 = "30-"+mnth+"-"+year;
				toMnth1 = todayMnth1;
			}else{
				fromMnth1 = todayMnth1;
				String mnthYear [] = todayMnth1.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "04";
				toMnth1 = "30-"+mnth+"-"+year;
			}
			for(EmployeeLopDO eLopList : lopList){
				int noOfDaysLopForEmp = 0;
				
				Date fromDate = CommonUtil.convertStringToDate(fromMnth1);
				Date toDate = CommonUtil.convertDateToStringWithOutDate(toMnth1);
				
				Date lopFromDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getStartdate()));
				Date lopToDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getEnddate()));
				
				if((lopFromDate.after(fromDate) || lopFromDate.equals(fromDate))
						&& lopFromDate.before(toDate) || lopFromDate.equals(toDate)){
					if((lopToDate.after(fromDate) || lopToDate.equals(fromDate))
							&& lopToDate.before(toDate) || lopToDate.equals(toDate)){
						
						Calendar start = Calendar.getInstance();
						start.setTime(eLopList.getStartdate());
						Calendar end = Calendar.getInstance();
						end.setTime(eLopList.getEnddate());
						
						for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
							Calendar weekend = Calendar.getInstance();
							weekend.setTime(date);
							if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
								weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
								//if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
								List<EmployeeCtcDO> empCtcList = new EmployeeCtcService().retrieveActiveWithDate(empCompensation.getEmpId(), CommonUtil.convertDateToStringWithOutDate(empCompensation.getPayrollMonth()));
								Long ctc = empCtcList.get(0).getEmpctc()/12;
						        int days = weekend.getActualMaximum(Calendar.DAY_OF_MONTH);
								Long perDay = ctc/days;
								if(noOfDaysLopForEmp > 0){
									lossOfPayAmountForEmp += (double) (noOfDaysLop * perDay);
								}
								//}
							}
						}
					}
				}
				result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(lossOfPayAmountForEmp));
			}
		}else{
			result.put(CommonConstants.LOSSOFPAYYEAR, String.valueOf(""));
		}
		Object[] earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDateSum(empCompensation.getEmpId(), fromMnth, toMnth);
		if(earningYear != null){
			double eraningYear = 0D;
			double deductionYear = 0D;
			double taxYear = 0D;
			
			if(earningYear[1] != null){
				eraningYear = empCompensation.getEarningmonth()+Double.parseDouble(earningYear[1].toString()) +  bonusYear;
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(empCompensation.getEarningmonth()+Double.parseDouble(earningYear[1].toString()) +  bonusYear));
			}else{
				eraningYear = empCompensation.getEarningmonth()+ bonusYear;
				result.put(CommonConstants.EARNINGYEAR,String.valueOf(empCompensation.getEarningmonth() +  bonusYear));
			}
			
			if(earningYear[2] != null){
				deductionYear = empCompensation.getDeductionmonth()+Double.parseDouble(earningYear[2].toString()) +  lossOfPayAmountForEmp;
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(empCompensation.getDeductionmonth()+Double.parseDouble(earningYear[2].toString()) +  lossOfPayAmountForEmp));
			}else{
				deductionYear = empCompensation.getDeductionmonth()+ lossOfPayAmountForEmp;
				result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(empCompensation.getDeductionmonth() + lossOfPayAmountForEmp));
			}
			
			if(earningYear[3] != null){
				taxYear = empCompensation.getTaxMonth()+Double.parseDouble(earningYear[3].toString());
				result.put(CommonConstants.TAXYEAR,String.valueOf(empCompensation.getTaxMonth()+Double.parseDouble(earningYear[3].toString())));
			}else{
				taxYear = empCompensation.getTaxMonth();
				result.put(CommonConstants.TAXYEAR,String.valueOf(empCompensation.getTaxMonth()));
			}
			result.put(CommonConstants.NETYEAR,String.valueOf(eraningYear - (deductionYear+taxYear)));
			
		}else{
			result.put(CommonConstants.EARNINGYEAR,String.valueOf(""));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(""));
			result.put(CommonConstants.TAXYEAR,String.valueOf(""));
			result.put(CommonConstants.NETYEAR,String.valueOf(""));
		}
		
		List<PayrollEarningsDetailsDO> earningsList = new PayrollEarningsDetailsService().retrieveByEmpCompensationId(empCompensation.getId());
		List<PayrollTaxDetailsDO> taxList = new PayrollTaxDetailsService().retrieveByEmpCompensationId(empCompensation.getId());
		List<PayrollDeductionDetailsDO> deductionList = new PayrollDeductionDetailsService().retrieveByEmpCompensationId(empCompensation.getId());
		
		JSONArray resultJSONArray = new JSONArray();
		
		if(earningsList != null && earningsList.size() > 0){
			for (PayrollEarningsDetailsDO empEarnings : earningsList) {
				resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings, empCompensation.getEmpId()));
			}
			result.put(CommonConstants.EARNINGLIST, resultJSONArray);
		}else{
			result.put(CommonConstants.EARNINGLIST,"");
		}
		JSONArray resultJSONArray1 = new JSONArray();
		
		if(deductionList != null && deductionList.size() > 0){
			for (PayrollDeductionDetailsDO empDeduction : deductionList) {
				resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction, empCompensation.getEmpId()));
			}
			result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
		}else{
			result.put(CommonConstants.DEDUCTIONLIST,"");
		}
		
		JSONArray resultJSONArray2 = new JSONArray();
		
		if(taxList != null && taxList.size() > 0){
			for (PayrollTaxDetailsDO empDeduction : taxList) {
				resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction, empCompensation.getEmpId()));
			}
			result.put(CommonConstants.TAXLIST, resultJSONArray2);
		}else{
			result.put(CommonConstants.TAXLIST,"");
		}
		
		if(empCompensation.getPayrollbatchId() != null){
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation.getPayrollbatchId()));
			List<PayrollBatchDO> payrollBatchList = new PayrollBatchService().retrieveById(empCompensation.getPayrollbatchId());
			if(payrollBatchList != null && payrollBatchList.size() > 0){
				result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(payrollBatchList.get(0).getName()));
			}else{
				result.put(CommonConstants.PAYROLLBATCHNAME, "");
			}
		}else{
			result.put(CommonConstants.PAYROLLBATCHID, "");
			result.put(CommonConstants.PAYROLLBATCHNAME, "");
		}
		
		
		
		if(empCompensation.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(empCompensation.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(empCompensation.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getObjectDetailObject(Object[] empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.COUNT, String.valueOf(empCompensation[0]));
		result.put(CommonConstants.PAYROLLMONTH, String.valueOf(empCompensation[1]));
		if(empCompensation[2] != null){
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation[2]));
			List<PayrollBatchDO> payrollBatchList = new PayrollBatchService().retrieveById(Long.parseLong(empCompensation[2].toString()));
			if(payrollBatchList != null && payrollBatchList.size() > 0){
				result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(payrollBatchList.get(0).getName()));
			}else{
				result.put(CommonConstants.PAYROLLBATCHNAME, "");
			}
		}else{
			result.put(CommonConstants.PAYROLLBATCHID, "");
			result.put(CommonConstants.PAYROLLBATCHNAME, "");
		}
		result.put(CommonConstants.NETYEAR, String.valueOf(empCompensation[3]));
		result.put(CommonConstants.EARNINGYEAR, String.valueOf(empCompensation[4]));
		result.put(CommonConstants.DEDUCTIONYEAR, String.valueOf(empCompensation[5]));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(empCompensation[6]));
		/*if(empCompensation[7] != null){
			if((char)empCompensation[7] == 'd'){
				result.put(CommonConstants.STATUS,"Draft");
			}
			
			if((char)empCompensation[7] == 'p'){
				result.put(CommonConstants.STATUS,"Processed");
			}
			
			if((char)empCompensation[7] == 'v'){
				result.put(CommonConstants.STATUS,"Deleted");
			}
			
		}else{
			result.put(CommonConstants.STATUS,"");
		}*/
		
		result.put(CommonConstants.STATUS,String.valueOf(empCompensation[7]));
		
		result.put(CommonConstants.UPDATED_ON, String.valueOf(CommonUtil.convertDateToStringWithdatetime((Date) empCompensation[8])));
		if(empCompensation[9] != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(empCompensation[9].toString()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.TAXYEAR, String.valueOf(empCompensation[10].toString()));
		/*result.put(CommonConstants.PAYROLLTYPE, String.valueOf(empCompensation[9] != null ? empCompensation[9] : ""));*/
		return result;
	}
	
	
	public static JSONObject getEmployeeEarningsDetailObject(PayrollEarningsDetailsDO empCompensation, Long empId)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.EARNINGID, String.valueOf(empCompensation.getEarningsId()));
		List<AllowanceSettingDO> allowanceList = new AllowanceSettingsService().retrieveById(empCompensation.getEarningsId());
		if(allowanceList != null && allowanceList.size() > 0){
			result.put(CommonConstants.EARNINGNAME, String.valueOf(allowanceList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.EARNINGNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly() != null ? empCompensation.getMonthly():"0"));
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
		String fromMnth = "";
		String toMnth = "";
		String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
		String currentMonth [] = todayMnth.split(" ");
		if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
			String mnthYear [] = todayMnth.split(" ");
			int year = Integer.parseInt(mnthYear[1]) - 1;
			String mnth= "Apr";
			fromMnth = mnth + " "+year;
			toMnth = todayMnth;
		}else{
			fromMnth = todayMnth;
			String mnthYear [] = todayMnth.split(" ");
			int year = Integer.parseInt(mnthYear[1]) - 1;
			String mnth= "Apr";
			toMnth = mnth + " "+year;
		}
		List<EmployeePayrollDO> earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
		
		if(earningYear != null && earningYear.size() > 0){
			List<Long> ids = new ArrayList<Long>();
			for (EmployeePayrollDO employeePayrollDO : earningYear) {
				ids.add(employeePayrollDO.getId());
			}
			Double ytd = 0D;
			List<PayrollEarningsDetailsDO> payrollEarnings = new PayrollEarningsDetailsService().retrieveByEmpCompensationIds(ids);
			for (PayrollEarningsDetailsDO payrollEarningsDetailsDO : payrollEarnings) {
				if(payrollEarningsDetailsDO.getEarningsId().equals(empCompensation.getEarningsId())){
					ytd = ytd+payrollEarningsDetailsDO.getMonthly();
				}
			}
			result.put(CommonConstants.YTD, String.valueOf(ytd + empCompensation.getMonthly()));
		}else{
			result.put(CommonConstants.YTD, String.valueOf(empCompensation.getMonthly()));
			
		}
		return result;
	}
	
	public static JSONObject getEmployeeDeductionDetailObject(PayrollDeductionDetailsDO empCompensation, Long empId)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(empCompensation.getDeductionId()));
		List<DeductionSettingDO> deductionList = new DeductionSettingsService().retrieveById(empCompensation.getDeductionId());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.DEDUCTIONNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()!= null ?empCompensation.getMonthly() :"0"));
		if(empId != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
			String fromMnth = "";
			String toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
			List<EmployeePayrollDO> earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
			if(earningYear != null  && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollDeductionDetailsDO> payrollDeductionList = new PayrollDeductionDetailsService().retrieveByEmpCompensationIds(ids);
				for (PayrollDeductionDetailsDO payrollDeductionDetailsDO : payrollDeductionList) {
					if(payrollDeductionDetailsDO.getDeductionId().equals(empCompensation.getDeductionId())){
						ytd = ytd+payrollDeductionDetailsDO.getMonthly();
					}
				}
				result.put(CommonConstants.YTD, String.valueOf(ytd + empCompensation.getMonthly()));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(empCompensation.getMonthly()));
				
			}
		}else{
			result.put(CommonConstants.YTD, String.valueOf(empCompensation.getYtd()));
		}
		return result;
	}
	
	public static JSONObject getEmployeeTaxDetailObject(PayrollTaxDetailsDO empCompensation, Long empId)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.TAXID, String.valueOf(empCompensation.getTaxId()));
		List<TaxSettingsDO> deductionList = new TaxSettingsService().retrieveById(empCompensation.getTaxId());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.TAXNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.TAXNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly() != null ? empCompensation.getMonthly():"0"));
		if(empId != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
			String fromMnth = "";
			String toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
			List<EmployeePayrollDO> earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
			if(earningYear != null && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollTaxDetailsDO> paytaxList = new PayrollTaxDetailsService().retrieveByEmpCompensationIds(ids);
				
				for (PayrollTaxDetailsDO payrollTaxDetailsDO : paytaxList) {
					if(payrollTaxDetailsDO.getTaxId().equals(empCompensation.getTaxId())){
						ytd = ytd+payrollTaxDetailsDO.getMonthly();
					}
				}
				result.put(CommonConstants.YTD, String.valueOf(ytd + empCompensation.getMonthly()));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(empCompensation.getMonthly()));
				
			}
		}else{
			result.put(CommonConstants.YTD, String.valueOf(empCompensation.getYtd()));
		}
		return result;
	}
	
	public static JSONObject SingleMonthTaxReport(List<EmployeePayrollDO> payrollList, String frequency, String period)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO payroll : payrollList) {
					resultJSONArray.put(getSingleMonthTaxDetailObject(payroll));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getSingleMonthTaxDetailObject(EmployeePayrollDO empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		if(empCompensation != null){
			if(empCompensation.getEmpId() != null){
				List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
				if(empList != null && empList.size() > 0){
					if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
					else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}else{
					result.put(CommonConstants.EMPNAME, ""); 
				}
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
			List<TaxSettingsDO> taxList = new TaxSettingsService().retrieve();
			List<PayrollTaxDetailsDO> payrollTaxList = new PayrollTaxDetailsService().retrieveByEmpCompensationId(empCompensation.getId());
			JSONArray resultJSONArray = new JSONArray();
			if(payrollTaxList != null && payrollTaxList.size()  > 0){
				for (PayrollTaxDetailsDO payrollTaxDetailsDO : payrollTaxList) {
					resultJSONArray.put(getTaxDetatils(payrollTaxDetailsDO));
				}
				result.put(CommonConstants.TAXLIST, resultJSONArray);
				
			}else{
				result.put(CommonConstants.TAXLIST, resultJSONArray);
			}
			JSONArray resultJSONArrayTemp = new JSONArray();
			if(taxList != null && taxList.size() > 0){
				for (TaxSettingsDO tax : taxList) {
					JSONObject taxResult = new JSONObject();
					if(resultJSONArray.length() <= 0){
						taxResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
						taxResult.put(CommonConstants.TAXID, String.valueOf(tax.getId()));
						taxResult.put(CommonConstants.TAXNAME, String.valueOf(tax.getName()));
					}else{
						String amount="";
						int count = 0;
						for(int i= 0; i<resultJSONArray.length();i++){
							JSONObject inputJSON1 = (JSONObject) resultJSONArray.get(i);
							if(tax.getId().equals(Long.parseLong(inputJSON1.get(CommonConstants.TAXID).toString()))){
								count++;
								amount = inputJSON1.get(CommonConstants.AMOUNT).toString();
							}
						}
						if(count > 0){
							taxResult.put(CommonConstants.AMOUNT, String.valueOf(amount));
							taxResult.put(CommonConstants.TAXID, String.valueOf(tax.getId()));
							taxResult.put(CommonConstants.TAXNAME, String.valueOf(tax.getName()));
						}else{
							taxResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
							taxResult.put(CommonConstants.TAXID, String.valueOf(tax.getId()));
							taxResult.put(CommonConstants.TAXNAME, String.valueOf(tax.getName()));
						}
					}
					resultJSONArrayTemp.put(taxResult);
				}
				result.put(CommonConstants.TAXLIST, resultJSONArrayTemp);
			}else{
				result.put(CommonConstants.TAXLIST, resultJSONArrayTemp);
			}
			
		}else{
			result.put(CommonConstants.TAXLIST, "");
		}
		
		return result;
	}
	public static JSONObject getTaxDetatils(PayrollTaxDetailsDO payrollTaxDetailsDO)throws JSONException, AppException {
		List<TaxSettingsDO> taxList = new TaxSettingsService().retrieveById(payrollTaxDetailsDO.getTaxId());
		JSONObject result = new JSONObject();
		if(taxList != null && taxList.size() > 0 ){
			result.put(CommonConstants.AMOUNT, String.valueOf(payrollTaxDetailsDO.getMonthly()));
			result.put(CommonConstants.TAXID, String.valueOf(taxList.get(0).getId()));
			result.put(CommonConstants.TAXNAME, String.valueOf(taxList.get(0).getName()));
			
		}else{
			result.put(CommonConstants.TAXID, String.valueOf(""));
			result.put(CommonConstants.TAXNAME, String.valueOf(""));
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}
		return result;
	}
	
	public static JSONObject MultipleMonthTaxReport(List<EmployeePayrollDO> payrollList, String fromMonth, String toMonth, String frequency)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			resultJSON.put(CommonConstants.PERIOD, String.valueOf(fromMonth+" - "+toMonth));
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeePayrollDO payroll : payrollList) {
				 Map<Long,Long> map=new HashMap<Long,Long>();
				 if(!map.containsKey(payroll.getEmpId())){
					 List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retrieveByEmpIdBetweenDate(payroll.getEmpId(), fromMonth, toMonth);
					 resultJSONArray.put(getMultipleMonthTaxDetailObject(compensationList, fromMonth, toMonth));
				 }
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMultipleMonthTaxDetailObject(List<EmployeePayrollDO> empCompensation, String fromMonth, String toMonth)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		Map<Long,Long> tax = new HashMap<Long,Long>();
		if(empCompensation.get(0).getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(empCompensation.get(0).getEmpId() );
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}
		List<Long> payrollIds = new ArrayList<Long>();
		if(empCompensation != null && empCompensation.size() > 0){
			for (EmployeePayrollDO employeePayrollDO : empCompensation) {
				 payrollIds.add(employeePayrollDO.getId());
			}
		}
		List<TaxSettingsDO> taxList = new TaxSettingsService().retrieve();
		 List<PayrollTaxDetailsDO> payrollTaxList = new PayrollTaxDetailsService().retrieveByEmpCompensationIds(payrollIds);
		 JSONArray resultJSONArray = new JSONArray();
		 if(payrollTaxList != null && payrollTaxList.size() > 0){
			 for (PayrollTaxDetailsDO payrollTaxDetailsDO : payrollTaxList) {
				 Double amount = 0D;
				 Long deductionId = 0L;
				 if(!tax.containsKey(payrollTaxDetailsDO.getTaxId())){
					 tax.put(payrollTaxDetailsDO.getTaxId(),payrollTaxDetailsDO.getTaxId());
					 deductionId = payrollTaxDetailsDO.getTaxId();
					 for (PayrollTaxDetailsDO payrollDeductionDetails : payrollTaxList) {
						 if(payrollDeductionDetails.getTaxId().equals(deductionId)){
							 amount = amount + payrollDeductionDetails.getMonthly();
						 }
					 }
					 resultJSONArray.put(getTaxDetatilsWithAmoount(payrollTaxDetailsDO, amount));
					 result.put(CommonConstants.TAXLIST, resultJSONArray);
				 }
			}
			 JSONArray resultJSONArrayTemp = new JSONArray();
			 
				if(taxList != null && taxList.size() > 0){
					for (TaxSettingsDO taxobj : taxList) {
						JSONObject taxResult = new JSONObject();
						if(resultJSONArray.length() <= 0){
							taxResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
							taxResult.put(CommonConstants.TAXID, String.valueOf(taxobj.getId()));
							taxResult.put(CommonConstants.TAXNAME, String.valueOf(taxobj.getName()));
						}else{
							String amount="";
							int count = 0;
							for(int i= 0; i<resultJSONArray.length();i++){
								JSONObject inputJSON1 = (JSONObject) resultJSONArray.get(i);
								if(taxobj.getId().equals(Long.parseLong(inputJSON1.get(CommonConstants.TAXID).toString()))){
									count++;
									amount = inputJSON1.get(CommonConstants.AMOUNT).toString();
								}
							}
							if(count > 0){
								taxResult.put(CommonConstants.AMOUNT, String.valueOf(amount));
								taxResult.put(CommonConstants.TAXID, String.valueOf(taxobj.getId()));
								taxResult.put(CommonConstants.TAXNAME, String.valueOf(taxobj.getName()));
							}else{
								taxResult.put(CommonConstants.AMOUNT, String.valueOf("0.00"));
								taxResult.put(CommonConstants.TAXID, String.valueOf(taxobj.getId()));
								taxResult.put(CommonConstants.TAXNAME, String.valueOf(taxobj.getName()));
							}
						}
						resultJSONArrayTemp.put(taxResult);
					}
					result.put(CommonConstants.TAXLIST, resultJSONArrayTemp);
				}else{
					result.put(CommonConstants.TAXLIST, resultJSONArrayTemp);
				}
		 }else{
			 result.put(CommonConstants.TAXLIST, "");
		 }
		return result;
	}
	
	public static JSONObject getTaxDetatilsWithAmoount(PayrollTaxDetailsDO payrollTax, Double amount)throws JSONException, AppException {
		List<DeductionSettingDO> taxList = new DeductionSettingsService().retrieveById(payrollTax.getTaxId());
		JSONObject result = new JSONObject();
		if(taxList != null && taxList.size() > 0 ){
			result.put(CommonConstants.AMOUNT, String.valueOf(amount));
			result.put(CommonConstants.TAXID, String.valueOf(taxList.get(0).getId()));
			result.put(CommonConstants.TAXNAME, String.valueOf(taxList.get(0).getName()));
			
		}else{
			result.put(CommonConstants.TAXID, String.valueOf(""));
			result.put(CommonConstants.TAXNAME, String.valueOf(""));
			result.put(CommonConstants.AMOUNT, String.valueOf(""));
		}
		return result;
	}
}
