package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class LeadEmailUtil {
	
	private LeadEmailUtil() {}
	
	public static JSONObject getLeadEmailList(List<LeadEmailDO> leadEmailEmailList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeadEmailDO leadEmail : leadEmailEmailList) {
				resultJSONArray.put(getLeadEmailDetailObject(leadEmail));
			}
			
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLeadEmailDetailObject(LeadEmailDO leadEmail)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(leadEmail.getId()));
		
		if(leadEmail.getContactId() !=null){
			result.put(CommonConstants.CONTACT_ID, String.valueOf(leadEmail.getContactId()));
		}else{
			result.put(CommonConstants.CONTACT_ID, "");
		}
		
		if(leadEmail.getFromAddress() !=null){
			result.put(CommonConstants.ADDRESS, String.valueOf(leadEmail.getFromAddress()));
		}else{
			result.put(CommonConstants.ADDRESS, "");
		}
		
		if(leadEmail.getFirstname() !=null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(leadEmail.getFirstname()));
		}else{
			result.put(CommonConstants.FIRSTNAME, "");
		}
		
		if(leadEmail.getLastname() !=null){
			result.put(CommonConstants.LASTNAME, String.valueOf(leadEmail.getLastname()));
		}else{
			result.put(CommonConstants.LASTNAME, "");
		}
		
		if(leadEmail.getMiddlename() !=null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(leadEmail.getMiddlename()));
		}else{
			result.put(CommonConstants.MIDDLENAME, "");
		}
		
		if(leadEmail.getToAddress() != null){
			result.put(CommonConstants.TO, leadEmail.getToAddress());
		}else{
			result.put(CommonConstants.TO, "");
		}
		
		if(leadEmail.getSubject() !=null){
			result.put(CommonConstants.SUBJECT, String.valueOf(leadEmail.getSubject()));
		}else{
			result.put(CommonConstants.SUBJECT, "");
		}
		
		if(leadEmail.getEmailid() !=null){
			result.put(CommonConstants.EMAIL, String.valueOf(leadEmail.getEmailid()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		
		if(leadEmail.getName() !=null){
			result.put(CommonConstants.NAME, String.valueOf(leadEmail.getName()));
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(leadEmail.getMessageContent() !=null){
			result.put(CommonConstants.CONTENT, String.valueOf(leadEmail.getMessageContent()));
		}else{
			result.put(CommonConstants.CONTENT, "");
		}
		if(leadEmail.getAttachFiles() !=null){
			result.put(CommonConstants.ATTACH_FILE, String.valueOf(leadEmail.getAttachFiles()));
		}else{
			result.put(CommonConstants.ATTACH_FILE, "");
		}
		
		if(leadEmail.getAttachmentFile() != null){
			
			result.put(CommonConstants.FILE, String.valueOf(leadEmail.getAttachmentFile()));
		}else{
			result.put(CommonConstants.FILE, "");
		}
		
		if(leadEmail.getuId() != null){
			result.put(CommonConstants.UID, String.valueOf(leadEmail.getuId()));
		}else{
			result.put(CommonConstants.UID, String.valueOf(leadEmail.getPop3UId()));
		}
		
		if(leadEmail.getContentType() != null){
			result.put(CommonConstants.CONTENT_TYPE, String.valueOf(leadEmail.getContentType()));
		}else{
			result.put(CommonConstants.CONTENT_TYPE, "");
		}
		
		if(leadEmail.getAttachment() != null){
			result.put(CommonConstants.ATTACHMENT, leadEmail.getAttachment());
		}else{
			result.put(CommonConstants.ATTACHMENT,"");
		}
		if(leadEmail.getSentDate() != null){
			String date = CommonUtil.convertDateToStringWithdatetime(leadEmail.getSentDate());
			String time = CommonUtil.convertDateToStringWithtime(leadEmail.getSentDate());
			result.put(CommonConstants.SENTDATE, leadEmail.getSentDate());
			result.put(CommonConstants.DATE, date);
			result.put(CommonConstants.TIME, time);
		}else{
			result.put(CommonConstants.SENTDATE, "");
			result.put(CommonConstants.DATE, "");
			result.put(CommonConstants.TIME, "");
		}
		
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(leadEmail.getUpdatedon())));
		result.put(CommonConstants.EMAIL_SEEN, String.valueOf(leadEmail.isSeenMsg()));
		if(leadEmail.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(leadEmail.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}		return result;
	}
}
