package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeContactInfoDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeContactInfoUtil {
	
	private EmployeeContactInfoUtil() {}
	
	public static JSONObject getEmployeeContactInfoList(List<EmployeeContactInfoDO> employeeContactInfoList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeContactInfoDO employeeContactInfoDO : employeeContactInfoList) {
				resultJSONArray.put(getEmployeeContactInfoDetailObject(employeeContactInfoDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeContactInfoDetailObject(EmployeeContactInfoDO employeeContactInfoDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeContactInfoDO.getId()));
		result.put(CommonConstants.HOMEPHONE, String.valueOf(employeeContactInfoDO.getHomePhone()!=null?employeeContactInfoDO.getHomePhone():""));
		result.put(CommonConstants.CELLPHONE, String.valueOf(employeeContactInfoDO.getCellPhone()));
		result.put(CommonConstants.WORKPHONE, String.valueOf(employeeContactInfoDO.getWorkPhone()));
		result.put(CommonConstants.PERSONALEMAIL, String.valueOf(employeeContactInfoDO.getPersonalEmail()));
		result.put(CommonConstants.COMPANYEMAIL, String.valueOf(employeeContactInfoDO.getCompanyEmail()));
		result.put(CommonConstants.OTHEREMAIL, String.valueOf(employeeContactInfoDO.getOtherEmail()!=null?employeeContactInfoDO.getOtherEmail():""));
		return result;
	}
}
