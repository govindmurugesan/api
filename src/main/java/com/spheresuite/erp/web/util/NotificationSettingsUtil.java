package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class NotificationSettingsUtil {
	
	private NotificationSettingsUtil() {}
	
	public static JSONObject getNotificationSettingsList(List<NotificationSettingsDO> notificationSettingsList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (NotificationSettingsDO notificationSettingsDO : notificationSettingsList) {
				resultJSONArray.put(getNotificationSettingsDetailObject(notificationSettingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	

	public static JSONObject getNotificationSettingsDetailObject(NotificationSettingsDO notificationSettingsDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(notificationSettingsDO.getId() != null ? notificationSettingsDO.getId() : ""));
		result.put(CommonConstants.HRREQUEST, String.valueOf(notificationSettingsDO.getHrRequest() != null ? notificationSettingsDO.getHrRequest() : ""));
		result.put(CommonConstants.PAYROLL, String.valueOf(notificationSettingsDO.getPayroll() != null ? notificationSettingsDO.getPayroll() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(notificationSettingsDO.getUpdatedon())));
		if(notificationSettingsDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(notificationSettingsDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
