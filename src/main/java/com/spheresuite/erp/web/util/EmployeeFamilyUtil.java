package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeFamilyDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeFamilyUtil {
	
	private EmployeeFamilyUtil() {}
	
	public static JSONObject getEmployeeFamilyList(List<EmployeeFamilyDO> employeeFamilyDOList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeFamilyDO employeeSkillsDO : employeeFamilyDOList) {
				resultJSONArray.put(getEmployeeFamilyDetailObject(employeeSkillsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeFamilyDetailObject(EmployeeFamilyDO employeeSkillsDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeSkillsDO.getId()));
		result.put(CommonConstants.FATHERNAME, String.valueOf(employeeSkillsDO.getFathername()));
		result.put(CommonConstants.MOTHERNAME, String.valueOf(employeeSkillsDO.getMothername()));
		return result;
	}
}
