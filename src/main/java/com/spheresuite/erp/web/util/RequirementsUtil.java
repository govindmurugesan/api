package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.RequirementsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.util.CommonConstants;

public class RequirementsUtil {
	
	private RequirementsUtil() {}
	
	public static JSONObject getRequirementsList(List<RequirementsDO> requirementsList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (RequirementsDO requirementsDO : requirementsList) {
				resultJSONArray.put(getRequirementsDetailObject(requirementsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getRequirementsDetailObject(RequirementsDO requirementsDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(requirementsDO.getId() != null ? requirementsDO.getId() : ""));
		result.put(CommonConstants.PROJECTID, String.valueOf(requirementsDO.getProjectid() != null ? requirementsDO.getProjectid() : ""));
		result.put(CommonConstants.NAME, String.valueOf(requirementsDO.getName() != null ? requirementsDO.getName() : ""));
		result.put(CommonConstants.CUSTOMERID, String.valueOf(requirementsDO.getCustomerid() != null ? requirementsDO.getCustomerid() : ""));
		result.put(CommonConstants.MINEXP, String.valueOf(requirementsDO.getMinexp() != null ? requirementsDO.getMinexp() : ""));
		result.put(CommonConstants.MAXEXP, String.valueOf(requirementsDO.getMaxexp() != null ? requirementsDO.getMaxexp() : ""));
		result.put(CommonConstants.COST, String.valueOf(requirementsDO.getCost() != null ? requirementsDO.getCost() : ""));
		result.put(CommonConstants.JOBDESC, String.valueOf(requirementsDO.getJobdesc() != null ? requirementsDO.getJobdesc() : ""));
		result.put(CommonConstants.COMMENT, String.valueOf(requirementsDO.getComments() != null ? requirementsDO.getComments() : ""));
		result.put(CommonConstants.NOTICE_PEROID, String.valueOf(requirementsDO.getNoticeperiod() != null ? requirementsDO.getNoticeperiod() : ""));
		result.put(CommonConstants.NOOFPOSITION, String.valueOf(requirementsDO.getNoofposition() != null ? requirementsDO.getNoofposition() : ""));
		result.put(CommonConstants.JOININGDATE, String.valueOf(requirementsDO.getJoiningdate() != null ? requirementsDO.getJoiningdate() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(requirementsDO.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(requirementsDO.getStatus()));
		if(requirementsDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(requirementsDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(requirementsDO.getProjectid() != null){
			List<OpportunitiesDO> opportunitiesList = new OpportunitiesService().retrieveById(requirementsDO.getProjectid());
			if(opportunitiesList != null && opportunitiesList.size() > 0){
				result.put(CommonConstants.PROJECTNAME, opportunitiesList.get(0).getProjectname());
			}else{
				result.put(CommonConstants.PROJECTNAME, "");
			}
		}else{
			result.put(CommonConstants.PROJECTNAME, "");
		}
		
		if(requirementsDO.getCustomerid() != null){
			List<LeadDO> leadList = new LeadService().retrieveById(requirementsDO.getCustomerid());
			if(leadList != null && leadList.size() > 0){
				result.put(CommonConstants.CUSTOMERNAME, leadList.get(0).getName());	
			}else{
				result.put(CommonConstants.CUSTOMERNAME, "");	
			}
		}else{
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		
		return result;
	}
}
