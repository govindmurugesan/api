package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.CompanyDO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;

public class CompanyUtil {
	
	private CompanyUtil() {}
	
	public static JSONObject getCompanyList(List<CompanyDO> companyList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CompanyDO company : companyList) {
				resultJSONArray.put(getCategoryDetailObject(company));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCategoryDetailObject(CompanyDO company)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(company.getId() != null ? company.getId() : ""));
		result.put(CommonConstants.NAME, String.valueOf(company.getName() != null ? company.getName() : ""));
		result.put(CommonConstants.TANNO, String.valueOf(company.getTanno() != null ? company.getTanno() : "" ));
		result.put(CommonConstants.PANNO, String.valueOf(company.getPanno() != null ? company.getPanno() : "" ));
		result.put(CommonConstants.ADDRESS1, String.valueOf(company.getAddress1() != null ? company.getAddress1() : "" ));
		result.put(CommonConstants.ADDRESS2, String.valueOf(company.getAddress2() != null ? company.getAddress2() : "" ));
		result.put(CommonConstants.CITY, String.valueOf(company.getCity() != null ? company.getCity() : "" ));
		result.put(CommonConstants.COUNTRY, String.valueOf(company.getCountry() != null ? company.getCountry() : "" ));
		result.put(CommonConstants.STATE, String.valueOf(company.getState() != null ? company.getState() : "" ));
		result.put(CommonConstants.ZIP, String.valueOf(company.getZip() != null ? company.getZip() : "" ));
		result.put(CommonConstants.MOBILE, String.valueOf(company.getMobile() != null ? company.getMobile() : "" ));
		result.put(CommonConstants.INDUSTRY, String.valueOf(company.getIndustry() != null ? company.getIndustry() : "" ));
		result.put(CommonConstants.WEBSITE, String.valueOf(company.getWebsite() != null ? company.getWebsite() : "" ));
		result.put(CommonConstants.CALLING_CODE, String.valueOf(company.getCallingCode() != null ? company.getCallingCode() : "" ));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(company.getUpdatedon())));
		if(company.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(company.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		CountryDAO obj = new CountryDAO();
		if(company.getCountry() != null){
			CountryDO countrydo = obj.retrieveByID(company.getCountry());
			if(countrydo != null){
				result.put(CommonConstants.COUNTRY_NAME, String.valueOf(countrydo.getName()));
			}
		}else{
			result.put(CommonConstants.COUNTRY_NAME, "");
		}
		
		StateService stateObj = new StateService();
		if(company.getState() != null){
			List<StateDO> stateDO = stateObj.retrieveById(company.getState());
			if(stateDO != null && stateDO.size() > 0){
				result.put(CommonConstants.STATE_NAME, String.valueOf(stateDO.get(0).getName()));
			}
		}else{
			result.put(CommonConstants.STATE_NAME, "");
		}
		
		if(company.getPhoto() != null){
			result.put(CommonConstants.PHOTO,company.getPhoto());
		}else{
			result.put(CommonConstants.PHOTO,"");
		}
		
		if(company.getCompanyIcon() != null){
			result.put(CommonConstants.ICON,company.getCompanyIcon());
		}else{
			result.put(CommonConstants.ICON,"");
		}
		
		return result;
	}
}
