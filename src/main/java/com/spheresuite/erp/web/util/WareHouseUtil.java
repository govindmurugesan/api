package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.PropertyTypeDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.domainobject.StorageTypeDO;
import com.spheresuite.erp.domainobject.WareHouseDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.PropertyTypeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.service.StorageTypeService;
import com.spheresuite.erp.util.CommonConstants;

public class WareHouseUtil {
	
	private WareHouseUtil() {}
	
	public static JSONObject getWareHouseList(List<WareHouseDO> wareHouseList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (WareHouseDO wareHouseDO : wareHouseList) {
				resultJSONArray.put(getWareHouseDetailObject(wareHouseDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getWareHouseDetailObject(WareHouseDO wareHouseDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(wareHouseDO.getId() != null ? wareHouseDO.getId() : ""));
		result.put(CommonConstants.NAME, String.valueOf(wareHouseDO.getName() != null ? wareHouseDO.getName() : ""));
		result.put(CommonConstants.WAREHOUSEID, String.valueOf(wareHouseDO.getWarehouseId() != null ? wareHouseDO.getWarehouseId() : ""));
		result.put(CommonConstants.PROPERTYTYPEID, String.valueOf(wareHouseDO.getPropertyTypeId() != null ? wareHouseDO.getPropertyTypeId() : ""));
		result.put(CommonConstants.STORAGETYPEID, String.valueOf(wareHouseDO.getStorageTypeId() != null ? wareHouseDO.getStorageTypeId() : ""));
		result.put(CommonConstants.COUNTRY, String.valueOf(wareHouseDO.getCountry() != null ? wareHouseDO.getCountry() : ""));
		result.put(CommonConstants.STATE, String.valueOf(wareHouseDO.getState() != null ? wareHouseDO.getState() : ""));
		result.put(CommonConstants.CITY, String.valueOf(wareHouseDO.getCity() != null ? wareHouseDO.getCity() : ""));
		result.put(CommonConstants.ZIP, String.valueOf(wareHouseDO.getPinCode() != null ? wareHouseDO.getPinCode() : ""));
		result.put(CommonConstants.ADDRESS, String.valueOf(wareHouseDO.getAddress() != null ? wareHouseDO.getAddress() : ""));
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(wareHouseDO.getUpdatedon())));
		if(wareHouseDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(wareHouseDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}

		if(wareHouseDO.getCountry() != null){
			CountryDAO obj = new CountryDAO();
			CountryDO countrydo = obj.retrieveByID(wareHouseDO.getCountry());
			if(countrydo != null){
				result.put(CommonConstants.COUNTRY_NAME, String.valueOf(countrydo.getName()));
			}else{
				result.put(CommonConstants.COUNTRY_NAME, "");
			}
		}else{
			result.put(CommonConstants.COUNTRY_NAME, "");
		}
		
		if(wareHouseDO.getState() != null){
			StateService stateObj = new StateService();
			List<StateDO> stateDO = stateObj.retrieveById(wareHouseDO.getState());
			if(stateDO != null && stateDO.size() > 0){
				result.put(CommonConstants.STATE_NAME, String.valueOf(stateDO.get(0).getName()));
			}else{
				result.put(CommonConstants.STATE_NAME, "");
			}
		}else{
			result.put(CommonConstants.STATE_NAME, "");
		}
		
		if(wareHouseDO.getPropertyTypeId() != null){
			List<PropertyTypeDO> allowanceType = new PropertyTypeService().retrieveById(wareHouseDO.getPropertyTypeId());
			if(allowanceType !=null && allowanceType.size() > 0){
				result.put(CommonConstants.PROPERTYTYPE_NAME, String.valueOf(allowanceType.get(0).getName()));
			}else{
				result.put(CommonConstants.PROPERTYTYPE_NAME,"");
			}
		}else{
			result.put(CommonConstants.PROPERTYTYPE_NAME,"");
		}
		
		if(wareHouseDO.getStorageTypeId() != null){
			List<StorageTypeDO> storageType = new StorageTypeService().retrieveById(wareHouseDO.getStorageTypeId());
			if(storageType !=null && storageType.size() > 0){
				result.put(CommonConstants.STORAGETYPE_NAME, String.valueOf(storageType.get(0).getName()));
			}else{
				result.put(CommonConstants.STORAGETYPE_NAME,"");
			}
		}else{
			result.put(CommonConstants.STORAGETYPE_NAME,"");
		}
		return result;
	}
}
