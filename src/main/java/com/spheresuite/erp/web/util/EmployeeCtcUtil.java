package com.spheresuite.erp.web.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeDeductionDO;
import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.domainobject.EmployeeTaxDO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.service.DeductionSettingsService;
import com.spheresuite.erp.service.EmployeeBonusService;
import com.spheresuite.erp.service.EmployeeCompensationService;
import com.spheresuite.erp.service.EmployeeDeductionService;
import com.spheresuite.erp.service.EmployeeEarningsService;
import com.spheresuite.erp.service.EmployeeLopService;
import com.spheresuite.erp.service.EmployeePayrollService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTaxService;
import com.spheresuite.erp.service.PayrollBatchService;
import com.spheresuite.erp.service.PayrollDeductionDetailsService;
import com.spheresuite.erp.service.PayrollEarningsDetailsService;
import com.spheresuite.erp.service.PayrollTaxDetailsService;
import com.spheresuite.erp.service.TaxSettingsService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeCtcUtil {
	
	private EmployeeCtcUtil() {}
	
	public static JSONObject getempCompensationList(List<EmployeeCtcDO> empCompensationList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCompensation : empCompensationList) {
				resultJSONArray.put(getCompensationDetailObject(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithEarnings(List<EmployeeCtcDO> empCompensationList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCompensation : empCompensationList) {
				resultJSONArray.put(getCompensationDetailObjectWithEarnings(empCompensation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithEarningsDate(List<EmployeeCtcDO> empCompensationList, String month)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCompensation : empCompensationList) {
				resultJSONArray.put(getCompensationDetailObjectWithEarningsDate(empCompensation, month));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getempCompensationListWithEarningsDateYtd(List<EmployeeCtcDO> empCompensationList, String month)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeCtcDO empCompensation : empCompensationList) {
				resultJSONArray.put(getCompensationDetailObjectWithEarningsDateYtd(empCompensation, month));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCompensationDetailObject(EmployeeCtcDO empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		if(empCompensation.getEmpctc() != null){
			result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
		}else{
			result.put(CommonConstants.EMPCTC, "");
		}
		if(empCompensation.getPayrollbatchId() != null){
			result.put(CommonConstants.PAYROLLBATCHID, String.valueOf(empCompensation.getPayrollbatchId()));
			List<PayrollBatchDO> payrollBatchList = new PayrollBatchService().retrieveById(empCompensation.getPayrollbatchId());
			if(payrollBatchList != null && payrollBatchList.size() > 0){
				result.put(CommonConstants.PAYROLLBATCHNAME, String.valueOf(payrollBatchList.get(0).getName()));
			}else{
				result.put(CommonConstants.PAYROLLBATCHNAME, "");
			}
		}else{
			result.put(CommonConstants.PAYROLLBATCHID, "");
			result.put(CommonConstants.PAYROLLBATCHNAME, "");
		}
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		if(empCompensation.getEmpId() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(empCompensation.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(empCompensation.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(empCompensation.getUpdatedon())));
		return result;
	}
	
	public static JSONObject getCompensationDetailObjectWithEarnings(EmployeeCtcDO empCompensation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		if(empCompensation.getEmpctc() != null){
			result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
		}else{
			result.put(CommonConstants.EMPCTC, "");
		}
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		if(empCompensation.getEmpId() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(empCompensation.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(empCompensation.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(empCompensation.getUpdatedon())));
		
		List<EmployeeCompensationDO> compensationList = new EmployeeCompensationService().retrieveByEmpId(empCompensation.getEmpId());
		if(compensationList != null && compensationList.size() >0){
			result.put(CommonConstants.EFFECTIVEFROM,String.valueOf(compensationList.get(0).getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(compensationList.get(0).getEffectivefrom()) : ""));
			result.put(CommonConstants.EFFECTIVETO,String.valueOf(compensationList.get(0).getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(compensationList.get(0).getEffectiveto()) : ""));
			result.put(CommonConstants.NETMONTHLY,String.valueOf(compensationList.get(0).getNetmonth()));
			result.put(CommonConstants.NETYEAR,String.valueOf(compensationList.get(0).getNetyear()));
			result.put(CommonConstants.EARNINGMONTHLY,String.valueOf(compensationList.get(0).getEarningmonth()));
			result.put(CommonConstants.EARNINGYEAR,String.valueOf(compensationList.get(0).getEarningyear()));
			result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf(compensationList.get(0).getDeductionmonth() != null ? compensationList.get(0).getDeductionmonth():"0"));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(compensationList.get(0).getDeductionyear() != null ? compensationList.get(0).getDeductionyear():"0"));
			result.put(CommonConstants.TAXMONTHLY,String.valueOf(compensationList.get(0).getTaxmonth() != null ? compensationList.get(0).getTaxmonth():"0"));
			result.put(CommonConstants.TAXYEAR,String.valueOf(compensationList.get(0).getTaxyear() != null ? compensationList.get(0).getTaxyear():"0"));
			List<EmployeeEarningsDO> earningsList = new EmployeeEarningsService().retrieveByEmpCompensationId(compensationList.get(0).getId());
			List<EmployeeTaxDO> taxList = new EmployeeTaxService().retrieveByEmpCompensationId(compensationList.get(0).getId());
			List<EmployeeDeductionDO> deductionList = new EmployeeDeductionService().retrieveByEmpCompensationId(compensationList.get(0).getId());
			JSONArray resultJSONArray = new JSONArray();
			
			if(earningsList != null && earningsList.size() > 0){
				for (EmployeeEarningsDO empEarnings : earningsList) {
					resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings, null));
				}
				result.put(CommonConstants.EARNINGLIST, resultJSONArray);
			}else{
				result.put(CommonConstants.EARNINGLIST,"");
			}
			JSONArray resultJSONArray1 = new JSONArray();
			
			if(deductionList != null && deductionList.size() > 0){
				for (EmployeeDeductionDO empDeduction : deductionList) {
					resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction, null));
				}
				result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
			}else{
				result.put(CommonConstants.DEDUCTIONLIST,"");
			}
			
			JSONArray resultJSONArray2 = new JSONArray();
			
			if(taxList != null && taxList.size() > 0){
				for (EmployeeTaxDO empDeduction : taxList) {
					resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction,  null));
				}
				result.put(CommonConstants.TAXLIST, resultJSONArray2);
			}else{
				result.put(CommonConstants.TAXLIST,"");
			}
		}else{
			result.put(CommonConstants.EFFECTIVEFROM,"");
			result.put(CommonConstants.EARNINGLIST,"");
			result.put(CommonConstants.TAXLIST,"");
			result.put(CommonConstants.DEDUCTIONLIST,"");
			result.put(CommonConstants.EFFECTIVETO,"");
			result.put(CommonConstants.NETMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.NETYEAR,String.valueOf("0"));
			result.put(CommonConstants.EARNINGMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.EARNINGYEAR,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf("0"));
			result.put(CommonConstants.TAXMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.TAXYEAR,String.valueOf("0"));
		}
		
		return result;
	}
	
	public static JSONObject getCompensationDetailObjectWithEarningsDate(EmployeeCtcDO empCompensation, String month)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		List<EmployeeLopDO> employeeLopList = new EmployeeLopService().retrieveByEmpId(empCompensation.getEmpId());
		int noOfDaysLop = 0;
		Double lossOfPayAmount = 0D;
		if(month != null){
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		if(month != null){
			if(empCompensation.getEmpctc() != null){
				result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
				Long ctc = empCompensation.getEmpctc()/12;
				Date s = CommonUtil.convertDateToStringWithOutDate(month);
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = ctc/days;
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
			}else{
				result.put(CommonConstants.EMPCTC, "");
			}
		}
		
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		if(empCompensation.getEmpId() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(empCompensation.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(empCompensation.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(empCompensation.getUpdatedon())));
		Long bonusMonth = 0L;
		Long bonusYear = 0L;
		List<EmployeeCompensationDO> compensationList = new EmployeeCompensationService().retrieveByEmpId(empCompensation.getEmpId());
		if(compensationList != null && compensationList.size() >0){
			if(month != null){
				List<EmployeeBonusDO> employeeBonusList = new EmployeeBonusService().retrieveByEmpIdAndMonth(empCompensation.getEmpId(), month);
				if(employeeBonusList != null && employeeBonusList.size() > 0){
					result.put(CommonConstants.BONUSMONTH,String.valueOf(employeeBonusList.get(0).getAmount()));
					bonusMonth = employeeBonusList.get(0).getAmount();
					String fromMnth = "";
					String toMnth = "";
					String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
					String currentMonth [] = todayMnth.split(" ");
					if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
						String mnthYear [] = todayMnth.split(" ");
						int year = Integer.parseInt(mnthYear[1]) - 1;
						String mnth= "Apr";
						fromMnth = mnth + " "+year;
						toMnth = todayMnth;
					}else{
						fromMnth = todayMnth;
						String mnthYear [] = todayMnth.split(" ");
						int year = Integer.parseInt(mnthYear[1]) - 1;
						String mnth= "Apr";
						toMnth = mnth + " "+year;
					}
					
					bonusYear = new EmployeeBonusService().retrieveByEmpIdBetweenMonth(empCompensation.getEmpId(), fromMnth, toMnth);
					result.put(CommonConstants.BONUSYEAR,String.valueOf(bonusYear));
				}else{
					result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
					result.put(CommonConstants.BONUSYEAR,String.valueOf(""));
				}
			}else{
				result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
				result.put(CommonConstants.BONUSYEAR,String.valueOf(""));
			}
			
			for (EmployeeCompensationDO compensation : compensationList) {
				if(CommonUtil.convertDateToStringWithOutDate(month).after(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectivefrom())))
						|| CommonUtil.convertDateToStringWithOutDate(month).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectivefrom())))){
					if(compensation.getEffectiveto() == null || CommonUtil.convertDateToStringWithOutDate(month).before(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectiveto())))
							|| CommonUtil.convertDateToStringWithOutDate(month).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectiveto())))){
						result.put(CommonConstants.EFFECTIVEFROM,String.valueOf(compensation.getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(compensation.getEffectivefrom()) : ""));
						result.put(CommonConstants.EFFECTIVETO,String.valueOf(compensation.getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(compensation.getEffectiveto()) : ""));
						result.put(CommonConstants.NETMONTHLY,String.valueOf((compensation.getNetmonth()+ bonusMonth)  - lossOfPayAmount));
						result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(lossOfPayAmount));
						result.put(CommonConstants.EARNINGMONTHLY,String.valueOf(compensation.getEarningmonth() + bonusMonth));
						result.put(CommonConstants.EARNINGYEAR,String.valueOf(compensation.getEarningyear() +  bonusYear));
						result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf(compensation.getDeductionmonth() + lossOfPayAmount));
						result.put(CommonConstants.TAXMONTHLY,String.valueOf(compensation.getTaxmonth()));
						result.put(CommonConstants.TAXYEAR,String.valueOf(compensation.getTaxyear()));
						List<EmployeeLopDO> lopList = new EmployeeLopService().retrieveByEmpId(empCompensation.getEmpId());
						Double lossOfPayAmountForEmp = 0D;
						if(lopList != null && lopList.size() > 0){
							String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
							String fromMnth = "";
							String toMnth = "";
							String currentMonth [] = todayMnth.split(" ");
							if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
								String mnthYear [] = todayMnth.split(" ");
								int year = Integer.parseInt(mnthYear[1]) - 1;
								String mnth= "04";
								fromMnth = "30-"+mnth+"-"+year;
								toMnth = todayMnth;
							}else{
								fromMnth = todayMnth;
								String mnthYear [] = todayMnth.split(" ");
								int year = Integer.parseInt(mnthYear[1]) - 1;
								String mnth= "04";
								toMnth = "30-"+mnth+"-"+year;
							}
							for(EmployeeLopDO eLopList : lopList){
								int noOfDaysLopForEmp = 0;
								
								Date fromDate = CommonUtil.convertStringToDate(fromMnth);
								Date toDate = CommonUtil.convertDateToStringWithOutDate(toMnth);
								
								Date lopFromDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getStartdate()));
								Date lopToDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getEnddate()));
								
								if((lopFromDate.after(fromDate) || lopFromDate.equals(fromDate))
										&& lopFromDate.before(toDate) || lopFromDate.equals(toDate)){
									if((lopToDate.after(fromDate) || lopToDate.equals(fromDate))
											&& lopToDate.before(toDate) || lopToDate.equals(toDate)){
										
										Calendar start = Calendar.getInstance();
										start.setTime(eLopList.getStartdate());
										Calendar end = Calendar.getInstance();
										end.setTime(eLopList.getEnddate());
										
										for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
											Calendar weekend = Calendar.getInstance();
											weekend.setTime(date);
											if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
												weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
												if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
													noOfDaysLopForEmp++;
												}
											}
										}
										
										Long ctc = empCompensation.getEmpctc()/12;
										Date s = CommonUtil.convertDateToStringWithOutDate(month);
								        Calendar c=Calendar.getInstance();
								        c.setTime(s);
								        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
										Long perDay = ctc/days;
										if(noOfDaysLopForEmp > 0){
											lossOfPayAmountForEmp += (double) (noOfDaysLop * perDay);
										}
									}
								}
							}
						}else{
							result.put(CommonConstants.LOSSOFPAYYEAR, String.valueOf(""));
						}
						result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(lossOfPayAmountForEmp));
						result.put(CommonConstants.NETYEAR,String.valueOf(compensation.getNetyear() - lossOfPayAmountForEmp));
						result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(compensation.getDeductionyear() + lossOfPayAmountForEmp));
						List<EmployeeEarningsDO> earningsList = new EmployeeEarningsService().retrieveByEmpCompensationId(compensation.getId());
						List<EmployeeTaxDO> taxList = new EmployeeTaxService().retrieveByEmpCompensationId(compensation.getId());
						List<EmployeeDeductionDO> deductionList = new EmployeeDeductionService().retrieveByEmpCompensationId(compensation.getId());
						JSONArray resultJSONArray = new JSONArray();
						
						if(earningsList != null && earningsList.size() > 0){
							for (EmployeeEarningsDO empEarnings : earningsList) {
								resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings, null));
							}
							result.put(CommonConstants.EARNINGLIST, resultJSONArray);
						}else{
							result.put(CommonConstants.EARNINGLIST,"");
						}
						JSONArray resultJSONArray1 = new JSONArray();
						
						if(deductionList != null && deductionList.size() > 0){
							for (EmployeeDeductionDO empDeduction : deductionList) {
								resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction, null));
							}
							result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
						}else{
							result.put(CommonConstants.DEDUCTIONLIST,"");
						}
						
						JSONArray resultJSONArray2 = new JSONArray();
						
						if(taxList != null && taxList.size() > 0){
							for (EmployeeTaxDO empDeduction : taxList) {
								resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction, null));
							}
							result.put(CommonConstants.TAXLIST, resultJSONArray2);
						}else{
							result.put(CommonConstants.TAXLIST,"");
						}
					}
				}
			}
		}else{
			result.put(CommonConstants.EFFECTIVEFROM,"");
			result.put(CommonConstants.EARNINGLIST,"");
			result.put(CommonConstants.TAXLIST,"");
			result.put(CommonConstants.DEDUCTIONLIST,"");
			result.put(CommonConstants.EFFECTIVETO,"");
			result.put(CommonConstants.NETMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.NETYEAR,String.valueOf("0"));
			result.put(CommonConstants.EARNINGMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.EARNINGYEAR,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf("0"));
			result.put(CommonConstants.TAXMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.TAXYEAR,String.valueOf("0"));
		}
		
		return result;
	}
	
	public static JSONObject getCompensationDetailObjectWithEarningsDateYtd(EmployeeCtcDO empCompensation, String month)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(empCompensation.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(empCompensation.getEmpId()));
		List<EmployeeLopDO> employeeLopList = new EmployeeLopService().retrieveByEmpId(empCompensation.getEmpId());
		int noOfDaysLop = 0;
		Double lossOfPayAmount = 0D;
		if(month != null){
			if(employeeLopList != null && employeeLopList.size() > 0){
				Calendar start = Calendar.getInstance();
				start.setTime(employeeLopList.get(0).getStartdate());
				Calendar end = Calendar.getInstance();
				end.setTime(employeeLopList.get(0).getEnddate());
				
				for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
							noOfDaysLop++;
						}
					}
				}
			}
		}
		if(month != null){
			if(empCompensation.getEmpctc() != null){
				result.put(CommonConstants.EMPCTC, String.valueOf(empCompensation.getEmpctc()));
				Long ctc = empCompensation.getEmpctc()/12;
				Date s = CommonUtil.convertDateToStringWithOutDate(month);
		        Calendar c=Calendar.getInstance();
		        c.setTime(s);
		        int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
				Long perDay = ctc/days;
				if(noOfDaysLop > 0){
					lossOfPayAmount = (double) (noOfDaysLop * perDay);
				}
			}else{
				result.put(CommonConstants.EMPCTC, "");
			}
		}
		
		result.put(CommonConstants.STARTDATE,String.valueOf(empCompensation.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(empCompensation.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(empCompensation.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(empCompensation.getStatus()));
		if(empCompensation.getEmpId() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveByEmpId(empCompensation.getEmpId());
			if(employeeList != null && employeeList.size() > 0){
				if(employeeList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " +employeeList.get(0).getMiddlename() + " " + employeeList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(employeeList.get(0).getFirstname() + " " + employeeList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.NAME, "");
		}
		if(empCompensation.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(empCompensation.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(empCompensation.getUpdatedon())));
		Long bonusMonth = 0L;
		Long bonusYear = 0L;
		List<EmployeeCompensationDO> compensationList = new EmployeeCompensationService().retrieveByEmpId(empCompensation.getEmpId());
		String fromMnth = "";
		String toMnth = "";
		if(compensationList != null && compensationList.size() >0){
			if(month != null){
				List<EmployeeBonusDO> employeeBonusList = new EmployeeBonusService().retrieveByEmpIdAndMonth(empCompensation.getEmpId(), month);
				if(employeeBonusList != null && employeeBonusList.size() > 0){
					result.put(CommonConstants.BONUSMONTH,String.valueOf(employeeBonusList.get(0).getAmount()));
					bonusMonth = employeeBonusList.get(0).getAmount();
				}else{
					result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
				}
				String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
				String currentMonth [] = todayMnth.split(" ");
				if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
					String mnthYear [] = todayMnth.split(" ");
					int year = Integer.parseInt(mnthYear[1]) - 1;
					String mnth= "Apr";
					fromMnth = mnth + " "+year;
					toMnth = todayMnth;
				}else{
					fromMnth = todayMnth;
					String mnthYear [] = todayMnth.split(" ");
					int year = Integer.parseInt(mnthYear[1]) - 1;
					String mnth= "Apr";
					toMnth = mnth + " "+year;
				}
				
				bonusYear = new EmployeeBonusService().retrieveByEmpIdBetweenMonth(empCompensation.getEmpId(), fromMnth, toMnth);
				result.put(CommonConstants.BONUSYEAR,String.valueOf(bonusYear));
			}else{
				result.put(CommonConstants.BONUSMONTH,String.valueOf(""));
				result.put(CommonConstants.BONUSYEAR,String.valueOf(""));
			}
			
			if(bonusYear == null){
				bonusYear = 0L;
			}
			
			for (EmployeeCompensationDO compensation : compensationList) {
				if(CommonUtil.convertDateToStringWithOutDate(month).after(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectivefrom())))
						|| CommonUtil.convertDateToStringWithOutDate(month).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectivefrom())))){
					if(compensation.getEffectiveto() == null || CommonUtil.convertDateToStringWithOutDate(month).before(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectiveto())))
							|| CommonUtil.convertDateToStringWithOutDate(month).equals(CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(compensation.getEffectiveto())))){
						result.put(CommonConstants.EFFECTIVEFROM,String.valueOf(compensation.getEffectivefrom() != null ? CommonUtil.convertDateToStringWithdatetime(compensation.getEffectivefrom()) : ""));
						result.put(CommonConstants.EFFECTIVETO,String.valueOf(compensation.getEffectiveto() != null ? CommonUtil.convertDateToStringWithdatetime(compensation.getEffectiveto()) : ""));
						result.put(CommonConstants.NETMONTHLY,String.valueOf((compensation.getNetmonth()+ bonusMonth)  - lossOfPayAmount));
						result.put(CommonConstants.LOSSOFPAYMONTHLY,  String.valueOf(lossOfPayAmount));
						result.put(CommonConstants.EARNINGMONTHLY,String.valueOf(compensation.getEarningmonth() + bonusMonth));
						result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf(compensation.getDeductionmonth() + lossOfPayAmount));
						result.put(CommonConstants.TAXMONTHLY,String.valueOf(compensation.getTaxmonth()));
						
						List<EmployeeLopDO> lopList = new EmployeeLopService().retrieveByEmpId(empCompensation.getEmpId());
						Double lossOfPayAmountForEmp = 0D;
						if(lopList != null && lopList.size() > 0){
							String todayMnth1 = CommonUtil.convertDateToStringWithOutDate(new Date());
							String fromMnth1 = "";
							String toMnth1 = "";
							String currentMonth1 [] = todayMnth1.split(" ");
							if(currentMonth1[0].equalsIgnoreCase("Jan") || currentMonth1[0].equalsIgnoreCase("Feb") || currentMonth1[0].equalsIgnoreCase("Mar")){
								String mnthYear [] = todayMnth1.split(" ");
								int year = Integer.parseInt(mnthYear[1]) - 1;
								String mnth= "04";
								fromMnth1 = "30-"+mnth+"-"+year;
								toMnth1 = todayMnth1;
							}else{
								fromMnth1 = todayMnth1;
								String mnthYear [] = todayMnth1.split(" ");
								int year = Integer.parseInt(mnthYear[1]) - 1;
								String mnth= "04";
								toMnth1 = "30-"+mnth+"-"+year;
							}
							for(EmployeeLopDO eLopList : lopList){
								int noOfDaysLopForEmp = 0;
								
								Date fromDate = CommonUtil.convertStringToDate(fromMnth1);
								Date toDate = CommonUtil.convertDateToStringWithOutDate(toMnth1);
								
								Date lopFromDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getStartdate()));
								Date lopToDate = CommonUtil.convertDateToStringWithOutDate(CommonUtil.convertDateToStringWithOutDate(eLopList.getEnddate()));
								
								if((lopFromDate.after(fromDate) || lopFromDate.equals(fromDate))
										&& lopFromDate.before(toDate) || lopFromDate.equals(toDate)){
									if((lopToDate.after(fromDate) || lopToDate.equals(fromDate))
											&& lopToDate.before(toDate) || lopToDate.equals(toDate)){
										
										Calendar start = Calendar.getInstance();
										start.setTime(eLopList.getStartdate());
										Calendar end = Calendar.getInstance();
										end.setTime(eLopList.getEnddate());
										
										for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
											Calendar weekend = Calendar.getInstance();
											weekend.setTime(date);
											if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
												weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
												//if(month.equalsIgnoreCase(CommonUtil.convertDateToStringWithOutDate(weekend.getTime()))){
												Long ctc = empCompensation.getEmpctc()/12;
										        int days = weekend.getActualMaximum(Calendar.DAY_OF_MONTH);
												Long perDay = ctc/days;
												if(noOfDaysLopForEmp > 0){
													lossOfPayAmountForEmp += (double) (noOfDaysLop * perDay);
												}
												//}
											}
										}
										
										
									}
								}
								result.put(CommonConstants.LOSSOFPAYYEAR,  String.valueOf(lossOfPayAmountForEmp));
							}
						}else{
							result.put(CommonConstants.LOSSOFPAYYEAR, String.valueOf(""));
						}
						Object[] earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDateSum(empCompensation.getEmpId(), fromMnth, toMnth);
						if(earningYear != null){
							double eraningYear = 0D;
							double deductionYear = 0D;
							double taxYear = 0D;
							
							if(earningYear[1] != null){
								eraningYear = compensation.getEarningmonth()+Double.parseDouble(earningYear[1].toString()) +  bonusYear;
								result.put(CommonConstants.EARNINGYEAR,String.valueOf(compensation.getEarningmonth()+Double.parseDouble(earningYear[1].toString()) +  bonusYear));
							}else{
								eraningYear = compensation.getEarningmonth()+ bonusYear;
								result.put(CommonConstants.EARNINGYEAR,String.valueOf(compensation.getEarningmonth() +  bonusYear));
							}
							
							if(earningYear[2] != null){
								deductionYear = compensation.getDeductionmonth()+Double.parseDouble(earningYear[2].toString()) +  lossOfPayAmountForEmp;
								result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(compensation.getDeductionmonth()+Double.parseDouble(earningYear[2].toString()) +  lossOfPayAmountForEmp));
							}else{
								deductionYear = compensation.getDeductionmonth()+ lossOfPayAmountForEmp;
								result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(compensation.getDeductionmonth() + lossOfPayAmountForEmp));
							}
							
							if(earningYear[3] != null){
								taxYear = compensation.getTaxmonth()+Double.parseDouble(earningYear[3].toString());
								result.put(CommonConstants.TAXYEAR,String.valueOf(compensation.getTaxmonth()+Double.parseDouble(earningYear[3].toString())));
							}else{
								taxYear = compensation.getTaxmonth();
								result.put(CommonConstants.TAXYEAR,String.valueOf(compensation.getTaxmonth()));
							}
							result.put(CommonConstants.NETYEAR,String.valueOf(eraningYear - (deductionYear+taxYear)));
							
						}else{
							result.put(CommonConstants.EARNINGYEAR,String.valueOf(""));
							result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf(""));
							result.put(CommonConstants.TAXYEAR,String.valueOf(""));
							result.put(CommonConstants.NETYEAR,String.valueOf(""));
						}
						
						List<EmployeeEarningsDO> earningsList = new EmployeeEarningsService().retrieveByEmpCompensationId(compensation.getId());
						List<EmployeeTaxDO> taxList = new EmployeeTaxService().retrieveByEmpCompensationId(compensation.getId());
						List<EmployeeDeductionDO> deductionList = new EmployeeDeductionService().retrieveByEmpCompensationId(compensation.getId());
						JSONArray resultJSONArray = new JSONArray();
						
						if(earningsList != null && earningsList.size() > 0){
							for (EmployeeEarningsDO empEarnings : earningsList) {
								resultJSONArray.put(getEmployeeEarningsDetailObject(empEarnings, compensation.getEmpId()));
							}
							result.put(CommonConstants.EARNINGLIST, resultJSONArray);
						}else{
							result.put(CommonConstants.EARNINGLIST,"");
						}
						JSONArray resultJSONArray1 = new JSONArray();
						
						if(deductionList != null && deductionList.size() > 0){
							for (EmployeeDeductionDO empDeduction : deductionList) {
								resultJSONArray1.put(getEmployeeDeductionDetailObject(empDeduction, compensation.getEmpId()));
							}
							result.put(CommonConstants.DEDUCTIONLIST, resultJSONArray1);
						}else{
							result.put(CommonConstants.DEDUCTIONLIST,"");
						}
						
						JSONArray resultJSONArray2 = new JSONArray();
						
						if(taxList != null && taxList.size() > 0){
							for (EmployeeTaxDO empDeduction : taxList) {
								resultJSONArray2.put(getEmployeeTaxDetailObject(empDeduction, compensation.getEmpId()));
							}
							result.put(CommonConstants.TAXLIST, resultJSONArray2);
						}else{
							result.put(CommonConstants.TAXLIST,"");
						}
					}
				}
			}
		}else{
			result.put(CommonConstants.EFFECTIVEFROM,"");
			result.put(CommonConstants.EARNINGLIST,"");
			result.put(CommonConstants.TAXLIST,"");
			result.put(CommonConstants.DEDUCTIONLIST,"");
			result.put(CommonConstants.EFFECTIVETO,"");
			result.put(CommonConstants.NETMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.NETYEAR,String.valueOf("0"));
			result.put(CommonConstants.EARNINGMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.EARNINGYEAR,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.DEDUCTIONYEAR,String.valueOf("0"));
			result.put(CommonConstants.TAXMONTHLY,String.valueOf("0"));
			result.put(CommonConstants.TAXYEAR,String.valueOf("0"));
		}
		
		return result;
	}
	
	public static JSONObject getEmployeeEarningsDetailObject(EmployeeEarningsDO empCompensation, Long empId)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.EARNINGID, String.valueOf(empCompensation.getEarningsId()));
		List<AllowanceSettingDO> allowanceList = new AllowanceSettingsService().retrieveById(empCompensation.getEarningsId());
		if(allowanceList != null && allowanceList.size() > 0){
			result.put(CommonConstants.EARNINGNAME, String.valueOf(allowanceList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.EARNINGNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
		if(empId != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
			String fromMnth = "";
			String toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
			List<EmployeePayrollDO> earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
			if(earningYear != null && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollEarningsDetailsDO> payrollEarnings = new PayrollEarningsDetailsService().retrieveByEmpCompensationIds(ids);
				for (PayrollEarningsDetailsDO payrollEarningsDetailsDO : payrollEarnings) {
					if(payrollEarningsDetailsDO.getEarningsId().equals(empCompensation.getEarningsId())){
						ytd = ytd+payrollEarningsDetailsDO.getMonthly();
					}
				}
				result.put(CommonConstants.YTD, String.valueOf(ytd + empCompensation.getMonthly()));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(empCompensation.getMonthly()));
				
			}
		}else{
			result.put(CommonConstants.YTD, String.valueOf(empCompensation.getYtd()));
		}
		
		
		return result;
	}
	
	public static JSONObject getEmployeeDeductionDetailObject(EmployeeDeductionDO empCompensation, Long empId)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.DEDUCTIONID, String.valueOf(empCompensation.getEmpdeductionid()));
		List<DeductionSettingDO> deductionList = new DeductionSettingsService().retrieveById(empCompensation.getEmpdeductionid());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.DEDUCTIONNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.DEDUCTIONNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
		if(empId != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
			String fromMnth = "";
			String toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
			List<EmployeePayrollDO> earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
			if(earningYear != null  && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollDeductionDetailsDO> payrollDeductionList = new PayrollDeductionDetailsService().retrieveByEmpCompensationIds(ids);
				for (PayrollDeductionDetailsDO payrollDeductionDetailsDO : payrollDeductionList) {
					if(payrollDeductionDetailsDO.getDeductionId().equals(empCompensation.getEmpdeductionid())){
						ytd = ytd+payrollDeductionDetailsDO.getMonthly();
					}
				}
				result.put(CommonConstants.YTD, String.valueOf(ytd + empCompensation.getMonthly()));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(empCompensation.getMonthly()));
				
			}
		}else{
			result.put(CommonConstants.YTD, String.valueOf(empCompensation.getYtd()));
		}
		return result;
	}
	
	public static JSONObject getEmployeeTaxDetailObject(EmployeeTaxDO empCompensation, Long empId)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.TAXID, String.valueOf(empCompensation.getEmptaxid()));
		List<TaxSettingsDO> deductionList = new TaxSettingsService().retrieveById(empCompensation.getEmptaxid());
		if(deductionList != null && deductionList.size() > 0){
			result.put(CommonConstants.TAXNAME, String.valueOf(deductionList.get(0).getDisplayname()));
		}else{
			result.put(CommonConstants.TAXNAME, "");
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
		
		if(empId != null){
			result.put(CommonConstants.MONTHLY, String.valueOf(empCompensation.getMonthly()));
			String fromMnth = "";
			String toMnth = "";
			String todayMnth = CommonUtil.convertDateToStringWithOutDate(new Date());
			String currentMonth [] = todayMnth.split(" ");
			if(currentMonth[0].equalsIgnoreCase("Jan") || currentMonth[0].equalsIgnoreCase("Feb") || currentMonth[0].equalsIgnoreCase("Mar")){
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				fromMnth = mnth + " "+year;
				toMnth = todayMnth;
			}else{
				fromMnth = todayMnth;
				String mnthYear [] = todayMnth.split(" ");
				int year = Integer.parseInt(mnthYear[1]) - 1;
				String mnth= "Apr";
				toMnth = mnth + " "+year;
			}
			List<EmployeePayrollDO> earningYear = new EmployeePayrollService().retrieveByEmpIdBetweenDate(empId, fromMnth, toMnth);
			
			if(earningYear != null && earningYear.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				for (EmployeePayrollDO employeePayrollDO : earningYear) {
					ids.add(employeePayrollDO.getId());
				}
				Double ytd = 0D;
				List<PayrollTaxDetailsDO> paytaxList = new PayrollTaxDetailsService().retrieveByEmpCompensationIds(ids);
				
				for (PayrollTaxDetailsDO payrollTaxDetailsDO : paytaxList) {
					if(payrollTaxDetailsDO.getTaxId().equals(empCompensation.getEmptaxid())){
						ytd = ytd+payrollTaxDetailsDO.getMonthly();
					}
				}
				result.put(CommonConstants.YTD, String.valueOf(ytd + empCompensation.getMonthly()));
			}else{
				result.put(CommonConstants.YTD, String.valueOf(empCompensation.getMonthly()));
				
			}
		}else{
			result.put(CommonConstants.YTD, String.valueOf(empCompensation.getYtd()));
		}
		return result;
	}
}
