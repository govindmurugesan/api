package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.LogActivityDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.util.CommonConstants;

public class LogActivityUtil {
	
	private LogActivityUtil() {}
	
	public static JSONObject getLogList(List<LogActivityDO> logList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LogActivityDO log : logList) {
				resultJSONArray.put(getLogDetailObject(log));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLogDetailObject(LogActivityDO log)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(log.getId() != null ? log.getId() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(log.getStatus() != null ? log.getStatus() : ""));
		result.put(CommonConstants.TYPE, String.valueOf(log.getType() != null ? log.getType() : ""));
		result.put(CommonConstants.TIME, String.valueOf(log.getLogTime() != null ? log.getLogTime() : ""));
		result.put(CommonConstants.DATE, String.valueOf(log.getLogDate()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToString(log.getUpdatedon())));
		if(log.getUpdatedBy() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(log.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(log.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.LEAD_ID, String.valueOf(log.getLeadId() != null ? log.getLeadId() : ""));
		result.put(CommonConstants.BODY, String.valueOf(log.getLogDetails() != null ? log.getLogDetails() : ""));
		
		if(log.getLeadId() != null){
			List<LeadDO> leads = new LeadService().retrieveById(log.getLeadId());
			if(leads !=null && leads.size() > 0){
				result.put(CommonConstants.LEAD_NAME, String.valueOf(leads.get(0).getName()));
			}else{
				result.put(CommonConstants.LEAD_NAME,"");
			}
		}else{
			result.put(CommonConstants.LEAD_NAME,"");
		}
		return result;
	}
}
