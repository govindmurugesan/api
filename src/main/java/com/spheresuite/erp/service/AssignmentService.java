package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.AssignmentDAO;
import com.spheresuite.erp.domainobject.AssignmentDO;
import com.spheresuite.erp.exception.AppException;

public class AssignmentService {
	static Logger logger = Logger.getLogger(AssignmentService.class.getName());
	
	public AssignmentDO persist(AssignmentDO assignmentDO) throws AppException {
		return new AssignmentDAO().persist(assignmentDO);
	}

	public List<AssignmentDO> retrieveActive() throws AppException {
		return new AssignmentDAO().retrieveActive();
	}
	
	public List<AssignmentDO> retrieveById(Long Id) throws AppException {
		return new AssignmentDAO().retrieveById(Id);
	}
	
	public List<AssignmentDO> retrieveByEmp(Long Id) throws AppException {
		return new AssignmentDAO().retrieveByEmp(Id);
	}
	
	public List<AssignmentDO> retrieve() throws AppException {
		return new AssignmentDAO().retrieve();
	}
	
	public AssignmentDO update(AssignmentDO assignmentDO) throws AppException {
		return new AssignmentDAO().update(assignmentDO);
	}
}
