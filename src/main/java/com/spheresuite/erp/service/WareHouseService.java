package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.WareHouseDAO;
import com.spheresuite.erp.domainobject.WareHouseDO;
import com.spheresuite.erp.exception.AppException;

public class WareHouseService {
	static Logger logger = Logger.getLogger(WareHouseService.class.getName());
	
	public WareHouseDO persist(WareHouseDO wareHouseDO) throws AppException {
		return new WareHouseDAO().persist(wareHouseDO);
	}
	
	public WareHouseDO update(WareHouseDO wareHouseDO) throws AppException {
		return new WareHouseDAO().update(wareHouseDO);
	}

	public List<WareHouseDO> retrieveById(Long Id) throws AppException {
		return new WareHouseDAO().retrieveById(Id);
	}

	public List<WareHouseDO> retrieve() throws AppException {
		return new WareHouseDAO().retrieve();
	}
}
