package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.LeadTypeDAO;
import com.spheresuite.erp.domainobject.LeadTypeDO;
import com.spheresuite.erp.exception.AppException;

public class LeadTypeService {
	static Logger logger = Logger.getLogger(LeadTypeService.class.getName());
	
	public LeadTypeDO persist(LeadTypeDO leadTypeDO) throws AppException {
		return new LeadTypeDAO().persist(leadTypeDO);
	}

	public List<LeadTypeDO> retrieveActive() throws AppException {
		return new LeadTypeDAO().retrieveActive();
	}
	
	public List<LeadTypeDO> retrieveById(Long Id) throws AppException {
		return new LeadTypeDAO().retrieveById(Id);
	}
	
	public List<LeadTypeDO> retrieve() throws AppException {
		return new LeadTypeDAO().retrieve();
	}
	
	public LeadTypeDO update(LeadTypeDO leadTypeDO) throws AppException {
		return new LeadTypeDAO().update(leadTypeDO);
	}
}
