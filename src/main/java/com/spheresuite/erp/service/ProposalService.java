package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ProposalDAO;
import com.spheresuite.erp.domainobject.ProposalDO;
import com.spheresuite.erp.exception.AppException;

public class ProposalService {
	static Logger logger = Logger.getLogger(ProposalService.class.getName());
	
	public ProposalDO persist(ProposalDO proposalDO) throws AppException {
		return new ProposalDAO().persist(proposalDO);
	}

	public List<ProposalDO> retriveById(Long id) throws AppException {
		return new ProposalDAO().retrieveById(id);
	}
	
	public List<ProposalDO> retrieve(List<Long> id) throws AppException {
		return new ProposalDAO().retrieve(id);
	}
	
	public List<ProposalDO> retrieveCustomerId(Long id) throws AppException {
		return new ProposalDAO().retrieveCustomerId(id);
	}
	
	public List<ProposalDO> retrieveAll() throws AppException {
		return new ProposalDAO().retrieveAll();
	}
	
	public ProposalDO update(ProposalDO proposalDO) throws AppException {
		return new ProposalDAO().update(proposalDO);
	}
}
