package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.InvoiceTermsConfigurationDAO;
import com.spheresuite.erp.domainobject.InvoiceTermsConfigurationDO;
import com.spheresuite.erp.exception.AppException;

public class InvoiceTermsConfigurationService {
	static Logger logger = Logger.getLogger(LeadTypeService.class.getName());
	
	public InvoiceTermsConfigurationDO persist(InvoiceTermsConfigurationDO invoiceTermsConfigurationDO) throws AppException {
		return new InvoiceTermsConfigurationDAO().persist(invoiceTermsConfigurationDO);
	}

	public List<InvoiceTermsConfigurationDO> retrieveActive() throws AppException {
		return new InvoiceTermsConfigurationDAO().retrieveActive();
	}
	
	public List<InvoiceTermsConfigurationDO> retrieveById(Long Id) throws AppException {
		return new InvoiceTermsConfigurationDAO().retrieveById(Id);
	}
	
	public List<InvoiceTermsConfigurationDO> retrieve() throws AppException {
		return new InvoiceTermsConfigurationDAO().retrieve();
	}
	
	public InvoiceTermsConfigurationDO update(InvoiceTermsConfigurationDO invoiceTermsConfigurationDO) throws AppException {
		return new InvoiceTermsConfigurationDAO().update(invoiceTermsConfigurationDO);
	}
}
