package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmailSettingsDAO;
import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.exception.AppException;

public class EmailSettingsService {
	static Logger logger = Logger.getLogger(EmailSettingsService.class.getName());
	
	public EmailSettingsDO persist(EmailSettingsDO emailSettingsDO) throws AppException {
		return new EmailSettingsDAO().persist(emailSettingsDO);
	}

	public List<EmailSettingsDO> retrieveById(Long Id) throws AppException {
		return new EmailSettingsDAO().retrieveById(Id);
	}
	
	public List<EmailSettingsDO> retrieveByMailType(String mailType) throws AppException {
		return new EmailSettingsDAO().retrieveByMailType(mailType);
	}
	
	public List<EmailSettingsDO> retrieve() throws AppException {
		return new EmailSettingsDAO().retrieve();
	}
	
	public EmailSettingsDO update(EmailSettingsDO emailSettingsDO) throws AppException {
		return new EmailSettingsDAO().update(emailSettingsDO);
	}
}
