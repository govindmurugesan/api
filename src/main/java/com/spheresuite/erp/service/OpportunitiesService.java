package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.OpportunitiesDAO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.exception.AppException;

public class OpportunitiesService {
	static Logger logger = Logger.getLogger(OpportunitiesService.class.getName());
	
	public OpportunitiesDO persist(OpportunitiesDO projectDO) throws AppException {
		return new OpportunitiesDAO().persist(projectDO);
	}

	public List<OpportunitiesDO> retrieveById(Long Id) throws AppException {
		return new OpportunitiesDAO().retrieveById(Id);
	}
	
	public List<OpportunitiesDO> retrieveByContactId(Long Id) throws AppException {
		return new OpportunitiesDAO().retrieveByContactId(Id);
	}
	
	public Long retrieveBySalesStageId(Long Id) throws AppException {
		return new OpportunitiesDAO().retrieveBySalesStageId(Id);
	}
	
	public List<OpportunitiesDO> retrieveByCustomerId(Long Id) throws AppException {
		return new OpportunitiesDAO().retrieveByCustomerId(Id);
	}
	
	public List<OpportunitiesDO> retrieve(List<Long> Id) throws AppException {
		return new OpportunitiesDAO().retrieve(Id);
	}
	
	public List<OpportunitiesDO> retrieveAll() throws AppException {
		return new OpportunitiesDAO().retrieveAll();
	}
	
	public Long retrieveForCustomer(List<Long> ids) throws AppException {
		return new OpportunitiesDAO().retrieveForCustomer(ids);
	}
	
	public OpportunitiesDO update(OpportunitiesDO projectDO) throws AppException {
		return new OpportunitiesDAO().update(projectDO);
	}
	
	public boolean persistList(List<OpportunitiesDO> projectDO) throws AppException {
		return new OpportunitiesDAO().persistList(projectDO);
	}
}
