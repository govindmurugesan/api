package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeOnboardDAO;
import com.spheresuite.erp.domainobject.EmployeeOnboardDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeOnboardService {
	static Logger logger = Logger.getLogger(EmployeeOnboardService.class.getName());
	
	public EmployeeOnboardDO persist(EmployeeOnboardDO employeeOnboardDO) throws AppException {
		return new EmployeeOnboardDAO().persist(employeeOnboardDO);
	}

	public List<EmployeeOnboardDO> retriveById(Long id) throws AppException {
		return new EmployeeOnboardDAO().retrieveById(id);
	}
	
	public List<EmployeeOnboardDO> retrieve() throws AppException {
		return new EmployeeOnboardDAO().retrieve();
	}
	
	public List<EmployeeOnboardDO> retrieveByEmail(String personalEmail) throws AppException {
		return new EmployeeOnboardDAO().retrieveByEmail(personalEmail);
	}
	
	public EmployeeOnboardDO update(EmployeeOnboardDO employeeOnboardDO) throws AppException {
		return new EmployeeOnboardDAO().update(employeeOnboardDO);
	}

}
