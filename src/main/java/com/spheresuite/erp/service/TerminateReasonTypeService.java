package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.TerminateReasonTypeDAO;
import com.spheresuite.erp.domainobject.TerminateReasonTypeDO;
import com.spheresuite.erp.exception.AppException;

public class TerminateReasonTypeService {
	static Logger logger = Logger.getLogger(TerminateReasonTypeService.class.getName());
	
	public TerminateReasonTypeDO persist(TerminateReasonTypeDO terminateReasonTypeDO) throws AppException {
		return new TerminateReasonTypeDAO().persist(terminateReasonTypeDO);
	}

	public List<TerminateReasonTypeDO> retrieveActive() throws AppException {
		return new TerminateReasonTypeDAO().retrieveActive();
	}
	
	public List<TerminateReasonTypeDO> retrieveById(Long id) throws AppException {
		return new TerminateReasonTypeDAO().retrieveById(id);
	}
	
	public List<TerminateReasonTypeDO> retrieve() throws AppException {
		return new TerminateReasonTypeDAO().retrieve();
	}
	
	public TerminateReasonTypeDO update(TerminateReasonTypeDO terminateReasonTypeDO) throws AppException {
		return new TerminateReasonTypeDAO().update(terminateReasonTypeDO);
	}
}
