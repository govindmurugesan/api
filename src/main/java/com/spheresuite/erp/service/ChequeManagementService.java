package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ChequeManagementDAO;
import com.spheresuite.erp.domainobject.ChequeManagementDO;
import com.spheresuite.erp.exception.AppException;

public class ChequeManagementService {
	static Logger logger = Logger.getLogger(ChequeManagementService.class.getName());
	
	public ChequeManagementDO persist(ChequeManagementDO chequeManagementDO) throws AppException {
		return new ChequeManagementDAO().persist(chequeManagementDO);
	}

	public List<ChequeManagementDO> retriveById(Long id) throws AppException {
		return new ChequeManagementDAO().retrieveById(id);
	}
	
	public List<ChequeManagementDO> retrieveActive() throws AppException {
		return new ChequeManagementDAO().retrieveActive();
	}
	
	public List<ChequeManagementDO> retrieve() throws AppException {
		return new ChequeManagementDAO().retrieve();
	}
	
	public ChequeManagementDO update(ChequeManagementDO chequeManagementDO) throws AppException {
		return new ChequeManagementDAO().update(chequeManagementDO);
	}
	
	
}
