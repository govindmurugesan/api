package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.TaxPayeeTypeDAO;
import com.spheresuite.erp.domainobject.TaxPayeeTypeDO;
import com.spheresuite.erp.exception.AppException;

public class TaxPayeeTypeService {
	static Logger logger = Logger.getLogger(TaxPayeeTypeService.class.getName());
	
	public TaxPayeeTypeDO persist(TaxPayeeTypeDO taxPayeeType) throws AppException {
		return new TaxPayeeTypeDAO().persist(taxPayeeType);
	}

	public List<TaxPayeeTypeDO> retrieve() throws AppException {
		return new TaxPayeeTypeDAO().retrieve();
	}
	
	public List<TaxPayeeTypeDO> retrieveById(Long Id) throws AppException {
		return new TaxPayeeTypeDAO().retrieveById(Id);
	}
	
	public List<TaxPayeeTypeDO> retrieveActive(char status) throws AppException {
		return new TaxPayeeTypeDAO().retrieveActive(status);
	}
	
	
	public TaxPayeeTypeDO update(TaxPayeeTypeDO taxPayeeType) throws AppException {
		return new TaxPayeeTypeDAO().update(taxPayeeType);
	}
}
