package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeBonusDAO;
import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeBonusService {
	static Logger logger = Logger.getLogger(EmployeeBonusService.class.getName());
	
	public EmployeeBonusDO persist(EmployeeBonusDO employeeBonusDO) throws AppException {
		return new EmployeeBonusDAO().persist(employeeBonusDO);
	}

	public List<EmployeeBonusDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeBonusDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeBonusDO> retrieveByEmpIdAndMonth(Long empId, String month) throws AppException {
		return new EmployeeBonusDAO().retrieveByEmpIdAndMonth(empId,month);
	}
	
	public Long retrieveByEmpIdBetweenMonth(Long empId, String fromMonth, String toMonth) throws AppException{
		return new EmployeeBonusDAO().retrieveByEmpIdBetweenMonth(empId,fromMonth, toMonth);
	}
	
	public List<EmployeeBonusDO> retrieve() throws AppException {
		return new EmployeeBonusDAO().retrieve();
	}
	
	public List<EmployeeBonusDO> retrieveById(Long id) throws AppException {
		return new EmployeeBonusDAO().retrieveById(id);
	}
	
	public List<EmployeeBonusDO> retrieveByEmpIdWithDate(List<Long> id, String bonusMonth) throws AppException {
		return new EmployeeBonusDAO().retrieveByEmpIdWithDate(id, bonusMonth);
	}
	
	public List<EmployeeBonusDO> retrieveByEmpIdBetweenDate(List<Long> id, String fromMonth, String toMonth) throws AppException {
		return new EmployeeBonusDAO().retrieveByEmpIdBetweenDate(id, fromMonth, toMonth);
	}
	
	public EmployeeBonusDO update(EmployeeBonusDO employeeBonusDO) throws AppException {
		return new EmployeeBonusDAO().update(employeeBonusDO);
	}
}
