package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.GSTTaxSlabDAO;
import com.spheresuite.erp.domainobject.GSTTaxSlabDO;
import com.spheresuite.erp.exception.AppException;

public class GSTTaxSlabService {
	static Logger logger = Logger.getLogger(GSTTaxSlabService.class.getName());
	
	public GSTTaxSlabDO persist(GSTTaxSlabDO gstTaxDO) throws AppException {
		return new GSTTaxSlabDAO().persist(gstTaxDO);
	}

	public List<GSTTaxSlabDO> retrieveActive() throws AppException {
		return new GSTTaxSlabDAO().retrieveActive();
	}
	
	public List<GSTTaxSlabDO> retrieveById(Long Id) throws AppException {
		return new GSTTaxSlabDAO().retrieveById(Id);
	}
	
	public List<GSTTaxSlabDO> retrieve() throws AppException {
		return new GSTTaxSlabDAO().retrieve();
	}
	
	public GSTTaxSlabDO update(GSTTaxSlabDO gstTaxDO) throws AppException {
		return new GSTTaxSlabDAO().update(gstTaxDO);
	}
}
