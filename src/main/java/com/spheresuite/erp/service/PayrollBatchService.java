package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.PayrollBatchDAO;
import com.spheresuite.erp.domainobject.PayrollBatchDO;
import com.spheresuite.erp.exception.AppException;

public class PayrollBatchService {
	static Logger logger = Logger.getLogger(PayrollBatchService.class.getName());
	
	public PayrollBatchDO persist(PayrollBatchDO batchDO) throws AppException {
		return new PayrollBatchDAO().persist(batchDO);
	}

	public List<PayrollBatchDO> retrieveActive() throws AppException {
		return new PayrollBatchDAO().retrieveActive();
	}
	
	public List<PayrollBatchDO> retrieveById(Long id) throws AppException {
		return new PayrollBatchDAO().retrieveById(id);
	}
	
	public List<PayrollBatchDO> retrieve() throws AppException {
		return new PayrollBatchDAO().retrieve();
	}
	
	public PayrollBatchDO update(PayrollBatchDO batchDO) throws AppException {
		return new PayrollBatchDAO().update(batchDO);
	}
}
