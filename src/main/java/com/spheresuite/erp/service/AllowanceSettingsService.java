package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.AllowanceSettingsDAO;
import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.exception.AppException;

public class AllowanceSettingsService {
	static Logger logger = Logger.getLogger(AllowanceSettingsService.class.getName());
	
	public AllowanceSettingDO persist(AllowanceSettingDO allowanceSettingDO) throws AppException {
		return new AllowanceSettingsDAO().persist(allowanceSettingDO);
	}
	
	public AllowanceSettingDO update(AllowanceSettingDO allowanceSettingDO) throws AppException {
		return new AllowanceSettingsDAO().update(allowanceSettingDO);
	}

	public List<AllowanceSettingDO> retrieveById(Long Id) throws AppException {
		return new AllowanceSettingsDAO().retrieveById(Id);
	}
	
	public List<AllowanceSettingDO> retrieveActive(String name) throws AppException {
		return new AllowanceSettingsDAO().retrieveActive(name);
	}

	public List<AllowanceSettingDO> retrieve() throws AppException {
		return new AllowanceSettingsDAO().retrieve();
	}
}
