package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.exception.AppException;

public class CountryService {
	static Logger logger = Logger.getLogger(CountryService.class.getName());
	
	public CountryDO persist(CountryDO countryDO) throws AppException {
		return new CountryDAO().persist(countryDO);
	}

	public List<CountryDO> retrieveActive() throws AppException {
		return new CountryDAO().retrieveActive();
	}
	
	public List<CountryDO> retrieve() throws AppException {
		return new CountryDAO().retrieve();
	}
	
	public List<CountryDO> retrieveById(Long Id) throws AppException {
		return new CountryDAO().retrieveById(Id);
	}
	
	public CountryDO update(CountryDO countryDO) throws AppException {
		return new CountryDAO().update(countryDO);
	}
}
