package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.HrPoliciesDocDAO;
import com.spheresuite.erp.domainobject.HrPoliciesDocDO;
import com.spheresuite.erp.exception.AppException;

public class HrPoliciesDocService {
	static Logger logger = Logger.getLogger(HrPoliciesDocService.class.getName());
	
	public HrPoliciesDocDO persist(HrPoliciesDocDO hrpoliciesdoc) throws AppException {
		return new HrPoliciesDocDAO().persist(hrpoliciesdoc);
	}

	public List<HrPoliciesDocDO> retrieveByOppId(Long id) throws AppException {
		return new HrPoliciesDocDAO().retrieveByOppId(id);
	}
	
	public boolean delete(Long id) throws AppException {
		return new HrPoliciesDocDAO().delete(id);
	}
	
	public List<HrPoliciesDocDO> retrieve() throws AppException {
		return new HrPoliciesDocDAO().retrieve();
	}
	
	public HrPoliciesDocDO update(HrPoliciesDocDO hrpoliciesdoc) throws AppException {
		return new HrPoliciesDocDAO().update(hrpoliciesdoc);
	}
}
