package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.OpportunitiesDocDAO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.exception.AppException;

public class OpportunitiesDocService {
	static Logger logger = Logger.getLogger(OpportunitiesDocService.class.getName());
	
	public OpportunitiesDocDO persist(OpportunitiesDocDO opportunitiesDocDO) throws AppException {
		return new OpportunitiesDocDAO().persist(opportunitiesDocDO);
	}

	public List<OpportunitiesDocDO> retrieveByOppId(Long id) throws AppException {
		return new OpportunitiesDocDAO().retrieveByOppId(id);
	}
	
	public boolean delete(Long id) throws AppException {
		return new OpportunitiesDocDAO().delete(id);
	}
	
	public List<OpportunitiesDocDO> retrieve() throws AppException {
		return new OpportunitiesDocDAO().retrieve();
	}
	
	public OpportunitiesDocDO update(OpportunitiesDocDO opportunitiesDocDO) throws AppException {
		return new OpportunitiesDocDAO().update(opportunitiesDocDO);
	}
}
