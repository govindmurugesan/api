package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.DeductionSettingsDAO;
import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.exception.AppException;

public class DeductionSettingsService {
	static Logger logger = Logger.getLogger(DeductionSettingsService.class.getName());
	
	public DeductionSettingDO persist(DeductionSettingDO deductionSettingDO) throws AppException {
		return new DeductionSettingsDAO().persist(deductionSettingDO);
	}
	
	public DeductionSettingDO update(DeductionSettingDO deductionSettingDO) throws AppException {
		return new DeductionSettingsDAO().update(deductionSettingDO);
	}

	public List<DeductionSettingDO> retrieveById(Long Id) throws AppException {
		return new DeductionSettingsDAO().retrieveById(Id);
	}

	public List<DeductionSettingDO> retrieve() throws AppException {
		return new DeductionSettingsDAO().retrieve();
	}
	
	public List<DeductionSettingDO> retrieveActive(String name) throws AppException {
		return new DeductionSettingsDAO().retrieveActive(name);
	}
}
