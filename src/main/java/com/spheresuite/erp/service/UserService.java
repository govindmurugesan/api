package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.UserDAO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.exception.AppException;

public class UserService {
	static Logger logger = Logger.getLogger(UserService.class.getName());
	
	public UserDO persist(UserDO userDO) throws AppException {
		return new UserDAO().persist(userDO);
	}

	public List<UserDO> retrieveActive() throws AppException {
		return new UserDAO().retrieveActive();
	}
	
	public List<UserDO> retrieveByEmail(String personal, String secondary) throws AppException {
		return new UserDAO().retrieveByEmail(personal, secondary);
	}
	
	public List<UserDO> retriveById(Long id) throws AppException {
		return new UserDAO().retrieveById(id);
	}
	
	public List<UserDO> retrieveByTempPassword(String tempPassword, String email) throws AppException {
		return new UserDAO().retrieveByTempPassword(tempPassword, email);
	}
	
	public List<UserDO> retrieveByEmailId(String email) throws AppException {
		return new UserDAO().retrieveByEmailId(email);
	}
	
	public List<UserDO> retrieve() throws AppException {
		return new UserDAO().retrieve();
	}
	
	public UserDO update(UserDO userDO) throws AppException {
		return new UserDAO().update(userDO);
	}
	
	public List<UserDO> retrieveForLogin(String email, String pasword) throws AppException {
		return new UserDAO().retrieveForLogin(email, pasword);
	}
	
	public List<UserDO> retriveByEmpId(Long id) throws AppException {
		return new UserDAO().retrieveByEmpId(id);
	}
	
	public List<UserDO> getSuperAdmins() throws AppException {
		return new UserDAO().getSuperAdmins();
	}
}
