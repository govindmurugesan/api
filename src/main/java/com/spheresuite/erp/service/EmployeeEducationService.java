package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeEducationDAO;
import com.spheresuite.erp.domainobject.EmployeeEducationDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeEducationService {
	static Logger logger = Logger.getLogger(EmployeeEducationService.class.getName());
	
	public EmployeeEducationDO persist(EmployeeEducationDO employeeEducationDO) throws AppException {
		return new EmployeeEducationDAO().persist(employeeEducationDO);
	}

	public List<EmployeeEducationDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeEducationDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeEducationDO> retrieveById(Long id) throws AppException {
		return new EmployeeEducationDAO().retrieveById(id);
	}
	
	public EmployeeEducationDO update(EmployeeEducationDO employeeEducationDO) throws AppException {
		return new EmployeeEducationDAO().update(employeeEducationDO);
	}
}
