package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.NotificationSettingsDAO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.exception.AppException;

public class NotificationSettingsService {
	static Logger logger = Logger.getLogger(NotificationSettingsService.class.getName());
	
	public NotificationSettingsDO persist(NotificationSettingsDO notificationSettingsDO) throws AppException {
		return new NotificationSettingsDAO().persist(notificationSettingsDO);
	}
	
	public NotificationSettingsDO update(NotificationSettingsDO notificationSettingsDO) throws AppException {
		return new NotificationSettingsDAO().update(notificationSettingsDO);
	}

	public List<NotificationSettingsDO> retrieveById(Long Id) throws AppException {
		return new NotificationSettingsDAO().retrieveById(Id);
	}

	public List<NotificationSettingsDO> retrieve() throws AppException {
		return new NotificationSettingsDAO().retrieve();
	}
}
