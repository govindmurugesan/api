package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.TaxSlabsSettingsDAO;
import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.exception.AppException;

public class TaxSlabsSettingsService {
	static Logger logger = Logger.getLogger(TaxSlabsSettingsService.class.getName());
	
	public TaxSlabsSettingsDO persist(TaxSlabsSettingsDO taxSettingDO) throws AppException {
		return new TaxSlabsSettingsDAO().persist(taxSettingDO);
	}
	
	public TaxSlabsSettingsDO update(TaxSlabsSettingsDO taxSettingDO) throws AppException {
		return new TaxSlabsSettingsDAO().update(taxSettingDO);
	}

	public List<TaxSlabsSettingsDO> retrieveById(Long Id) throws AppException {
		return new TaxSlabsSettingsDAO().retrieveById(Id);
	}
	
	public List<TaxSlabsSettingsDO> retrieveBytaxPayee(Long Id, String startDate, String endDate) throws AppException {
		return new TaxSlabsSettingsDAO().retrieveBytaxPayee(Id, startDate, endDate);
	}

	public List<TaxSlabsSettingsDO> retrieve() throws AppException {
		return new TaxSlabsSettingsDAO().retrieve();
	}
	
	public List<TaxSlabsSettingsDO> retrieveActive() throws AppException {
		return new TaxSlabsSettingsDAO().retrieveActive();
	}
}
