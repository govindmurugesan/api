package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.PtDAO;
import com.spheresuite.erp.domainobject.PtDO;
import com.spheresuite.erp.exception.AppException;

public class PtService {
	static Logger logger = Logger.getLogger(PtService.class.getName());
	
	public PtDO persist(PtDO ptDO) throws AppException {
		return new PtDAO().persist(ptDO);
	}

	public List<PtDO> retrieveActive() throws AppException {
		return new PtDAO().retrieveActive();
	}
	
	public List<PtDO> retrieveActiveById(Long Id) throws AppException {
		return new PtDAO().retrieveActiveById(Id);
	}
	
	public List<PtDO> retrieveByState(Long stateId) throws AppException {
		return new PtDAO().retrieveByState(stateId);
	}
	
	public List<PtDO> retrieve() throws AppException {
		return new PtDAO().retrieve();
	}
	
	public PtDO update(PtDO ptDO) throws AppException {
		return new PtDAO().update(ptDO);
	}
}
