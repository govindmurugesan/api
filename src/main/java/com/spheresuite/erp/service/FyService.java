package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.FyDAO;
import com.spheresuite.erp.domainobject.FyDO;
import com.spheresuite.erp.exception.AppException;

public class FyService {
	static Logger logger = Logger.getLogger(FyService.class.getName());
	
	public FyDO persist(FyDO fyDO) throws AppException {
		return new FyDAO().persist(fyDO);
	}

	public List<FyDO> retrieveActive() throws AppException {
		return new FyDAO().retrieveActive();
	}
	
	public List<FyDO> retrieveById(Long Id) throws AppException {
		return new FyDAO().retrieveById(Id);
	}
	
	public List<FyDO> retrieve() throws AppException {
		return new FyDAO().retrieve();
	}
	
	public FyDO update(FyDO fyDO) throws AppException {
		return new FyDAO().update(fyDO);
	}
}
