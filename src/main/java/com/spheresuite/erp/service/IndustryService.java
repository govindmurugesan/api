package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.IndustryDAO;
import com.spheresuite.erp.domainobject.IndustryDO;
import com.spheresuite.erp.exception.AppException;

public class IndustryService {
	static Logger logger = Logger.getLogger(IndustryService.class.getName());
	
	public IndustryDO persist(IndustryDO industryDO) throws AppException {
		return new IndustryDAO().persist(industryDO);
	}

	public List<IndustryDO> retrieveActive() throws AppException {
		return new IndustryDAO().retrieveActive();
	}
	
	public List<IndustryDO> retrieveById(Long Id) throws AppException {
		return new IndustryDAO().retrieveById(Id);
	}
	
	public List<IndustryDO> retrieve() throws AppException {
		return new IndustryDAO().retrieve();
	}
	
	public IndustryDO update(IndustryDO industryDO) throws AppException {
		return new IndustryDAO().update(industryDO);
	}
}
