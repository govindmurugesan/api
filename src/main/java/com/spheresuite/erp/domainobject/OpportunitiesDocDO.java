package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="opportinutiesdoc")
@TableGenerator(name ="opportinutiesdoc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "opportinutiesdoc.findAll", query = "SELECT r FROM OpportunitiesDocDO r"),
    @NamedQuery(name = "opportinutiesdoc.findByOppId", query = "SELECT r FROM OpportunitiesDocDO r where r.opportunityId =:id"),
})
public class OpportunitiesDocDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "opportinutiesdoc.findAll";
	
	public static final String FIND_BY_OPP_ID = "opportinutiesdoc.findByOppId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "opportinutiesdoc")
	private Long id;
	private Long opportunityId;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String fileName;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(Long opportunityId) {
		this.opportunityId = opportunityId;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}