package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="GSTTaxSlab")
@TableGenerator(name ="GSTTaxSlab", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "GSTTaxSlab.findAll", query = "SELECT r FROM GSTTaxSlabDO r"),
    @NamedQuery(name = "GSTTaxSlab.findById", query = "SELECT r FROM GSTTaxSlabDO r where r.id =:id"),
    @NamedQuery(name = "GSTTaxSlab.findByStatus", query = "SELECT r FROM GSTTaxSlabDO r where r.status =:status"),
})
public class GSTTaxSlabDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "GSTTaxSlab.findAll";
	
	public static final String FIND_BY_ID = "GSTTaxSlab.findById";
	
	public static final String FIND_BY_STATUS = "GSTTaxSlab.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "GSTTaxSlab")
	private Long id;
	private String percentage;
	private char status;
	private String updatedby;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}