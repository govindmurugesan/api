package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="payrollearningsdetails")
@TableGenerator(name ="payrollearningsdetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "payrollearningsdetails.findAll", query = "SELECT r FROM PayrollEarningsDetailsDO r"),
    @NamedQuery(name = "payrollearningsdetails.findByEmpPayrollId", query = "SELECT r FROM PayrollEarningsDetailsDO r where r.payrollid =:id"),
    @NamedQuery(name = "payrollearningsdetails.findByEmpPayrollIds", query = "SELECT r FROM PayrollEarningsDetailsDO r where r.payrollid IN :id"),
})
public class PayrollEarningsDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "payrollearningsdetails.findAll";
	
	public static final String FIND_BY_ID = "payrollearningsdetails.findByEmpPayrollId";
	
	public static final String FIND_BY_IDS = "payrollearningsdetails.findByEmpPayrollIds";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "payrollearningsdetails")
	private Long id;
	private Long earningsId;
	private Long payrollid;
	private Double monthly;
	private Double ytd;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEarningsId() {
		return earningsId;
	}
	public void setEarningsId(Long earningsId) {
		this.earningsId = earningsId;
	}
	public Long getPayrollid() {
		return payrollid;
	}
	public void setPayrollid(Long payrollid) {
		this.payrollid = payrollid;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Double getYtd() {
		return ytd;
	}
	public void setYtd(Double ytd) {
		this.ytd = ytd;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}