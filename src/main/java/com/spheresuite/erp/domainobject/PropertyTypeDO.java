package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="propertytype")
@TableGenerator(name ="propertytype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "propertytype.findAll", query = "SELECT r FROM PropertyTypeDO r"),
    @NamedQuery(name = "propertytype.findById", query = "SELECT r FROM PropertyTypeDO r where r.id =:id"),
    @NamedQuery(name = "propertytype.findByStatus", query = "SELECT r FROM PropertyTypeDO r where r.status =:status"),
})
public class PropertyTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "propertytype.findAll";
	
	public static final String FIND_BY_ID = "propertytype.findById";
	
	public static final String FIND_BY_STATUS = "propertytype.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "propertytype")
	private Long id;
	private String name;
	private char status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String updatedBy;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}