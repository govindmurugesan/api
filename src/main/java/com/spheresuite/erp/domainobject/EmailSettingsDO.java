package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="emailsettings")
@TableGenerator(name ="emailsettings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "emailsettings.findAll", query = "SELECT r FROM EmailSettingsDO r"),
    @NamedQuery(name = "emailsettings.findById", query = "SELECT r FROM EmailSettingsDO r where r.id =:id"),
    @NamedQuery(name = "emailsettings.findByStatus", query = "SELECT r FROM EmailSettingsDO r where r.status =:status"),
    @NamedQuery(name = "emailsettings.findByMailType", query = "SELECT r FROM EmailSettingsDO r where r.mailtype =:mailType and r.status =:status"),
})
public class EmailSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "emailsettings.findAll";
	
	public static final String FIND_BY_ID = "emailsettings.findById";
	
	public static final String FIND_BY_STATUS = "emailsettings.findByStatus";
	
	public static final String FIND_BY_MAILTYPE = "emailsettings.findByMailType";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "emailsettings")
	private Long id;
	private String mailtype;
	private String imaphost;
	private String imapport;
	private String smtphost;
	private String smtpport;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public String getMailtype() {
		return mailtype;
	}

	public void setMailtype(String mailtype) {
		this.mailtype = mailtype;
	}

	public String getImaphost() {
		return imaphost;
	}

	public void setImaphost(String imaphost) {
		this.imaphost = imaphost;
	}

	public String getImapport() {
		return imapport;
	}

	public void setImapport(String imapport) {
		this.imapport = imapport;
	}

	public String getSmtphost() {
		return smtphost;
	}

	public void setSmtphost(String smtphost) {
		this.smtphost = smtphost;
	}

	public String getSmtpport() {
		return smtpport;
	}

	public void setSmtpport(String smtpport) {
		this.smtpport = smtpport;
	}
}