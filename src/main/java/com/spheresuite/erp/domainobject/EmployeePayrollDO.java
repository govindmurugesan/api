package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeepayroll")
@TableGenerator(name ="employeepayroll", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeepayroll.findAll", query = "SELECT r FROM EmployeePayrollDO r where r.status =:status"),
    @NamedQuery(name = "employeepayroll.findById", query = "SELECT r FROM EmployeePayrollDO r where r.id =:id"),
    @NamedQuery(name = "employeepayroll.findByStatus", query = "SELECT r FROM EmployeePayrollDO r where r.status =:status and r.empId =:empId"),
    @NamedQuery(name = "employeepayroll.findByEmpId", query = "SELECT r FROM EmployeePayrollDO r where r.empId =:empId and r.status =:status"),
    @NamedQuery(name = "employeepayroll.findByMonth", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly and r.status =:status and r.payrollbatchId =:payrollBatchId"),
    @NamedQuery(name = "employeepayroll.retriveByMonthAndEmp", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly and r.empId =:empId"),
    @NamedQuery(name = "employeepayroll.retriveForPayrollSummary", query = "SELECT count(r),r.payrollMonth,r.payrollbatchId,sum(r.netmonth),sum(r.earningmonth),sum(r.deductionmonth),sum(r.grossPay),r.status, r.updatedon, r.updatedBy,sum(r.taxMonth) FROM EmployeePayrollDO r group by r.payrollMonth, r.payrollbatchId, r.status, r.updatedon, r.updatedBy"),
    @NamedQuery(name = "employeepayroll.findAllEmp", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:payrollMonth and r.payrollbatchId =:payrollBatchId and r.status=:status"),
    @NamedQuery(name = "employeepayroll.findByEmpIdBetweenDateSum", query = "SELECT sum(r.netmonth), sum(r.earningmonth), sum(r.deductionmonth), sum(r.taxMonth) FROM EmployeePayrollDO r where r.empId =:empId and (r.payrollMonth >= :fromMonth or r.payrollMonth <= :toMonth) and r.status =:status"),
    @NamedQuery(name = "employeepayroll.findByEmpIdDateSum", query = "SELECT sum(r.netmonth), sum(r.earningmonth), sum(r.deductionmonth), sum(r.taxMonth) FROM EmployeePayrollDO r where r.empId =:empId and r.payrollMonth =:fromMonth and r.status =:status"),
    @NamedQuery(name = "employeepayroll.findByEmpIdBetweenDate", query = "SELECT r FROM EmployeePayrollDO r where r.empId =:empId and (r.payrollMonth >= :fromMonth or r.payrollMonth <= :toMonth) and r.status =:status"),
    @NamedQuery(name = "employeepayroll.retriveByMonthForReport", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly"),
    @NamedQuery(name = "employeepayroll.retriveByMonthForReportBatch", query = "SELECT r FROM EmployeePayrollDO r where r.payrollMonth =:monthly and r.payrollbatchId =:payrollBatchId"),
    @NamedQuery(name = "employeepayroll.findByEmpIdBetweenDateForReportBatch", query = "SELECT r FROM EmployeePayrollDO r where (r.payrollMonth >= :fromMonth or r.payrollMonth <= :toMonth) and r.payrollbatchId =:payrollBatchId"),
    @NamedQuery(name = "employeepayroll.findByEmpIdBetweenDateForReport", query = "SELECT r FROM EmployeePayrollDO r where (r.payrollMonth >= :fromMonth or r.payrollMonth <= :toMonth)"),
})

public class EmployeePayrollDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeepayroll.findAll";
	
	public static final String FIND_BY_ID = "employeepayroll.findById";
	
	public static final String FIND_BY_STATUS = "employeepayroll.findByStatus";
	
	public static final String FIND_BY_EMP_ID = "employeepayroll.findByEmpId";
	
	public static final String FIND_BY_EMP_BEWTEENDATE = "employeepayroll.findByEmpIdBetweenDate";
	
	public static final String FIND_BY_EMP_BEWTEENDATE_SUM = "employeepayroll.findByEmpIdBetweenDateSum";
	
	public static final String FIND_BY_EMP_DATE_SUM = "employeepayroll.findByEmpIdDateSum";
	
	public static final String FIND_BY_MONTH = "employeepayroll.findByMonth";
	
	public static final String FIND_BY_MONTH_EMP = "employeepayroll.retriveByMonthAndEmp";
	
	public static final String FIND_FOR_SUMMARY = "employeepayroll.retriveForPayrollSummary";
	
	public static final String FIND_ALL_EMP = "employeepayroll.findAllEmp";
	
	public static final String FIND_FOR_REPORT="employeepayroll.retriveByMonthForReport";
	
	public static final String FIND_FOR_REPORT_BATCH="employeepayroll.retriveByMonthForReportBatch";
	
	public static final String FIND_FOR_REPORT_BEWTEENDATE_BATCH="employeepayroll.findByEmpIdBetweenDateForReportBatch";
	
	public static final String FIND_FOR_REPORT_BEWTEENDATE="employeepayroll.findByEmpIdBetweenDateForReport";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeepayroll")
	private Long id;
	private Long empId;
	private Long compensationId;
	private String payrollMonth;
	private Double netmonth;
	private Double earningmonth;
	private Double deductionmonth;
	private char status;
	private Double grossPay;
	@Temporal(TemporalType.DATE)
	private Date updatedon;
	private String updatedBy;
	private Long createdby;
	private String empName;
	private Long payrollbatchId;
	private Double taxMonth;
	/*private String payrollType;*/

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getCompensationId() {
		return compensationId;
	}
	public void setCompensationId(Long compensationId) {
		this.compensationId = compensationId;
	}
	public String getPayrollMonth() {
		return payrollMonth;
	}
	public void setPayrollMonth(String payrollMonth) {
		this.payrollMonth = payrollMonth;
	}
	public Double getNetmonth() {
		return netmonth;
	}
	public void setNetmonth(Double netmonth) {
		this.netmonth = netmonth;
	}
	public Double getEarningmonth() {
		return earningmonth;
	}
	public void setEarningmonth(Double earningmonth) {
		this.earningmonth = earningmonth;
	}
	public Double getDeductionmonth() {
		return deductionmonth;
	}
	public void setDeductionmonth(Double deductionmonth) {
		this.deductionmonth = deductionmonth;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Double getGrossPay() {
		return grossPay;
	}
	public void setGrossPay(Double grossPay) {
		this.grossPay = grossPay;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Long getPayrollbatchId() {
		return payrollbatchId;
	}
	public void setPayrollbatchId(Long payrollbatchId) {
		this.payrollbatchId = payrollbatchId;
	}
	/*public String getPayrollType() {
		return payrollType;
	}
	public void setPayrollType(String payrollType) {
		this.payrollType = payrollType;
	}*/
	public Double getTaxMonth() {
		return taxMonth;
	}
	public void setTaxMonth(Double taxMonth) {
		this.taxMonth = taxMonth;
	}
}