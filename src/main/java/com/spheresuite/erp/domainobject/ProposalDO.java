package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="proposal")
@TableGenerator(name ="proposal", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "proposal.findAll", query = "SELECT r FROM ProposalDO r where r.empId IN :id"),
    @NamedQuery(name = "proposal.findAllData", query = "SELECT r FROM ProposalDO r"),
    @NamedQuery(name = "proposal.findById", query = "SELECT r FROM ProposalDO r where r.id =:id"),
    @NamedQuery(name = "proposal.findAllByCustomerId", query = "SELECT r FROM ProposalDO r where r.customerId =:id"),
})
public class ProposalDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "proposal.findAll";
	
	public static final String FIND_BY_ID = "proposal.findById";
	
	public static final String FIND_ALL_DATA = "proposal.findAllData";
	
	public static final String FIND_ALL_BY_CUSTOMERID = "proposal.findAllByCustomerId";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "proposal")
	private Long id;
	private Long customerId;
	private Long projectId;
	private Long projectTypeId;
	private String currencyType;
	private Long qunatity;	
	private Long paymentTermId;
	private String invoiceTerm;
	private String rate;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Long getProjectTypeId() {
		return projectTypeId;
	}
	public void setProjectTypeId(Long projectTypeId) {
		this.projectTypeId = projectTypeId;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	public Long getQunatity() {
		return qunatity;
	}
	public void setQunatity(Long qunatity) {
		this.qunatity = qunatity;
	}
	public Long getPaymentTermId() {
		return paymentTermId;
	}
	public void setPaymentTermId(Long paymentTermId) {
		this.paymentTermId = paymentTermId;
	}
	public String getInvoiceTerm() {
		return invoiceTerm;
	}
	public void setInvoiceTerm(String invoiceTerm) {
		this.invoiceTerm = invoiceTerm;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}