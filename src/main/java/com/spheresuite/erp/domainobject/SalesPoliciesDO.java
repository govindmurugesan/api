package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="salespolicies")
@TableGenerator(name ="salespolicies", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "salespolicies.findAll", query = "SELECT r FROM SalesPoliciesDO r where r.empId IN :id"),
    @NamedQuery(name = "salespolicies.findAllData", query = "SELECT r FROM SalesPoliciesDO r"),
    @NamedQuery(name = "salespolicies.findById", query = "SELECT r FROM SalesPoliciesDO r where r.id =:id"),
})
public class SalesPoliciesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "salespolicies.findAll";
	
	public static final String FIND_BY_ID = "salespolicies.findById";
	
	public static final String FIND_ALL_DATA="salespolicies.findAllData";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "salespolicies")
	private Long id;
	@Column(columnDefinition="LONGTEXT")
	private String name;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private char status;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}