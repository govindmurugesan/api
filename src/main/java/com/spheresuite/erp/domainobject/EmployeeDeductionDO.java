package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeededuction")
@TableGenerator(name ="employeededuction", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeededuction.findAll", query = "SELECT r FROM EmployeeDeductionDO r"),
    @NamedQuery(name = "employeededuction.findByEmpCompensationId", query = "SELECT r FROM EmployeeDeductionDO r where r.empcompensationid =:id"),
})
public class EmployeeDeductionDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeededuction.findAll";
	
	public static final String FIND_BY_ID = "employeededuction.findByEmpCompensationId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeededuction")
	private Long id;
	
	private Long empcompensationid;
	private Long empdeductionid;
	private Double monthly;
	private Double ytd;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmpcompensationid() {
		return empcompensationid;
	}
	public void setEmpcompensationid(Long empcompensationid) {
		this.empcompensationid = empcompensationid;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Double getYtd() {
		return ytd;
	}
	public void setYtd(Double ytd) {
		this.ytd = ytd;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEmpdeductionid() {
		return empdeductionid;
	}
	public void setEmpdeductionid(Long empdeductionid) {
		this.empdeductionid = empdeductionid;
	}
}