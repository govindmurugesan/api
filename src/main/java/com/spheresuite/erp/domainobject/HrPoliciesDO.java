package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrpolicies")
@TableGenerator(name ="hrpolicies", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrpolicies.findAll", query = "SELECT r FROM HrPoliciesDO r"),
    @NamedQuery(name = "hrpolicies.findById", query = "SELECT r FROM HrPoliciesDO r where r.id =:id"),
    @NamedQuery(name = "hrpolicies.findByRoleId", query = "SELECT r FROM HrPoliciesDO r where (r.roleId =:id or r.roleId IS NULL)"),
})
public class HrPoliciesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrpolicies.findAll";
	
	public static final String FIND_BY_ID = "hrpolicies.findById";
	
	public static final String FIND_BY_ROLEID = "hrpolicies.findByRoleId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "hrpolicies")
	private Long id;
	@Column(columnDefinition="LONGTEXT")
	private String name;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private char status;
	private Long empId;
	private Long roleId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}