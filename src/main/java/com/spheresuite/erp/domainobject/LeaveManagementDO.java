package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leavemanagement")
@TableGenerator(name ="leavemanagement", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "leavemanagement.findAll", query = "SELECT r FROM LeaveManagementDO r"),
    @NamedQuery(name = "leavemanagement.findAllByDept ", query = "SELECT r FROM LeaveManagementDO r where r.empId IN :id"),
    @NamedQuery(name = "leavemanagement.findById", query = "SELECT r FROM LeaveManagementDO r where r.id =:id"),
    @NamedQuery(name = "leavemanagement.findByEmpId", query = "SELECT r FROM LeaveManagementDO r where r.empId =:id"),
    @NamedQuery(name = "leavemanagement.findActiveById", query = "SELECT r FROM LeaveManagementDO r where r.id =:id and r.status =:status"),
    @NamedQuery(name = "leavemanagement.findActiveByType", query = "SELECT r FROM LeaveManagementDO r where r.leaveType =:type and r.status =:status"),
    @NamedQuery(name = "leavemanagement.findActive", query = "SELECT r FROM LeaveManagementDO r where r.status =:status"),
})
public class LeaveManagementDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "leavemanagement.findAll";
	public static final String FIND_ALL_BY_DEPT = "leavemanagement.findAllByDept";
	public static final String FIND_BY_ID = "leavemanagement.findById";
	public static final String FINDBY_EMP_ID = "leavemanagement.findByEmpId";
	public static final String FIND_ACTIVE_BY_ID = "leavemanagement.findActiveById";
	public static final String FIND_ACTIVE_BY_TYPE = "leavemanagement.findActiveByType";
	public static final String FIND_ACTIVE = "leavemanagement.findActive";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "leavemanagement")
	private Long id;
	
	private Long leaveType;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private char status;
	private Long empId;
	private String fromDate;
	private String toDate;
	private Long NumberOfDays;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(Long leaveType) {
		this.leaveType = leaveType;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Long getNumberOfDays() {
		return NumberOfDays;
	}
	public void setNumberOfDays(Long numberOfDays) {
		NumberOfDays = numberOfDays;
	}
	
}