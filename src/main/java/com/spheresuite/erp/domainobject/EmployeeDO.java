package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employee")
@TableGenerator(name ="employee", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employee.findAll", query = "SELECT r FROM EmployeeDO r"),
    @NamedQuery(name = "employee.findActive", query = "SELECT r FROM EmployeeDO r where r.status=:status"),
    @NamedQuery(name = "employee.findIsActive", query = "SELECT r FROM EmployeeDO r where r.status=:status and r.id =:id"),
    @NamedQuery(name = "employee.findById", query = "SELECT r FROM EmployeeDO r where r.id =:id"),
    @NamedQuery(name = "employee.findEmpId", query = "SELECT r FROM EmployeeDO r where r.id =:id"),
    @NamedQuery(name = "employee.findByEmail", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email or r.personalemail =:email"),
    @NamedQuery(name = "employee.getCount", query = "SELECT COUNT(r) FROM EmployeeDO r"),
    @NamedQuery(name = "employee.checkDuplicateInsert", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email or r.empId =:empId and r.status=:status"),
    @NamedQuery(name = "employee.checkDuplicateUpdate", query = "SELECT r FROM EmployeeDO r where (r.companyemail =:email or r.empId =:empId) and r.id NOT IN :id and r.status=:status"),
    @NamedQuery(name = "employee.checkDuplicateUpdateNoEmpId", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email and r.id NOT IN :id and r.status=:status"),
    @NamedQuery(name = "employee.checkDuplicateInsertNoEmpId", query = "SELECT r FROM EmployeeDO r where r.companyemail =:email and r.status=:status"),
    @NamedQuery(name = "employee.findByDept", query = "SELECT r.id FROM EmployeeDO r where r.empdept IN :id and r.status=:status"),
    @NamedQuery(name = "employee.findEmployeeNoCtc", query = "SELECT r FROM EmployeeDO r where r.status=:status and r.id NOT IN :id"),
})
public class EmployeeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employee.findAll";
	
	public static final String FIND_ACTIVE = "employee.findActive";
	
	public static final String FIND_IS_ACTIVE = "employee.findIsActive";
	
	public static final String FIND_BY_DEPT = "employee.findByDept";
	
	public static final String FIND_BY_ID = "employee.findById";

	public static final String FIND_BY_EMAIL = "employee.findByEmail";
	
	public static final String FIND_EMP_ID = "employee.findEmpId";
	
	public static final String FIND_DUPLICATE_INSERT = "employee.checkDuplicateInsert";
	
	public static final String FIND_DUPLICATE_UPDATE = "employee.checkDuplicateUpdate";
	
	public static final String FIND_DUPLICATE_UPDATE_NOEMP = "employee.checkDuplicateUpdateNoEmpId";
	
	public static final String FIND_DUPLICATE_NOEMP = "employee.checkDuplicateInsertNoEmpId";
	
	public static final String FIND_EMP_NOCTC = "employee.findEmployeeNoCtc";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employee")
	private Long id;
	private Long workLocationId;
	private String empId;
	private String firstname;
	private String lastname;
	private String middlename;
	private String personalcontact;
	private String primarycontact;
	private String companyemail;
	private String personalemail;
	private String aadhar;
	private String panno;
	private String dateofbirth;
	private Long emptype;
	private Long reportto;
	private Long empdept;
	private String title;
	private String jobdesc;
	private String startdate;
	private char gender;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String emailId;
	private String emailPassword;
	private String emailType;
	private Long createdBy;
	private char status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getWorkLocationId() {
		return workLocationId;
	}
	public void setWorkLocationId(Long workLocationId) {
		this.workLocationId = workLocationId;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getPersonalcontact() {
		return personalcontact;
	}
	public void setPersonalcontact(String personalcontact) {
		this.personalcontact = personalcontact;
	}
	public String getPrimarycontact() {
		return primarycontact;
	}
	public void setPrimarycontact(String primarycontact) {
		this.primarycontact = primarycontact;
	}
	public String getCompanyemail() {
		return companyemail;
	}
	public void setCompanyemail(String companyemail) {
		this.companyemail = companyemail;
	}
	public String getPersonalemail() {
		return personalemail;
	}
	public void setPersonalemail(String personalemail) {
		this.personalemail = personalemail;
	}
	public String getAadhar() {
		return aadhar;
	}
	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public Long getEmptype() {
		return emptype;
	}
	public void setEmptype(Long emptype) {
		this.emptype = emptype;
	}
	public Long getReportto() {
		return reportto;
	}
	public void setReportto(Long reportto) {
		this.reportto = reportto;
	}
	public Long getEmpdept() {
		return empdept;
	}
	public void setEmpdept(Long empdept) {
		this.empdept = empdept;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getJobdesc() {
		return jobdesc;
	}
	public void setJobdesc(String jobdesc) {
		this.jobdesc = jobdesc;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getEmailPassword() {
		return emailPassword;
	}
	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}
	public String getEmailType() {
		return emailType;
	}
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
}