package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="schedule")
@TableGenerator(name ="schedule", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "schedule.findById", query = "SELECT r FROM ScheduleDO r where r.id =:id"),
    @NamedQuery(name = "schedule.findByLeadId", query = "SELECT r FROM ScheduleDO r where r.leadId =:id"),
    @NamedQuery(name = "schedule.findUpcommingCalender", query = "SELECT r FROM ScheduleDO r where r.logDate >= :currentDate AND r.empId=:empId order by r.logDate asc"),
    @NamedQuery(name = "schedule.findUpcommingCalenderOfEmp", query = "SELECT r FROM ScheduleDO r where r.empId=:empId"),
})
public class ScheduleDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_ID = "schedule.findById";
	
	public static final String FIND_BY_LEAD_ID = "schedule.findByLeadId";
	
	public static final String FIND_UPCOMMING_CALENDER = "schedule.findUpcommingCalender";
	
	public static final String FIND_UPCOMMING_CALENDER_OF_EMP = "schedule.findUpcommingCalenderOfEmp";
			
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "schedule")
	private Long id;
	private Long leadId;
	private String description;
	private String subject;
	private String contactList;
	private String logDate;
	private String logTime;
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContactList() {
		return contactList;
	}

	public void setContactList(String contactList) {
		this.contactList = contactList;
	}

	public String getLogDate() {
		return logDate;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}

	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}