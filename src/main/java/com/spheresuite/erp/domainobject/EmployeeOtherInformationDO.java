package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="otherinformation")
@TableGenerator(name ="otherinformation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "otherinformation.findAll", query = "SELECT r FROM EmployeeOtherInformationDO r"),
    @NamedQuery(name = "otherinformation.findById", query = "SELECT r FROM EmployeeOtherInformationDO r where r.id =:id"),
    @NamedQuery(name = "otherinformation.findByEmpId", query = "SELECT r FROM EmployeeOtherInformationDO r where r.empId =:empId order by r.id desc"),
})
public class EmployeeOtherInformationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "otherinformation.findAll";
	
	public static final String FIND_BY_ID = "otherinformation.findById";
	
	public static final String FIND_BY_EMPID = "otherinformation.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "otherinformation")
	private Long id;
	private String voterId;
	private String rationCard;
	private String maritalStatus;
	private String spouseName;
	private Long childern;
	private String bloodGroup;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getRationCard() {
		return rationCard;
	}
	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getSpouseName() {
		return spouseName;
	}
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
	public Long getChildern() {
		return childern;
	}
	public void setChildern(Long childern) {
		this.childern = childern;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}