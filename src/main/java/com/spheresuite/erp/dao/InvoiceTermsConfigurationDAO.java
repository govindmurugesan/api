package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.InvoiceTermsConfigurationDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class InvoiceTermsConfigurationDAO {
	static Logger logger = Logger.getLogger(InvoiceTermsConfigurationDAO.class.getName());
	private EntityManager em = null;
	
	public InvoiceTermsConfigurationDO persist(InvoiceTermsConfigurationDO invoiceTerms) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && invoiceTerms != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(invoiceTerms);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return invoiceTerms;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<InvoiceTermsConfigurationDO> retrieve() throws AppException {
		List<InvoiceTermsConfigurationDO> invoicetermsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(InvoiceTermsConfigurationDO.FIND_ALL);
				invoicetermsList = (List<InvoiceTermsConfigurationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return invoicetermsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceTermsConfigurationDO> retrieveActive() throws AppException {
		List<InvoiceTermsConfigurationDO> invoiceTermsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(InvoiceTermsConfigurationDO.FIND_BY_STATUS);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				invoiceTermsList = (List<InvoiceTermsConfigurationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return invoiceTermsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceTermsConfigurationDO> retrieveById(Long Id) throws AppException {
		List<InvoiceTermsConfigurationDO> termsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(InvoiceTermsConfigurationDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				termsList = (List<InvoiceTermsConfigurationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return termsList;
	}
	
	public InvoiceTermsConfigurationDO update(InvoiceTermsConfigurationDO invoicetermsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(invoicetermsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return invoicetermsDO;
	}

}
