package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.SalesTargetDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class SalesTargetDAO {
	static Logger logger = Logger.getLogger(SalesTargetDAO.class.getName());
	private EntityManager em = null;
	
	public SalesTargetDO persist(SalesTargetDO salesTargetDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && salesTargetDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(salesTargetDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salesTargetDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieve(List<Long> id) throws AppException {
		List<SalesTargetDO> billsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesTargetDO.FIND_ALL);
				q.setParameter(CommonConstants.ID, id);
				billsList = (List<SalesTargetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return billsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveAll() throws AppException {
		List<SalesTargetDO> billsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesTargetDO.FIND_ALL_DATA);
				billsList = (List<SalesTargetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return billsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveById(Long id) throws AppException {
		List<SalesTargetDO> proposal = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesTargetDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				proposal = (List<SalesTargetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposal;
	}
	
	public SalesTargetDO update(SalesTargetDO salesTargetDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(salesTargetDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salesTargetDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveByEmpId(Long empId) throws AppException {
		List<SalesTargetDO> proposal = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesTargetDO.FIND_BY_EMP_ID);
				q.setParameter(CommonConstants.EMPID, empId);
				proposal = (List<SalesTargetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposal;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveByYear(Long fromDate, Long toDate) throws AppException {
		List<SalesTargetDO> proposal = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesTargetDO.FIND_BY_YEAR);
				q.setParameter(CommonConstants.FROMDATE, fromDate);
				q.setParameter(CommonConstants.TODATE, toDate);
				proposal = (List<SalesTargetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposal;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesTargetDO> retrieveByYearForEmp(Long fromDate, Long toDate, Long empId) throws AppException {
		List<SalesTargetDO> proposal = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesTargetDO.FIND_BY_YEAR_FOR_EMP);
				q.setParameter(CommonConstants.FROMDATE, fromDate);
				q.setParameter(CommonConstants.TODATE, toDate);
				q.setParameter(CommonConstants.EMPID, empId);
				proposal = (List<SalesTargetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposal;
	}

}
