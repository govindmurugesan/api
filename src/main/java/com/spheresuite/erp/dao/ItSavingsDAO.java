package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.ItSavingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class ItSavingsDAO {
	static Logger logger = Logger.getLogger(ItSavingsDAO.class.getName());
	private EntityManager em = null;
	
	public ItSavingsDO persist(ItSavingsDO itSavingsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && itSavingsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(itSavingsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDO> retrieve() throws AppException {
		List<ItSavingsDO> itSavingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsDO.FIND_ALL);
				itSavingsList = (List<ItSavingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDO> retrieveByEmpId(Long empId) throws AppException {
		List<ItSavingsDO> itSavingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsDO.FIND_BY_EMPID);
				q.setParameter(CommonConstants.EMPID, empId);
				itSavingsList = (List<ItSavingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDO> retrieveById(Long id) throws AppException {
		List<ItSavingsDO> itSavingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				itSavingsList = (List<ItSavingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDO> retrieveByDate(String fromDate, String toDate) throws AppException {
		List<ItSavingsDO> itSavingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsDO.FIND_BY_DATE);
				q.setParameter(CommonConstants.FROMDATE, fromDate);
				q.setParameter(CommonConstants.TODATE, toDate);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				itSavingsList = (List<ItSavingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsList;
	}
	
	public ItSavingsDO update(ItSavingsDO itSavingsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(itSavingsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsDO;
	}

}
