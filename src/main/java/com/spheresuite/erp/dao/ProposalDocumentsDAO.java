package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.ProposalDocumentsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class ProposalDocumentsDAO {
	static Logger logger = Logger.getLogger(ProposalDocumentsDAO.class.getName());
	private EntityManager em = null;
	
	public List<ProposalDocumentsDO> persist(List<ProposalDocumentsDO> proposalDocumentList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && proposalDocumentList != null && proposalDocumentList.size() > 0) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (ProposalDocumentsDO proposalDocumnetDO : proposalDocumentList) {
					em.persist(proposalDocumnetDO);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposalDocumentList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProposalDocumentsDO> retrieveById(Long proposalId) throws AppException {
		List<ProposalDocumentsDO> proposalList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ProposalDocumentsDO.FIND_BY_PROPOSAL_ID);
				q.setParameter(CommonConstants.PROPOSAL_DETAILS, proposalId);
				proposalList = (List<ProposalDocumentsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposalList;
	}
}
