package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class OpportunitiesDAO {
	static Logger logger = Logger.getLogger(OpportunitiesDAO.class.getName());
	private EntityManager em = null;
	
	public OpportunitiesDO persist(OpportunitiesDO projectDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && projectDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(projectDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieve(List<Long> Id) throws AppException {
		List<OpportunitiesDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(OpportunitiesDO.FIND_ALL);
				q.setParameter(CommonConstants.ID, Id);
				projectList = (List<OpportunitiesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieveAll() throws AppException {
		List<OpportunitiesDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(OpportunitiesDO.FIND_ALL_DATA);
				projectList = (List<OpportunitiesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	public Long retrieveForCustomer(List<Long> ids) throws AppException {
		Long projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(OpportunitiesDO.FIND_SUM_CUSTOMER);
				q.setParameter(CommonConstants.ID, ids);
				projectList = (Long) q.getSingleResult();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieveById(Long Id) throws AppException {
		List<OpportunitiesDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(OpportunitiesDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				projectList = (List<OpportunitiesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieveByContactId(Long Id) throws AppException {
		List<OpportunitiesDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(OpportunitiesDO.FIND_BY_CONTACT);
				q.setParameter(CommonConstants.ID, Id);
				projectList = (List<OpportunitiesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	public Long retrieveBySalesStageId(Long Id) throws AppException {
		Long projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(OpportunitiesDO.FIND_BY_SALES_STAGE_ID);
				q.setParameter(CommonConstants.ID, Id);
				projectList = (Long) q.getSingleResult();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpportunitiesDO> retrieveByCustomerId(Long Id) throws AppException {
		List<OpportunitiesDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(OpportunitiesDO.FIND_BY_CUSTOMER);
				q.setParameter(CommonConstants.ID, Id);
				projectList = (List<OpportunitiesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	
	
	public OpportunitiesDO update(OpportunitiesDO ProjectDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(ProjectDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return ProjectDO;
	}
	
	public boolean persistList(List<OpportunitiesDO> projectDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && projectDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (OpportunitiesDO projectDO2 : projectDO) {
					em.persist(projectDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			eException.printStackTrace();
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}

}
