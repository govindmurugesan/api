package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.MenuDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;

public class MenusDAO {
	
	static Logger logger = Logger.getLogger(MenusDAO.class.getName());
	
	private EntityManager em = null;

		
	@SuppressWarnings("unchecked")
	public List<MenuDO> retrieveMainMenu() throws AppException {
		List<MenuDO> menuList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(MenuDO.FIND_MENU);
				menuList = (List<MenuDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return menuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MenuDO> retrieveSubMenu(Long menuId) throws AppException {
		List<MenuDO> menuList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(MenuDO.FIND_MAIN_MENU);
				q.setParameter(CommonConstants.SUPERMENU, menuId);
				menuList = (List<MenuDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return menuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MenuDO> retrieveSubSubMenu(Long submenu) throws AppException {
		List<MenuDO> menuList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(MenuDO.FIND_SUB_MENU);
				q.setParameter(CommonConstants.SUB_MENU, submenu);
				menuList = (List<MenuDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return menuList;
	}
}	
