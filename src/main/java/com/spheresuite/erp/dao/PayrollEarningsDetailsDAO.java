package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class PayrollEarningsDetailsDAO {
	static Logger logger = Logger.getLogger(PayrollEarningsDetailsDAO.class.getName());
	private EntityManager em = null;
	
	public List<PayrollEarningsDetailsDO> persist(List<PayrollEarningsDetailsDO> payrollEarningsDetailsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && payrollEarningsDetailsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (PayrollEarningsDetailsDO payrollEarningsDetailsDO2 : payrollEarningsDetailsDO) {
					em.persist(payrollEarningsDetailsDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollEarningsDetailsDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PayrollEarningsDetailsDO> retrieve() throws AppException {
		List<PayrollEarningsDetailsDO> payrollEarningsDetailsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollEarningsDetailsDO.FIND_ALL);
				payrollEarningsDetailsList = (List<PayrollEarningsDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollEarningsDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationId(Long id) throws AppException {
		List<PayrollEarningsDetailsDO> payrollEarningsDetailsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollEarningsDetailsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				payrollEarningsDetailsList = (List<PayrollEarningsDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollEarningsDetailsList;
	}
	
	public PayrollEarningsDetailsDO update(PayrollEarningsDetailsDO payrollEarningsDetailsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(payrollEarningsDetailsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollEarningsDetailsDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationIds(List<Long> ids) throws AppException {
		List<PayrollEarningsDetailsDO> employeeEarningsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollEarningsDetailsDO.FIND_BY_IDS);
				q.setParameter(CommonConstants.ID, ids);
				employeeEarningsList = (List<PayrollEarningsDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeEarningsList;
	}

}
