package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class ItSavingsSettingsDAO {
	static Logger logger = Logger.getLogger(ItSavingsSettingsDAO.class.getName());
	private EntityManager em = null;
	
	public ItSavingsSettingsDO persist(ItSavingsSettingsDO itSavingsSettingsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && itSavingsSettingsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(itSavingsSettingsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsSettingsDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsSettingsDO> retrieve() throws AppException {
		List<ItSavingsSettingsDO> itSavingsSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsSettingsDO.FIND_ALL);
				itSavingsSettingsList = (List<ItSavingsSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsSettingsDO> retrieveActive() throws AppException {
		List<ItSavingsSettingsDO> itSavingsSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsSettingsDO.FIND_BY_STATUS);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				itSavingsSettingsList = (List<ItSavingsSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsSettingsDO> retrieveById(Long id) throws AppException {
		List<ItSavingsSettingsDO> itSavingsSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsSettingsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				itSavingsSettingsList = (List<ItSavingsSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsSettingsDO> retrieveByDate(String fromDate, String toDate) throws AppException {
		List<ItSavingsSettingsDO> itSavingsSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsSettingsDO.FIND_BY_DATE);
				q.setParameter(CommonConstants.FROMDATE, fromDate);
				q.setParameter(CommonConstants.TODATE, toDate);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				itSavingsSettingsList = (List<ItSavingsSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsSettingsList;
	}
	
	public ItSavingsSettingsDO update(ItSavingsSettingsDO itSavingsSettingsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(itSavingsSettingsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itSavingsSettingsDO;
	}

}
