package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmailSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmailSettingsDAO {
	static Logger logger = Logger.getLogger(EmailSettingsDAO.class.getName());
	private EntityManager em = null;
	
	public EmailSettingsDO persist(EmailSettingsDO emailSettingsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && emailSettingsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(emailSettingsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return emailSettingsDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmailSettingsDO> retrieve() throws AppException {
		List<EmailSettingsDO> EmailSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmailSettingsDO.FIND_ALL);
				EmailSettingsList = (List<EmailSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return EmailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmailSettingsDO> retrieveById(Long Id) throws AppException {
		List<EmailSettingsDO> EmailSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmailSettingsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				EmailSettingsList = (List<EmailSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return EmailSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmailSettingsDO> retrieveByMailType(String mailtype) throws AppException {
		List<EmailSettingsDO> EmailSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmailSettingsDO.FIND_BY_MAILTYPE);
				q.setParameter(CommonConstants.MAILTYPE, mailtype);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				EmailSettingsList = (List<EmailSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return EmailSettingsList;
	}
	
	public EmailSettingsDO update(EmailSettingsDO emailSettingsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(emailSettingsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return emailSettingsDO;
	}

}
