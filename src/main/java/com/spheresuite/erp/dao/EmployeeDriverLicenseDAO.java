package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeDriverLicenseDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeDriverLicenseDAO {
	static Logger logger = Logger.getLogger(EmployeeDriverLicenseDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeeDriverLicenseDO persist(EmployeeDriverLicenseDO employeeDriverLicenseDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeeDriverLicenseDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(employeeDriverLicenseDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeDriverLicenseDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDriverLicenseDO> retrieveById(Long id) throws AppException {
		List<EmployeeDriverLicenseDO> employeeDriverLicenseList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDriverLicenseDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				employeeDriverLicenseList = (List<EmployeeDriverLicenseDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeDriverLicenseList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDriverLicenseDO> retrieveByEmpId(Long empId) throws AppException {
		List<EmployeeDriverLicenseDO> employeeDriverLicenseList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDriverLicenseDO.FIND_BY_EMPID);
				q.setParameter(CommonConstants.EMPID, empId);
				employeeDriverLicenseList = (List<EmployeeDriverLicenseDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeDriverLicenseList;
	}
	
	public EmployeeDriverLicenseDO update(EmployeeDriverLicenseDO employeeDriverLicenseDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeeDriverLicenseDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeDriverLicenseDO;
	}

}
