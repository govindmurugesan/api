package com.spheresuite.erp.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.ScheduleDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;
import com.spheresuite.erp.web.util.CommonUtil;

public class ScheduleDAO {
	static Logger logger = Logger.getLogger(ScheduleDAO.class.getName());
	private EntityManager em = null;
	
	public ScheduleDO persist(ScheduleDO scheduleDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && scheduleDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(scheduleDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return scheduleDO;
	}
	
	public ScheduleDO update(ScheduleDO scheduleDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && scheduleDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(scheduleDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return scheduleDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ScheduleDO> retrieveById(Long Id) throws AppException {
		List<ScheduleDO> scheduleList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ScheduleDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				scheduleList = (List<ScheduleDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return scheduleList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ScheduleDO> retrieveByLeadId(Long Id) throws AppException {
		List<ScheduleDO> scheduleList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ScheduleDO.FIND_BY_LEAD_ID);
				q.setParameter(CommonConstants.ID, Id);
				scheduleList = (List<ScheduleDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return scheduleList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ScheduleDO> retrieveUpcomingCalender(Long empid ,java.util.Date date) throws AppException {
		List<ScheduleDO> scheduleList = new ArrayList<ScheduleDO>();
		List<ScheduleDO> tempScheduleList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ScheduleDO.FIND_UPCOMMING_CALENDER);
				q.setParameter(CommonConstants.CURRENT_DATE, CommonUtil.convertDateToYearWithOutTime(date));
				q.setParameter(CommonConstants.EMPID, empid);
				tempScheduleList = (List<ScheduleDO>) q.getResultList();
				int i=0;
				for (ScheduleDO scheduleDO : tempScheduleList) {
					if(scheduleDO.getLogTime().compareTo(CommonUtil.convertDateToOnlyTime(date)) >= 0){
						scheduleList.add(scheduleDO);
						i++;
						if(i >= 5){
							break;
						}
					}else{
						if(scheduleDO.getLogDate().compareTo(CommonUtil.convertDateToYearWithOutTime(date)) >= 0){
							scheduleList.add(scheduleDO);
							i++;
							if(i >= 5){
								break;
							}
						}
					}
				}
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return scheduleList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ScheduleDO> retrieveUpcomingCalenderOfEmp(Long empid ,java.util.Date date) throws AppException {
		List<ScheduleDO> scheduleList = new ArrayList<ScheduleDO>();
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ScheduleDO.FIND_UPCOMMING_CALENDER_OF_EMP);
				q.setParameter(CommonConstants.EMPID, empid);
				scheduleList = (List<ScheduleDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return scheduleList;
	}
}
