package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.ProposalDefaultDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class ProposalDefaultDAO {
	static Logger logger = Logger.getLogger(ProposalDefaultDAO.class.getName());
	private EntityManager em = null;
	
	public List<ProposalDefaultDO> persist(List<ProposalDefaultDO> proposalDefaultList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && proposalDefaultList != null && proposalDefaultList.size() > 0) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				if(proposalDefaultList != null && proposalDefaultList.size() > 0){
					Query q =  em.createQuery("delete from ProposalDefaultDO e where e.proposalId =:proposalId");
					q.setParameter(CommonConstants.PROPOSAL_ID, proposalDefaultList.get(0).getProposalId());
					q.executeUpdate();
				}
				for (ProposalDefaultDO proposalDefaultDO : proposalDefaultList) {
					em.persist(proposalDefaultDO);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposalDefaultList;
	}
	
	public boolean delete(Long proposalId) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && proposalId != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				Query q =  em.createQuery("delete from ProposalDefaultDO e where e.proposalId =:proposalId");
				q.setParameter(CommonConstants.PROPOSAL_ID, proposalId);
				q.executeUpdate();
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProposalDefaultDO> retrieveById(Long proposalId) throws AppException {
		List<ProposalDefaultDO> proposalList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ProposalDefaultDO.FIND_BY_PROPOSAL_ID);
				q.setParameter(CommonConstants.PROPOSAL_ID, proposalId);
				proposalList = (List<ProposalDefaultDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposalList;
	}
}
