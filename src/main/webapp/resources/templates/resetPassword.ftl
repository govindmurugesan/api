<!DOCTYPE html>
<html>
  <head>
    <title>Sphere Suite</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="icon" type="img/png" href="http://www.spheresuite.com/images/logo_icon.png">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
     <style type="text/css">
      @media (min-width:320px) and (max-width:450px) {
        .mail_content{
          height: 100%;
          width: 90% !important;
          background-color: #fff;
          margin: 10px auto;
          text-align: center;
}     
        .paratext h1{
            font-size: 17px !important;
          }
          .paratext h3{
            font-size: 15px !important;
          }
          .paratext p{
            font-size: 14px !important;
          }    
        .footer{
            text-align: center !important;
}
        .footer_left{
  
            float: none !important;
}
        .footer_right{
  
            float: none !important;
}
        .respTextAlign{
            padding: 10px 0 !important;
}
        .footer a{
            font-size: 12px !important;
          }
        .footer p{
            font-size: 12px !important;
          }
}
@media (min-width: 451px) and (max-width: 767px){
          .mail_content{
                height: 100%;
                width: 90% !important;
                background-color: #fff;
                margin: 10px auto;
                text-align: center;
         }
          .paratext h1{
            font-size: 18px !important;
          }
          .paratext h3{
            font-size: 16px !important;
          }
          .paratext p{
            font-size: 14px !important;
          }
          .footer a{
            font-size: 14px !important;
          }
          .footer p{
            font-size: 12px !important;
          }
      }


    </style> 
  </head>
  <body style="background-color: #e8e8e8;">
    <div style="position: relative;overflow-x: hidden;">
      <div class="mail_content" style="height: 100%;max-width:630px;min-width: 320px; background-color: #fff;margin: 20px auto;">

    <div style="font-family:Arial, sans-serif;">
        <div>
          <img src="http://www.spheresuite.com/images/logo.png" class="img-responsive" style="padding: 25px; margin: 0 auto;display: block;float: left;">
        </div>
        <div class="paratext" style="border-top: 1px solid #e8e8e8;clear: both;">
        <div style="color: #777;padding: 25px;">
          <p style="margin: 0 !important;">Hello ${userName},  </p>
          </br>
          <!-- <h3>Congratulations !</h3>
          </br> -->
          <p style="margin: 0 !important; color: #777;">Thanks for being part of SphereSuite, kindly click on the below button to reset your password.</p>
          </br>
          <div style="">
           <a href="${link}"> <button style="color: #fff !important; background-color: #00254d; border: none; outline: 0; padding: 7px 18px;">Reset Password</button></a>
          </div>
        </div>
      </div>
      <div class="row" style="margin: 0px;">
       <div class="col-lg-12 col-xs-12" style="text-align: center;color: #fff; border-top: 1px solid #e8e8e8;">
         <div class="footer">
          <li style="margin-top: 23px;">
               <a href="https://www.facebook.com/spheresuite/"><img src="http://www.spheresuite.com/images/fb.png"> </a>
                <a href=" https://www.linkedin.com/company/13599877/"><img src="http://www.spheresuite.com/images/linkedin.png"></a>
                <a href=" https://plus.google.com/u/0/102531410633648808151"><img src="http://www.spheresuite.com/images/googleplus.png"> </a>
                <a href="https://twitter.com/spheresuite"><img src="http://www.spheresuite.com/images/twitter.png"></a>
              </li>
          <p class="respTextAlign" style="color: #777 !important;padding: 20px 0px;margin: 0;">Copyright © &nbsp;SphereSuite&nbsp;2017&nbsp;|&nbsp;All Rights Reserved</p>
        </div>
        </div>
        </div>
    </div>
  </div>
  </div>
  </div>
</body>
</html>